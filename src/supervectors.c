#include "supervectors.h"
#include <stdio.h>
uint64_t i_invert   = 1;
uint64_t i_negate   = 1;
uint64_t i_scm      = 1;
uint64_t i_signed   = 1;
uint64_t e_default  = 1;
uint64_t i_end      = 1;
uint64_t i_algebra  = 1;
uint64_t i_float    = 1;

SCM ds_vtable = SCM_BOOL_F;

#define X(bv) SCM_BYTEVECTOR_CONTENTS(bv)
#define Y(bv) ((uint64_t *) SCM_I_VECTOR_WELTS(bv))

SCM_DEFINE(set_supervector_constants,"supervector-constants",1,0,0,(SCM x),
	   "type constants")
#define FUNC_NAME s_set_supervector_constants  
{
  SCM *w = (SCM *) Y(x);
  i_invert  = scm_to_uint64(w[0]);
  i_negate  = scm_to_uint64(w[1]);
  i_scm     = scm_to_uint64(w[2]);
  i_signed  = scm_to_uint64(w[3]);
  i_end     = scm_to_uint64(w[4]);
  i_algebra = scm_to_uint64(w[5]);
  e_default = scm_to_uint64(w[6]);
  i_float   = scm_to_uint64(w[7]);
  ds_vtable = w[8];
  
  return SCM_UNDEFINED;
}
#undef FUNC_NAME




SCM_DEFINE(super_bits_binary1, "super-bits-binary1", 7, 0, 0,
	   (SCM x1_,
	    SCM b2, SCM i2_, SCM j2_, SCM d2_, SCM doinv, SCM kind_),
	   "bit operations for fast execution")
#define FUNC_NAME s_super_bits_binary
{
  int kind = scm_to_int (kind_);
  
  int i2   = scm_to_int(i2_);
  int j2   = scm_to_int(j2_);
  int d2   = scm_to_int(d2_);

  j2 = (d2 > 0) ? j2 + 1 : j2 - 1;
  
  uint64_t bits2 = scm_to_uint64(SCM_STRUCT_SLOT_REF(b2,3));

  SCM v2 = SCM_STRUCT_SLOT_REF(b2,0);

  uint64_t *w2 = (uint64_t *)X(v2);

  uint64_t x1 = scm_to_uint64(x1_);
  
  int k2 = (i_invert & bits2)         ? 1 : 0;
  int k3 = (scm_is_true(doinv) && k2) ? 2 : 0;

#define E(N,A,C,OP,OP1,OP2)					\
  case N:							\
  {								\
    k2 = i2;							\
    do								\
      {								\
	w2[k2] = (A((OP1(x1)) OP (OP2(C(w2[k2])))));		\
	k2 += d2;						\
      } while (k2 != j2);					\
  }								\
  break;

#define B(N,A,OP1)						\
  case N:							\
    {								\
      k2 = i2;							\
      do							\
	{							\
	  w2[k2] = (A((OP1(x1))));				\
	  k2 += d2;						\
	} while ( k2 != j2);					\
    }								\
    break;

#define FUNCTION(K,OP,OP1,OP2)				\
  switch (k2 | (K ^ k3))				\
    {							\
      E(0,+,+,OP,OP1,OP2);				\
      E(1,+,~,OP,OP1,OP2);				\
      E(2,~,+,OP,OP1,OP2);				\
      E(3,~,~,OP,OP1,OP2);				\
    }

#define FUNCTIONB(OP)					\
  switch (k3>>1)					\
    {						        \
      B(0,+,OP);					\
      B(1,~,OP);					\
    }

    switch (kind)
      {
      case 0:
	FUNCTION(0,&,+,+);
	break;
	
      case 1:
	FUNCTION(0,|,+,+);
	break;
	
      case 2:
	FUNCTION(0,^,+,+);
	break;
	
      case 3:
	FUNCTION(0,&,+,~);
	break;
	
      case 4:
	FUNCTIONB(+);
	break;
	
      case 5:
	FUNCTIONB(~);
	break;
	
      case 6:
	FUNCTION(4,^,+,+);
	break;
      }

    return SCM_UNDEFINED;
}
#undef FUNC_NAME
#undef FUNCTION
#undef FUNCTIONB
#undef E
#undef B
#undef U
#undef V


SCM_DEFINE(super_bits_binary, "super-bits-binary", 10, 0, 0,
	   (SCM b1, SCM i1_, SCM j1_, SCM d1_,
	    SCM b2, SCM i2_, SCM j2_, SCM d2_, SCM doinv, SCM kind_),
	   "bit operations for fast execution")
#define FUNC_NAME s_super_bits_binary
{
  int kind = scm_to_int (kind_);
  
  int i1   = scm_to_int(i1_);
  int j1   = scm_to_int(j1_);
  int d1   = scm_to_int(d1_);
  int i2   = scm_to_int(i2_);
  int j2   = scm_to_int(j2_);
  int d2   = scm_to_int(d2_);

  j1 = (d1 > 0) ? j1 + 1 : j1 - 1;
  j2 = (d2 > 0) ? j2 + 1 : j2 - 1;
  
  uint64_t bits1 = scm_to_uint64(SCM_STRUCT_SLOT_REF(b1,3));
  uint64_t bits2 = scm_to_uint64(SCM_STRUCT_SLOT_REF(b2,3));

  SCM v1 = SCM_STRUCT_SLOT_REF(b1,0);
  SCM v2 = SCM_STRUCT_SLOT_REF(b2,0);

  uint64_t *w1 = (uint64_t *)X(v1);
  uint64_t *w2 = (uint64_t *)X(v2);

  int k1 = (i_invert & bits1)         ? 2 : 0;
  int k2 = (i_invert & bits2)         ? 1 : 0;
  int k3 = (scm_is_true(doinv) && k2) ? 4 : 0;

#define E(N,A,B,C,OP,OP1,OP2)					\
  case N:							\
  {								\
    int k1 = i1, k2 = i2;					\
    printf("binary op");					\
    do								\
      {								\
	w2[k2] = (A((OP1(B(w1[k1]))) OP (OP2(C(w2[k2])))));	\
	k1 += d1, k2 += d2;					\
      } while (k1 != j1);					\
  }								\
  break;

#define B(N,A,B,OP1)						\
  case N:							\
    {								\
    int k1 = i1, k2 = i2;					\
    do								\
      {								\
      w2[k2] = (A((OP1(B(w1[k1])))));				\
      k1 += d1, k2 += d2;					\
      } while ( k1 != j1);					\
    }								\
    break;

#define FUNCTION(K,OP,OP1,OP2)				\
  switch ((k1 | k2 | (K ^ k3)))				\
    {							\
      E(0,+,+,+,OP,OP1,OP2);				\
      E(1,+,+,~,OP,OP1,OP2);				\
      E(2,+,~,+,OP,OP1,OP2);				\
      E(3,+,~,~,OP,OP1,OP2);				\
      E(4,~,+,+,OP,OP1,OP2);				\
      E(5,~,+,~,OP,OP1,OP2);				\
      E(6,~,~,+,OP,OP1,OP2);				\
      E(7,~,~,~,OP,OP1,OP2);				\
    }

#define FUNCTIONB(OP)					\
  switch (k1>>1 | k3>>1)				\
    {						        \
      B(0,+,+,OP);					\
      B(1,+,~,OP);					\
      B(2,~,+,OP);					\
      B(3,~,~,OP);					\
    }

  printf("binart %d\n",kind);
    switch (kind)
      {
      case 0:
	printf("and-op\n");
	FUNCTION(0,&,+,+);
	break;
	
      case 1:
	FUNCTION(0,|,+,+);
	break;
	
      case 2:
	FUNCTION(0,^,+,+);
	break;
	
      case 3:
	FUNCTION(0,&,+,~);
	break;
	
      case 4:
	FUNCTIONB(+);
	break;
	
      case 5:
	FUNCTIONB(~);
	break;
	
      case 6:
	FUNCTION(4,^,+,+);
	break;
      }

    return SCM_UNDEFINED;
}
#undef FUNC_NAME
#undef FUNCTION
#undef FUNCTIONB
#undef E
#undef B
#undef U
#undef V

#include "masked.c"
#include "copy.c"

void supervector_init()
{
#include "supervectors.x"
}
