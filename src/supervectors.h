#include <libguile.h>

SCM_API SCM set_supervector_constants (SCM inv);

SCM_API SCM super_bits_masked_binary (SCM b1   , SCM i1_ , SCM j1_  , SCM d1_,
				      SCM b2   , SCM i2_ , SCM j2_  , SCM d2_,
				      SCM doinv_data, SCM kind_);

SCM_API SCM super_bits_masked_binary1 (SCM d_,
				       SCM b2   , SCM i2_ , SCM j2_  , SCM d2_,
				       SCM doinv_data, SCM kind_);

SCM_API SCM super_bits_binary (SCM b1   , SCM i1_  , SCM j1_, SCM d1_,
			       SCM b2   , SCM i2_  , SCM j2_, SCM d2_,
			       SCM doinv, SCM kind_);

SCM_API SCM super_bits_binary1 (SCM x1   ,
				SCM b2   , SCM i2_  , SCM j2_, SCM d2_,
				SCM doinv, SCM kind_);

