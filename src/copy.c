int is_ds_compound(SCM x)
{
  return SCM_STRUCTP(x) && scm_is_eq(scm_struct_vtable(x),ds_vtable);
}

SCM scm_nop(SCM x)
{
  return x;
}

SCM scm_negate(SCM x)
{
  return scm_difference(scm_from_int(0),x);
}


void swap_2(void *x)
  {		
    char *v;
    v = (char *) x;	
    char t = v[0];
    v[0] = v[1];
    v[1] = t;
  }

void swap_4(void *x)
{
  char *v;
  v = (char *) x;
  char t = v[0];				
  v[0] = v[3];				
  v[3] = t;					
  t    = v[1];				
  v[1] = v[2];			
  v[2] = t;				
}

void swap_8(void *x)
{			
  char *v;	
  v = (char *) x;	
  char t = v[0];
  v[0] = v[7];
  v[7] = t;	
		
  t    = v[1];
  v[1] = v[6];
  v[6] = t;	
      		
  t    = v[2];
  v[2] = v[5];
  v[5] = t;	
		
  t    = v[3];
  v[3] = v[4];
  v[5] = t;	
}

#define E(X1,Y1,A1,T1,X2,Y2,A2,T2,TP2)					\
  {									\
    int k1  = i1;							\
    int k2  = i2;							\
    do									\
      {									\
	z1 = w1[k1];							\
	T1((void *) &z1);						\
	z2 = X1 + Y1*(A1(z1));						\
	T2((void *) &z2);						\
	w2[k2] = (TP2)(X2 + Y2*(A2(z2)));				\
									\
	k1 += d1;							\
	k2 += d2;							\
      } while (k1 != j1);						\
								\
    break;							\
  }

#define ESCM1(X1,Y1,A1,T1,X2,Y2,A2,T2)					\
  {									\
    int k1  = i1;							\
    int k2  = i2;							\
    do									\
      {									\
	z1 = w1[k1];							\
	T1((void *) &z1);						\
	z2 = T2(X1 + Y1*(A1(z1)));					\
	w2[k2] = scm_sum(X2,scm_product(Y2,A2(z2)));			\
    									\
	k1 += d1;							\
	k2 += d2;							\
      } while (k1 != j1);						\
  									\
    break;								\
  }

#define ESCM2(X1,Y1,A1,X2,Y2,A2)					\
  {									\
    int k1  = i1;							\
    int k2  = i2;							\
    do									\
      {									\
	z1     = w1[k1];						\
	z2     = scm_sum(X1, scm_product(Y1, A1(z1)));			\
	w2[k2] = scm_sum(X2, scm_product(Y2, A2(z2)));			\
									\
	k1 += d1;							\
	k2 += d2;							\
      } while (k1 != j1);						\
    									\
    break;								\
  }

#define HSCM							\
  {								\
    int k1 = i1;						\
    int k2 = i2;						\
    do								\
      {								\
	w2[k2] = w1[k1];					\
								\
	k1 += d1;						\
	k2 += d2;						\
      } while (k1 != j1);					\
								\
    break;							\
  }

#define G2(X1,Y1,A1,T1,T2,TP2,INV2)		\
  {						\
    switch (k2)					\
      {						\
      case 0:					\
	E(X1,Y1,A1,T1,0,1,+,T2,TP2);		\
      case 1:					\
	E(X1,Y1,A1,T1,0,1,INV2,T2,TP2);		\
      case 3:					\
	E(X1,Y1,A1,T1,xx2,yy2,+,T2,TP2);	\
      }						\
    break;					\
  }

#define GG2(X1,Y1,A1,T1,T2)						\
  {									\
  switch (k2)								\
    {									\
    case 0:								\
      ESCM1(X1,Y1,A1,T1,scm_from_int(0), scm_from_int(1), scm_nop, T2); \
    case 1:								\
      ESCM1(X1,Y1,A1,T1,scm_from_int(0), scm_from_int(1), scm_lognot,T2); \
    case 2:								\
      ESCM1(X1,Y1,A1,T1,scm_from_int(0), scm_from_int(1), scm_negate,T2); \
    case 3:								\
      ESCM1(X1,Y1,A1,T1,x2, y2, scm_nop,T2);				\
    }									\
  break;								\
  }

#define GGG2(X1,Y1,A1)							\
  {									\
    switch (k2)								\
      {									\
      case 0:								\
	ESCM2(X1,Y1,A1,scm_from_int(0), scm_from_int(1), scm_nop);	\
      case 1:								\
	ESCM2(X1,Y1,A1,scm_from_int(0), scm_from_int(1), scm_lognot);	\
      case 2:								\
	ESCM2(X1,Y1,A1,scm_from_int(0), scm_from_int(1), scm_negate);	\
      case 3:								\
	ESCM2(X1,Y1,A1,x2, y2, scm_nop);				\
      }									\
    break;								\
  }

#define G1(T1,T2,TP2,INV1,INV2)				\
  switch (k1)						\
    {							\
    case 0:						\
      G2(0,1,+,T1,T2,TP2,INV2);				\
    case 1:						\
      G2(0,1,INV1,T1,T2,TP2,INV2);				\
    case 3:						\
      G2(xx1,yy1,+,T1,T2,TP2,INV2);				\
    }

#define GG1(T1,INV1,T2)					\
  switch (k1)						\
    {							\
    case 0:						\
      GG2(0,1,+,T1,T2);					\
    case 1:						\
      GG2(0,1,INV1,T1,T2);				\
    case 3:						\
      GG2(xx1,yy1,+,T1,T2);				\
    }

#define GGG1()								\
  switch (k1)								\
    {									\
    case 0:								\
      GGG2(scm_from_int(0), scm_from_int(1), scm_nop);			\
    case 1:								\
      GGG2(scm_from_int(0),scm_from_int(1),scm_lognot);			\
    case 2:								\
      GGG2(scm_from_int(0),scm_from_int(1),scm_negate);			\
    case 3:								\
      GGG2(x1,y1,scm_nop);						\
    }

#define H2(T1,mm2,TP2,INV1,INV2)		\
  if (e2)					\
    {						\
      switch (mm2)				\
	{					\
	case 1:					\
	  G1(T1,(void),TP2,INV1,INV2);		\
	  break;				\
	case 2:					\
	  G1(T1,swap_2,TP2,INV1,INV2);		\
	  break;				\
	case 4:					\
	  G1(T1,swap_4,TP2,INV1,INV2);		\
	  break;				\
	case 8:					\
	  G1(T1,swap_8,TP2,INV1,INV2);		\
	  break;				\
	}					\
    }						\
  else						\
    {						\
      G1(T1,(void),TP2,INV1,INV2);		\
    }

#define HQ(mm1,mm2,TP2,INV1,INV2)		\
  if (e1)					\
    {						\
      switch (mm1)				\
	{					\
	case 1:					\
	  H2((void),mm2,TP2,INV1,INV2);		\
	  break;				\
	case 2:					\
	  H2(swap_2,mm2,TP2,INV1,INV2);		\
	  break;				\
	case 4:					\
	  H2(swap_4,mm2,TP2,INV1,INV2);		\
	  break;				\
	case 8:					\
	  H2(swap_8,mm2,TP2,INV1,INV2);		\
	  break;				\
	}					\
    }						\
  else						\
    {						\
      H2((void),mm2,TP2,INV1,INV2);		\
    }

#define HSCM1(mm1,INV1,T2)			\
  if (e1)					\
    {						\
      switch (mm1)				\
	{					\
	case 1:					\
	  GG1((void),INV1,T2);			\
	  break;				\
	case 2:					\
	  GG1(swap_2,INV1,T2);			\
	  break;				\
	case 4:					\
	  GG1(swap_4,INV1,T2);			\
	  break;				\
	case 8:					\
	  GG1(swap_8,INV1,T2);			\
	  break;				\
	}					\
    }						\
  else						\
    {						\
      GG1((void),INV1,T2);			\
    }

#define H(mm1,mm2,TP2) HQ(mm1,mm2,TP2,~,~)

// This is code explosion el magnifico
SCM_DEFINE(super_bits_copy, "super-bits-copy", 9, 0, 0,
	   (SCM b1, SCM i1_, SCM j1_, SCM d1_,
	    SCM b2, SCM i2_, SCM j2_, SCM d2_, SCM doinv_),
	   "bit operations for fast execution")
#define FUNC_NAME s_super_bits_copy
{
  int i1   = scm_to_int(i1_);
  int j1   = scm_to_int(j1_);
  int d1   = scm_to_int(d1_);
  int i2   = scm_to_int(i2_);
  int j2   = scm_to_int(j2_);
  int d2   = scm_to_int(d2_);

  j1 = (d1 > 0) ? j1 + 1 : j1 - 1;
  j2 = (d2 > 0) ? j2 + 1 : j2 - 1;

  int doinv = scm_is_true(doinv_) ? 1 : 0;
  
  uint64_t bits1 = scm_to_uint64(SCM_STRUCT_SLOT_REF(b1,3));
  uint64_t bits2 = scm_to_uint64(SCM_STRUCT_SLOT_REF(b2,3));

  SCM v1 = SCM_STRUCT_SLOT_REF(b1,0);
  SCM v2 = SCM_STRUCT_SLOT_REF(b2,0);

  int scm1 = (bits1 & i_scm) > 0 ? 1 : 0;
  int scm2 = (bits2 & i_scm) > 0 ? 1 : 0;
  
  uint64_t *ww1 = NULL;
  uint64_t *ww2 = NULL;
  
  if (scm1)
    ww1 = (uint64_t *)Y(v1);
  else
    ww1 = (uint64_t *)X(v1);

  if (scm2)
    ww2 = (uint64_t *)Y(v2);
  else
    ww2 = (uint64_t *)X(v2);

  int k1 = 0;

  SCM x1 = scm_from_int(0);
  SCM y1 = scm_from_int(1);
     
  if ((bits1 & i_invert) > 0)
    {
      k1 = 1;
    }
  else if ((bits1 & i_negate) > 0)
    {
      k1 = 2;
    }
  else if ((bits1 & i_algebra) > 0)
    {
      k1 = 3;
      
      x1 = SCM_STRUCT_SLOT_REF(b1,1);
      if (is_ds_compound(x1))
	{
	  y1 = SCM_STRUCT_SLOT_REF(x1,1);
	  x1 = SCM_STRUCT_SLOT_REF(x1,0);
	}      
    }
  
  int k2 = 0;
  SCM x2 = scm_from_int(0);
  SCM y2 = scm_from_int(1);
  if ((bits2 & i_invert) > 0 && doinv)
    {
      k2 = 1;
    }
  else if ((bits2 & i_negate) > 0)
    {
      k2 = 2;
    }
  else if ((bits2 & i_algebra) > 0)
    {
      k2 = 3;
      
      x2 = SCM_STRUCT_SLOT_REF(b2,1);
      if (is_ds_compound(x2))
	{
	  y2 = SCM_STRUCT_SLOT_REF(x2,1);
	  x2 = SCM_STRUCT_SLOT_REF(x2,0);
	}      
    }


  int64_t xx1 = scm_to_int64(x1);
  int64_t yy1 = scm_to_int64(y1);
  int64_t xx2 = scm_to_int64(x2);
  int64_t yy2 = scm_to_int64(y2);

  yy1 = yy1 / yy2;
  xx1 = (yy1 - xx1) / yy2;
  
  yy2 = 1;
  xx2 = 0;

  int m1 = bits1 & 0xf;
  int m2 = bits2 & 0xf;

  int e1 = 0;
  if (m1 > 0)
    {
      if (((bits1 & i_end) ^ e_default) > 0)
	e1 = 1;
    }

  int e2 = 0;
  if (m2 > 0)
    {
      if (((bits2 & i_end) ^ e_default) > 0)
	e2 = 1;
    }

  int t1 = 0;
  if (scm1)
    t1 = 0;
  else if ((bits1 & i_float ) > 0)
    {
      t1 = (m1 == 2) ? 5 : 6;
    }
  else if ((bits1 & i_signed) > 0)
    {
      t1 = (m1+1) << 4;      
    }
  else
    {
      t1 = (m1+1);      
    }

  int t2 = 0;
  if (scm2)
    t2 = 0;
  else if ((bits2 & i_float ) > 0)
    {
      t2 = (m2 == 2) ? 5 : 6;
    }
  else if ((bits2 & i_signed) > 0)
    {
      t2 = (m2+1) << 4;      
    }
  else
    {
      t2 = (m2+1);      
    }

  int t = (t1 << 8) + t2;

  switch (t)
    {
    case 0:
      {
	SCM* w1 = (SCM *) ww1;
	SCM* w2 = (SCM *) ww2;
	SCM z1;
	SCM z2;
	GGG1();
	break;
      }

    case 0x101:
      {
	uint8_t* w1 = (uint8_t *) ww1;
	uint8_t* w2 = (uint8_t *) ww2;
	uint8_t z1;
	uint8_t z2;
	H(1,1,uint8_t);
	break;
      }

    case 0x100:
      {
	uint8_t* w1 = (uint8_t *) ww1;
	SCM    * w2 = (SCM     *) ww2;
	uint8_t z1;
	SCM     z2;
	HSCM1(1,~,scm_from_uint8);
	break;
      }
      
    case 0x102:
      {
	uint8_t* w1 = (uint8_t *) ww1;
	uint16_t* w2 = (uint16_t *) ww2;
	uint8_t z1;
	uint16_t z2;
	
	H(1,2,uint16_t);
	break;
      }
      
    case 0x103:
      {
	uint8_t* w1 = (uint8_t *) ww1;
	uint32_t* w2 = (uint32_t *) ww2;
	uint8_t z1;
	uint16_t z2;
	
	H(1,4,uint32_t);
	break;
      }
      
    case 0x104:
      {
	uint8_t* w1 = (uint8_t *) ww1;
	uint64_t* w2 = (uint64_t *) ww2;
	uint8_t z1;
	uint64_t z2;

	H(1,8,uint64_t);
	break;
      }

    case 0x105:
      {
	uint8_t* w1 = (uint8_t *) ww1;
	float* w2   = (float   *) ww2;
	uint8_t z1;
	float   z2;

	HQ(1,4,float,~,+);
	break;
      }

    case 0x106:
      {
	uint8_t* w1 = (uint8_t *) ww1;
	double* w2  = (double  *) ww2;
	uint8_t z1;
	double  z2;

	HQ(1,8,double,~,+);
	break;
      }
      
    case 0x120:
      {
	uint8_t* w1 = (uint8_t *) ww1;
	int16_t* w2 = (int16_t *) ww2;
	uint8_t z1;
	int16_t z2;

	H(1,2,int16_t);
	break;
      }
      
    case 0x130:
      {
	uint8_t* w1 = (uint8_t *) ww1;
	int32_t* w2 = (int32_t *) ww2;	
	uint8_t z1;
	int32_t z2;

	H(1,4,int32_t);
	break;
      }
      
    case 0x140:
      {
	uint8_t* w1 = (uint8_t *) ww1;
	int64_t* w2 = (int64_t *) ww2;
	uint8_t z1;
	int64_t z2;

	H(1,8,int64_t);
	break;
      }

    case 0x200:
      {
	uint16_t* w1 = (uint16_t *) ww1;
	SCM    *  w2 = (SCM     *)  ww2;
	uint16_t z1;
	SCM      z2;
	HSCM1(2,~,scm_from_uint16);
	break;
      }

    case 0x202:
      {
	uint16_t* w1 = (uint16_t *) ww1;
	uint16_t* w2 = (uint16_t *) ww2;
	uint16_t z1;
	uint16_t z2;

	H(2,2,uint16_t);
	break;
      }
      
    case 0x203:
      {
	uint16_t* w1 = (uint16_t *) ww1;
	uint32_t* w2 = (uint32_t *) ww2;
	uint16_t z1;
	uint32_t z2;

	H(2,4,uint32_t);
	break;
      }
      
    case 0x204:
      {
	uint16_t* w1 = (uint16_t *) ww1;
	uint64_t* w2 = (uint64_t *) ww2;
	uint16_t z1;
	uint64_t z2;

	H(2,4,uint64_t);
	break;
      }

    case 0x205:
      {
	uint16_t* w1 = (uint16_t *) ww1;
	float* w2    = (float    *) ww2;
	uint16_t z1;
	float    z2;

	HQ(2,8,float,~,+);
	break;
      }

    case 0x206:
      {
	uint16_t* w1 = (uint16_t *) ww1;
	double* w2   = (double   *) ww2;
	uint16_t z1;
	double   z2;

	HQ(2,4,double,~,+);
	break;
      }
      
    case 0x230:
      {
	uint16_t* w1 = (uint16_t *) ww1;
	int32_t* w2 = (int32_t *) ww2;
	uint16_t z1;
	int32_t z2;

	H(2,4,uint64_t);
	break;
      }
      
    case 0x240:
      {
	uint16_t* w1 = (uint16_t *) ww1;
	int64_t* w2 = (int64_t *) ww2;
	uint16_t z1;
	int64_t z2;

	H(2,8,int32_t);
	break;
      }

    case 0x300:
      {
	uint32_t* w1 = (uint32_t *) ww1;
	SCM    *  w2 = (SCM     *)  ww2;
	uint32_t z1;
	SCM      z2;
	HSCM1(4,~,scm_from_uint32);
	break;
      }

    case 0x303:
      {
	uint32_t* w1 = (uint32_t *) ww1;
	uint32_t* w2 = (uint32_t *) ww2;
	uint32_t z1;
	uint32_t z2;

	H(4,4,int64_t);
	break;
      }
    case 0x304:
      {
	uint32_t* w1 = (uint32_t *) ww1;
	uint64_t* w2 = (uint64_t *) ww2;
	uint32_t z1;
	uint64_t z2;
	
	H(4,8,uint32_t);
	break;
	
      }

    case 0x305:
      {
	uint32_t* w1 = (uint32_t *) ww1;
	float*    w2 = (float    *) ww2;
	uint32_t z1;
	float    z2;
	
	HQ(4,4,float,~,+);
	break;
	
      }
      
    case 0x306:
      {
	uint32_t* w1 = (uint32_t *) ww1;
	double*   w2 = (double   *) ww2;
	uint32_t  z1;
	double    z2;
	
	HQ(4,8,double,~,+);
	break;
	
      }
    case 0x340:
      {
	uint32_t* w1 = (uint32_t *) ww1;
	int64_t* w2  = (int64_t *) ww2;
	uint32_t z1;
	int64_t z2;
	
	H(4,8,uint64_t);
	break;
      }
      

    case 0x400:
      {
	uint64_t* w1 = (uint64_t *) ww1;
	SCM    *  w2 = (SCM     *)  ww2;
	uint64_t z1;
	SCM      z2;
	HSCM1(8,~,scm_from_uint64);
	break;
      }

    case 0x404:
      {
	uint64_t* w1 = (uint64_t *) ww1;
	uint64_t* w2 = (uint64_t *) ww2;
	uint64_t z1;
	uint64_t z2;

	H(8,8,int64_t);
	break;
      }

    case 0x405:
      {
	uint64_t* w1 = (uint64_t *) ww1;
	float*    w2 = (float    *) ww2;
	uint64_t z1;
	float    z2;

	HQ(8,4,float,~,+);
	break;
      }

    case 0x406:
      {
	uint64_t* w1 = (uint64_t *) ww1;
	double*   w2 = (double   *) ww2;
	uint64_t z1;
	double   z2;

	HQ(8,8,double,~,+);
	break;
      }

    case 0x1000:
      {
	int8_t* w1 = (int8_t *) ww1;
	SCM   * w2 = (SCM    *)  ww2;
	int8_t z1;
	SCM    z2;
	HSCM1(1,~,scm_from_int8);
	break;
      }

    case 0x1010:
      {
	int8_t* w1 = (int8_t *) ww1;
	int8_t* w2 = (int8_t *) ww2;
	int8_t z1;
	int8_t z2;

	H(1,1,int8_t);
	break;
      }

    case 0x1020:
      {
	int8_t* w1 = (int8_t *) ww1;
	int16_t* w2 = (int16_t *) ww2;
	int8_t z1;
	int16_t z2;

	H(1,2,int16_t);
	break;
      }

    case 0x1030:
      {
	int8_t* w1 = (int8_t *) ww1;
	int32_t* w2 = (int32_t *) ww2;
	int8_t z1;
	int32_t z2;

	H(1,4,int32_t);
	break;
      }

    case 0x1040:
      {
	int8_t* w1 = (int8_t *) ww1;
	int64_t* w2 = (int64_t *) ww2;
	int8_t z1;
	int64_t z2;

	H(1,8,int64_t);
	break;
      }

    case 0x1005:
      {
	int8_t* w1 = (int8_t *) ww1;
	float*  w2 = (float  *) ww2;
	int8_t z1;
	float  z2;

	HQ(1,4,float,~,+);
	break;
      }

    case 0x1006:
      {
	int8_t* w1 = (int8_t *) ww1;
	double* w2 = (double *) ww2;
	int8_t z1;
	double z2;

	HQ(1,8,double,~,+);
	break;
      }
      
    case 0x2000:
      {
	int16_t* w1 = (int16_t *) ww1;
	SCM   *  w2 = (SCM    *)  ww2;
	int16_t z1;
	SCM     z2;
	HSCM1(2,~,scm_from_int16);
	break;
      }

    case 0x2020:
      {
	int16_t* w1 = (int16_t *) ww1;
	int16_t* w2 = (int16_t *) ww2;
	int16_t z1;
	int16_t z2;

	H(2,2,int16_t);
	break;
      }
      
    case 0x2030:
      {
	int16_t* w1 = (int16_t *) ww1;
	int32_t* w2 = (int32_t *) ww2;
	int16_t z1;
	int32_t z2;

	H(2,4,int32_t);
	break;
      }

    case 0x2040:
      {
	int16_t* w1 = (int16_t *) ww1;
	int64_t* w2 = (int64_t *) ww2;
	int16_t z1;
	int64_t z2;

	H(2,8,int64_t);
	break;
      }

    case 0x2005:
      {
	int16_t* w1 = (int16_t *) ww1;
	float*   w2 = (float   *) ww2;
	int16_t z1;
	float   z2;

	HQ(2,4,float,~,+);
	break;
      }

    case 0x2006:
      {
	int16_t* w1 = (int16_t *) ww1;
	double*  w2 = (double  *) ww2;
	int16_t z1;
	double  z2;

	HQ(2,8,double,~,+);
	break;
      }

    case 0x3000:
      {
	int32_t* w1 = (int32_t *) ww1;
	SCM   *  w2 = (SCM    *)  ww2;
	int32_t z1;
	SCM     z2;
	HSCM1(4,~,scm_from_int32);
	break;
      }

    case 0x3030:
      {
	int32_t* w1 = (int32_t *) ww1;
	int32_t* w2 = (int32_t *) ww2;
	int32_t z1;
	int32_t z2;

	H(4,4,int32_t);
	break;
      }
      
    case 0x3040:
      {
	int32_t* w1 = (int32_t *) ww1;
	int64_t* w2 = (int64_t *) ww2;
	int32_t z1;
	int64_t z2;

	H(4,8,int64_t);
	break;
      }

    case 0x3005:
      {
	int32_t* w1 = (int32_t *) ww1;
	float*   w2 = (float   *) ww2;
	int32_t z1;
	float   z2;

	HQ(4,4,float,~,+);
	break;
      }

    case 0x3006:
      {
	int32_t* w1 = (int32_t *) ww1;
	double*  w2 = (double  *) ww2;
	int32_t z1;
	double  z2;

	HQ(4,8,double,~,+);
	break;
      }

    case 0x4000:
      {
	int64_t* w1 = (int64_t *) ww1;
	SCM   *  w2 = (SCM    *)  ww2;
	int64_t z1;
	SCM     z2;
	HSCM1(8,~,scm_from_int64);
	break;
      }

    case 0x4040:
      {
	int64_t* w1 = (int64_t *) ww1;
	int64_t* w2 = (int64_t *) ww2;
	int64_t z1;
	int64_t z2;

	H(8,8,int64_t);
	break;
      }

    case 0x4005:
      {
	int64_t* w1 = (int64_t *) ww1;
	float*   w2 = (float   *) ww2;
	int64_t z1;
	float   z2;

	HQ(8,4,float,~,+);
	break;
      }

    case 0x4006:
      {
	int64_t* w1 = (int64_t *) ww1;
	double*  w2 = (double  *) ww2;
	int64_t z1;
	double  z2;

	HQ(8,8,double,~,+);

	break;
      }

    case 0x5000:
      {
	float *  w1 = (float *) ww1;
	SCM   *  w2 = (SCM   *)  ww2;
	float z1;
	SCM   z2;
	HSCM1(4,+,scm_from_double);
	break;
      }

    case 0x505:
      {
	float*   w1 = (float   *) ww1;
	float*   w2 = (float   *) ww2;
	float  z1;
	float  z2;

	HQ(4,4,float,+,+);
	break;
      }

    case 0x506:
      {
	float*   w1 = (float   *) ww1;
	double*  w2 = (double  *) ww2;
	float  z1;
	double z2;

	HQ(4,8,double,+,+);
	break;
      }

    case 0x6000:
      {
	double* w1 = (double *) ww1;
	SCM   * w2 = (SCM    *)  ww2;
	double  z1;
	SCM     z2;
	HSCM1(8,+,scm_from_double);
	break;
      }

    case 0x605:
      {
	double*  w1 = (double  *) ww1;
	float*   w2 = (float   *) ww2;
        double z1;
	float  z2;

	HQ(8,4,float,+,+);
	break;
      }

    case 0x606:
      {
	double*  w1 = (double  *) ww1;
	double*  w2 = (double  *) ww2;
	double z1;
	double z2;

	HQ(8,8,double,+,+);
	break;
      }

    default:
      scm_misc_error("superbits-copy!","was not able to decode type info for ~a", scm_list_1(scm_from_int(t)));
    }
  
  return SCM_UNDEFINED;
}
#undef FUNC_NAME
#undef FUNCTION
#undef FUNCTIONB
#undef E
#undef B
#undef U
#undef V

/*

SCM_DEFINE(super_bits_binary1, "super-bits-binary1", 7, 0, 0,
	   (SCM x1_,
	    SCM b2, SCM i2_, SCM j2_, SCM d2_, SCM doinv, SCM kind_),
	   "bit operations for fast execution")
#define FUNC_NAME s_super_bits_binary
{
  int kind = scm_to_int (kind_);
  
  int i2   = scm_to_int(i2_);
  int j2   = scm_to_int(j2_);
  int d2   = scm_to_int(d2_);

  j2 = (d2 > 0) ? j2 + 1 : j2 - 1;
  
  uint64_t bits2 = scm_to_uint64(SCM_STRUCT_SLOT_REF(b2,3));

  SCM v2 = SCM_STRUCT_SLOT_REF(b2,0);

  uint64_t *w2 = (uint64_t *)X(v2);

  uint64_t x1 = scm_to_uint64(x1_);
  
  int k2 = (i_invert & bits2)         ? 1 : 0;
  int k3 = (scm_is_true(doinv) && k2) ? 2 : 0;

#define E(N,A,C,OP,OP1,OP2)					\
  case N:							\
  {								\
    k2 = i2;							\
    do								\
      {								\
	w2[k2] = (A((OP1(x1)) OP (OP2(C(w2[k2])))));		\
	k2 += d2;						\
      } while (k2 != j2);					\
  }								\
  break;

#define B(N,A,OP1)						\
  case N:							\
    {								\
      k2 = i2;							\
      do							\
	{							\
	  w2[k2] = (A((OP1(x1))));				\
	  k2 += d2;						\
	} while ( k2 != j2);					\
    }								\
    break;

#define FUNCTION(K,OP,OP1,OP2)				\
  switch (k2 | (K ^ k3))				\
    {							\
      E(0,+,+,OP,OP1,OP2);				\
      E(1,+,~,OP,OP1,OP2);				\
      E(2,~,+,OP,OP1,OP2);				\
      E(3,~,~,OP,OP1,OP2);				\
    }

#define FUNCTIONB(OP)					\
  switch (k3>>1)					\
    {						        \
      B(0,+,OP);					\
      B(1,~,OP);					\
    }

    switch (kind)
      {
      case 0:
	FUNCTION(0,&,+,+);
	break;
	
      case 1:
	FUNCTION(0,|,+,+);
	break;
	
      case 2:
	FUNCTION(0,^,+,+);
	break;
	
      case 3:
	FUNCTION(0,&,+,~);
	break;
	
      case 4:
	FUNCTIONB(+);
	break;
	
      case 5:
	FUNCTIONB(~);
	break;
	
      case 6:
	FUNCTION(4,^,+,+);
	break;
      }

    return SCM_UNDEFINED;
}
#undef FUNC_NAME
#undef FUNCTION
#undef FUNCTIONB
#undef E
#undef B
#undef U
#undef V
*/

/*
  // X additional contribution
  // Y multiplicative contribution
  // A negate/invert/nothing
  // T swap or no swap
#define E(N,X1,Y1,A1,T1,X2,Y2,A2,T2)				\
  case N:							\
  {								\
    int k1 = i1, k2 = i2;					\
    do								\
      {								\
	w2[k2] = T2(X2 + Y2*(A2(X1 + Y1*(A1(T1(w1[k1]))))));	\
	k1 += d1, k2 += d2;					\
      } while (k1 != j1);					\
  }								\
  break;

*/
