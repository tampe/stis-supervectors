SCM_DEFINE(super_bits_masked_binary, "super-bits-masked-binary", 10, 0, 0,
	   (SCM b1, SCM i1_, SCM j1_, SCM d1_,
	    SCM b2, SCM i2_, SCM j2_, SCM d2_, SCM doinv_data, SCM kind_),
	   "masked bit operations for fast execution")
#define FUNC_NAME s_super_bits_binary
{
  SCM doinv = SCM_CAR(doinv_data);
  SCM data  = SCM_CDR(doinv_data);
  
  int kind = scm_to_int (kind_);
  
  int i1   = scm_to_int(i1_);
  int j1   = scm_to_int(j1_);
  int d1   = scm_to_int(d1_);
  
  int i2   = scm_to_int(i2_);
  int j2   = scm_to_int(j2_);
  int d2   = scm_to_int(d2_);

  j1 = (d1 > 0) ? j1 + 1 : j1 - 1;
  j2 = (d2 > 0) ? j2 + 1 : j2 - 1;

  uint64_t bits1 = scm_to_uint64(SCM_STRUCT_SLOT_REF(b1,3));
  uint64_t bits2 = scm_to_uint64(SCM_STRUCT_SLOT_REF(b2,3));

  SCM v1 = SCM_STRUCT_SLOT_REF(b1,0);
  SCM v2 = SCM_STRUCT_SLOT_REF(b2,0);

  uint64_t *w1 = (uint64_t *)X(v1);
  uint64_t *w2 = (uint64_t *)X(v2);

  int k1 = (i_invert & bits1)         ? 2 : 0;
  int k2 = (i_invert & bits2)         ? 1 : 0;
  int k3 = (scm_is_true(doinv) && k2) ? 4 : 0;

  uint64_t mask   = scm_to_uint64 (SCM_CAR   (data));
  int64_t  shift  = scm_to_int64  (SCM_CADR  (data));
  uint64_t mask2  = scm_to_uint64 (SCM_CADDR (data));
  uint64_t mask2c = ~mask2;

  if (mask == 0) return SCM_UNDEFINED;
  
#define U(A,B,C,OP,OP1,OP2,SHIFT,sh)					\
  {									\
    int k1 = i1, k2 = i2;						\
    do {								\
      w2[k2] = (w2[k2] & mask2c) |					\
	(mask2 & (A(((mask & (OP1(B(w1[k1])))) SHIFT sh)		\
		    OP							\
		    (OP2(C(w2[k2]))))));				\
      k1 += d1;								\
      k2 += d2;								\
    } while (k1 != j1);							\
  }
  
#define E(N,A,B,C,OP,OP1,OP2)						\
  case N:								\
    if (shift > 0)							\
      {									\
	U(A,B,C,OP,OP1,OP2,<<,shift);					\
      }									\
    else								\
      {									\
	U(A,B,C,OP,OP1,OP2,>>,(- shift));				\
      }									\
    break;

#define V(A,B,OP1,SHIFT,sh)					\
  {								\
    int k1 = i1;						\
    int k2 = i2;						\
    do{								\
      w2[k2] = (w2[k2] & mask2c) |				\
	(mask2 & (A((mask & (OP1(B(w1[k1])))) SHIFT sh)));	\
      k1 += d1;							\
      k2 += d2;							\
    } while (k1 != j1);						\
  }
  
#define B(N,A,B,OP1)							\
  case N:								\
    if (shift > 0)							\
      {									\
	V(A,B,OP1,<<,shift);						\
      }									\
    else								\
      {									\
	V(A,B,OP1,>>,(- shift));					\
      }									\
    break;

#define FUNCTION(I,OP,OP1,OP2)				\
  switch (k1 | k2 | (I ^ k3))				\
    {							\
      E(0,+,+,+,OP,OP1,OP2);				\
      E(1,+,+,~,OP,OP1,OP2);				\
      E(2,+,~,+,OP,OP1,OP2);				\
      E(3,+,~,~,OP,OP1,OP2);				\
      E(4,~,+,+,OP,OP1,OP2);				\
      E(5,~,+,~,OP,OP1,OP2);				\
      E(6,~,~,+,OP,OP1,OP2);				\
      E(7,~,~,~,OP,OP1,OP2);				\
    }

#define FUNCTIONB(OP)					\
  switch (k1>>1 | k2>>1)				\
    {						        \
      B(0,+,+,OP);					\
      B(1,+,~,OP);					\
      B(2,~,+,OP);					\
      B(3,~,~,OP);					\
    }

  switch (kind)
    {
    case 0:
      FUNCTION(0,&,+,+);
      break;
      
    case 1:
      FUNCTION(0,|,+,+);
      break;
      
    case 2:
      FUNCTION(0,^,+,+);
      break;
      
    case 3:
      FUNCTION(0,&,+,~);
      break;
      
    case 4:
      FUNCTIONB(+);
      break;
      
    case 5:
      FUNCTIONB(~);
      break;
      
    case 6:
      FUNCTION(4,^,+,+);
      break;      
    }

    return SCM_UNDEFINED;
}
#undef FUNC_NAME
#undef FUNCTION
#undef FUNCTIONB
#undef E
#undef B
#undef U
#undef V


SCM_DEFINE(super_bits_masked_binary1, "super-bits-masked-binary1", 7, 0, 0,
	   (SCM x1_,
	    SCM b2, SCM i2_, SCM j2_, SCM d2_, SCM doinv_data, SCM kind_),
	   "masked bit operations for fast execution")
#define FUNC_NAME s_super_bits_binary
{
  SCM doinv = SCM_CAR(doinv_data);
  SCM data  = SCM_CDR(doinv_data);
  
  int kind = scm_to_int (kind_);
    
  int i2   = scm_to_int(i2_);
  int j2   = scm_to_int(j2_);
  int d2   = scm_to_int(d2_);

  j2 = (d2 > 0) ? j2 + 1 : j2 - 1;

  uint64_t bits2 = scm_to_uint64(SCM_STRUCT_SLOT_REF(b2,3));

  SCM v2 = SCM_STRUCT_SLOT_REF(b2,0);

  uint64_t *w2 = (uint64_t *)X(v2);
  
  int k2 = (i_invert & bits2)         ? 1 : 0;
  int k3 = (scm_is_true(doinv) && k2) ? 2 : 0;

  uint64_t mask   = scm_to_uint64 (SCM_CAR   (data));
  int64_t  shift  = scm_to_int64  (SCM_CADR  (data));
  uint64_t mask2  = scm_to_uint64 (SCM_CADDR (data));
  uint64_t mask2c = ~mask2;

  uint64_t x1 = scm_to_uint64(x1_);

  if (mask == 0) return SCM_UNDEFINED;
  
#define U(A,C,OP,OP1,OP2,SHIFT,sh)					\
  {									\
    int k2 = i2;							\
    uint64_t y = ((mask & (OP1 x1)) SHIFT sh);				\
    do {								\
      w2[k2] = (w2[k2] & mask2c) |					\
	(mask2 & (A(y OP (OP2(C(w2[k2]))))));				\
      k2 += d2;								\
    } while (k2 != j2);							\
  }
  
#define E(N,A,C,OP,OP1,OP2)						\
  case N:								\
    if (shift > 0)							\
      {									\
	U(A,C,OP,OP1,OP2,<<,shift);					\
      }									\
    else								\
      {									\
	U(A,C,OP,OP1,OP2,>>,(- shift));					\
      }									\
    break;

#define V(A,SHIFT,OP1,sh)					\
  {								\
    int k2 = i2;						\
    uint64_t y  = (mask2 & (A( (mask & (OP1 x1)) SHIFT sh)));	\
    do{								\
      w2[k2] = (w2[k2] & mask2c) | y;				\
      k2 += d2;							\
    } while (k2 != j2);						\
  }
  
#define B(N,A,OP1)							\
  case N:								\
    if (shift > 0)							\
      {									\
	V(A,<<,OP1,shift);						\
      }									\
    else								\
      {									\
	V(A,>>,OP1,(- shift));						\
      }									\
    break;

#define FUNCTION(I,OP,OP1,OP2)				\
  switch (k2 | (I ^ k3))				\
    {							\
      E(0,+,+,OP,OP1,OP2);				\
      E(1,+,~,OP,OP1,OP2);				\
      E(2,~,+,OP,OP1,OP2);				\
      E(3,~,~,OP,OP1,OP2);				\
    }

#define FUNCTIONB(OP)					\
  switch (k3 >> 1)					\
    {						        \
      B(0,+,OP);					\
      B(1,~,OP);					\
    }

  switch (kind)
    {
    case 0:
      FUNCTION(0,&,+,+);
      break;
      
    case 1:
      FUNCTION(0,|,+,+);
      break;
      
    case 2:
      FUNCTION(0,^,+,+);
      break;
      
    case 3:
      FUNCTION(0,&,+,~);
      break;
      
    case 4:
      FUNCTIONB(+);
      break;
      
    case 5:
      FUNCTIONB(~);
      break;
      
    case 6:
      FUNCTION(2,^,+,+);
      break;      
    }

    return SCM_UNDEFINED;
}
#undef FUNC_NAME
#undef FUNCTION
#undef FUNCTIONB
#undef E
#undef B
#undef U
#undef V
