/*
Toolbox for copy! differently bytevectors and vectors

superfast for guile vectors and bytevectors Copying for the same kind of 
bytevectors are probably as fast as memcpy for a good compiler and is 
heavily optimized compared to working in guile atm. So it makes sense 
to have specoific c-code to handle these operations.

Copyright: Stefan Israelsson Tampe (stefan.itampe@gmail.com) 2022
GPL v3
*/

#include <libguile.h>

#define X(bv) SCM_BYTEVECTOR_CONTENTS(bv)
#define Y(bv) SCM_I_VECTOR_WELTS(bv)

void swap_2(void *x)
  {		
    char *v;
    v = (char *) x;	
    char t = v[0];
    v[0] = v[1];
    v[1] = t;
  }

void swap_4(void *x)
{
  char *v;
  v = (char *) x;
  char t = v[0];				
  v[0] = v[3];				
  v[3] = t;					
  t    = v[1];				
  v[1] = v[2];			
  v[2] = t;				
}

void swap_8(void *x)
{			
  char *v;	
  v = (char *) x;	
  char t = v[0];
  v[0] = v[7];
  v[7] = t;	
		
  t    = v[1];
  v[1] = v[6];
  v[6] = t;	
      		
  t    = v[2];
  v[2] = v[5];
  v[5] = t;	
		
  t    = v[3];
  v[3] = v[4];
  v[5] = t;	
}

#define K(HSCM__)						\
  {								\
  uint64_t k1 = i1;						\
  uint64_t k2 = i2;						\
  if (d1 > 0 && d2 > 0)						\
    {								\
      HSCM__(<,<);						\
    }								\
  else if (d1 > 0 && d2 < 0)					\
    {								\
      HSCM__(<,>);						\
    }								\
  else if (d1 < 0 && d2 > 0)					\
    {								\
      HSCM__(>,<);						\
    }								\
  else if (d1 < 0 && d2 < 0)					\
    {								\
      HSCM__(>,>);						\
    }								\
  }

#define K2(HSCM__,A,B)						\
  {								\
   uint64_t k1 = i1;						\
   uint64_t k2 = i2;						\
   if (d1 > 0 && d2 > 0)					\
     {								\
       HSCM__(<,<,A,B);						\
     }								\
   else if (d1 > 0 && d2 < 0)					\
     {								\
       HSCM__(<,>,A,B);						\
     }								\
   else if (d1 < 0 && d2 > 0)					\
     {								\
       HSCM__(>,<,A,B);						\
     }								\
   else if (d1 < 0 && d2 < 0)					\
     {								\
       HSCM__(>,>,A,B);						\
     }								\
  }

#define K3(HSCM__,A,B,C)					\
  {								\
    uint64_t k1 = i1;						\
    uint64_t k2 = i2;						\
    if (d1 > 0 && d2 > 0)					\
      {								\
	HSCM__(<,<,A,B,C);					\
      }								\
    else if (d1 > 0 && d2 < 0)					\
      {								\
	HSCM__(<,>,A,B,C);					\
      }								\
    else if (d1 < 0 && d2 > 0)					\
      {								\
	HSCM__(>,<,A,B,C);					\
      }								\
    else if (d1 < 0 && d2 < 0)					\
      {								\
	HSCM__(>,>,A,B,C);					\
      }								\
  }

#define E_(OP1,OP2,T1,T2,TP2)						\
  {									\
    while (k1 OP1 j1 && k2 OP2 j2)					\
      {									\
	z1 = w1[k1];							\
	T1((void *) &z1);						\
        z2 = TP2(z1);							\
	T2((void *) &z2);						\
	w2[k2] = z2;							\
									\
	k1 += d1;							\
	k2 += d2;							\
      }									\
    									\
    break;								\
  }

#define E1_(P1,P2,OP1,OP2,T1,T2,TP2)					\
  {									\
    uint64_t k1 = i1;							\
    uint64_t k2 = i2;							\
    while (k1 OP1 j1 && k2 OP2 j2)					\
      {									\
	z1 = w1[k1];							\
	T1((void *) &z1);						\
        z2 = TP2(z1);							\
	T2((void *) &z2);						\
	w2[k2] = z2;							\
									\
	k1 P1;								\
	k2 P2;								\
      }									\
    									\
    break;								\
  }

  
#define E(T1,T2,TP)				\
    {						\
      int xxx = 0;				\
      switch (d1)				\
	{					\
	case 1:					\
	  {					\
	    switch (d2)				\
	      {					\
	      case 1:				\
		{				\
		  E1_(++,++,<,<,T1,T2,TP);	\
		  break;			\
		}				\
	      case -1:				\
		{				\
		  E1_(++,--,<,>,T1,T2,TP);	\
		  break;			\
		}				\
	      default:				\
		{				\
		  xxx = 1;			\
		}				\
	      }					\
	    break;				\
	  }					\
     						\
	case -1:				\
	  {					\
	    switch (d2)				\
	      {					\
	      case 1:				\
		{				\
		  E1_(--,++,>,<,T1,T2,TP);	\
		  break;			\
		}				\
	      case -1:				\
		{				\
		  E1_(--,--,>,>,T1,T2,TP);	\
		  break;			\
		}				\
	      default:				\
		{				\
		  xxx = 1;			\
		}				\
	      }					\
	    break;				\
	  }					\
	default:				\
	  {					\
	    xxx = 1;				\
	  }					\
	}					\
      if (xxx)					\
	{					\
	  K3(E_,T1,T2,TP);			\
	}					\
    }
  

#define ESCM1_(OP1,OP2,T1,T2)						\
  {									\
    while (k1 OP1 j1 && k2 OP2 j2)					\
      {									\
	z1 = w1[k1];							\
	T1((void *) &z1);						\
	z2 = T2(z1);							\
	w2[k2] = z2;							\
    									\
	k1 += d1;							\
	k2 += d2;							\
      }									\
  									\
    break;								\
  }
#define ESCM1(T1,T2) K2(ESCM1_,T1,T2)

#define HSCM_(OP1,OP2)						\
  {								\
    while (k1 OP1 j1 && k2 OP2 j2)				\
      {								\
	w2[k2] = w1[k1];					\
								\
	k1 += d1;						\
	k2 += d2;						\
      }								\
								\
    break;							\
  }

#define HSCM K(HSCM_)


#define H2(T1,mm2,TP2)				\
  if (e2)					\
    {						\
      switch (mm2)				\
	{					\
	case 1:					\
	  {					\
	    E(T1,(void),TP2);			\
	    break;				\
	  }					\
	case 2:					\
	  {					\
	    E(T1,swap_2,TP2);			\
	    break;				\
	  }					\
	case 4:					\
	  {					\
	    E(T1,swap_4,TP2);			\
	    break;				\
	  }					\
	case 8:					\
	  {					\
	    E(T1,swap_8,TP2);			\
	    break;				\
	  }					\
	}					\
    }						\
  else						\
    {						\
      E(T1,(void),TP2);				\
    }
   
#define HQ(mm1,mm2,TP2)					\
  if (e1)						\
    {							\
      switch (mm1)					\
	{						\
	case 1:						\
	  {						\
	    H2((void),mm2,TP2);				\
	    break;					\
	  }						\
	case 2:						\
	  {						\
	    H2(swap_2,mm2,TP2);				\
	    break;					\
	  }						\
	case 4:						\
	  {						\
	    H2(swap_4,mm2,TP2);				\
	    break;					\
	  }						\
	case 8:						\
	  {						\
	    H2(swap_8,mm2,TP2);				\
	    break;					\
	  }						\
	}						\
    }							\
  else							\
    {							\
      H2((void),mm2,TP2);				\
    }

#define HSCM1(mm1,T2)				\
  if (e1)					\
    {						\
      switch (mm1)				\
	{					\
	case 1:					\
	  ESCM1((void),T2);			\
	  break;				\
	case 2:					\
	  ESCM1(swap_2,T2);			\
	  break;				\
	case 4:					\
	  ESCM1(swap_4,T2);			\
	  break;				\
	case 8:					\
	  ESCM1(swap_8,T2);			\
	  break;				\
	}					\
    }						\
  else						\
    {						\
      ESCM1((void),T2);				\
    }

#define H(mm1,mm2,TP2) HQ(mm1,mm2,TP2)

uint64_t i_scm      = 1;
uint64_t i_signed   = 1;
uint64_t i_end      = 1;
uint64_t i_float    = 1;
uint64_t i_native   = 1;
uint64_t m_mask     = 0;

/*
Assumes a vector with the coding of the bitvector,
the input is a vector of size 6 such that we have the 
fields,

i_scm    : if it's a scm vector or not
i_signed : is it is a signed bytevector
i_end    : endianness for bytevector
i_float  : if its a float bytevector
i_native : native endianness for byte vectors
m_mask   : wordsize of bytevector is masked out by this

m = 0 : 1 byte
m = 2 : 2 byte
m = 3 : 4 byte
m = 4 : 8 byte
 */
SCM_DEFINE(set_copy_constants,"vector-copy-constants",1,0,0,(SCM x),
	   "type constants")
#define FUNC_NAME s_set_supervector_constants  
{
  SCM *w = (SCM *) Y(x);
  i_scm     = scm_to_uint64(w[0]);
  i_signed  = scm_to_uint64(w[1]);
  i_end     = scm_to_uint64(w[2]);
  i_float   = scm_to_uint64(w[3]);
  i_native  = scm_to_uint64(w[4]);
  m_mask    = scm_to_uint64(w[5]);
  
  return SCM_UNDEFINED;
}
#undef FUNC_NAME

/*
We expect an incomming vector elements
start1 inclusive
end1   exclusive
jump1  (+1-1)
start2 inclusive
end2   exclusive
jump2  (+1,-1)
bits1             se above function
bits2             se above function
v1                vector or bytevector as source
v2                vector or bytevector as sink


Note: the coding of the size of the bin are
m = 0 : 1 byte
m = 1 : 2 byte
m = 2 : 4 byte
m = 3 : 8 byte
*/

SCM_DEFINE(bits_copy, "vector-copy-raw!", 1, 0, 0,
	   (SCM data_),
	   "bit operations for fast execution")
#define FUNC_NAME s_super_bits_copy
{
  if ((!scm_is_vector(data_) || scm_c_vector_length(data_) != 10))
    scm_misc_error ("vector-copy-raw!",
                    "not correct input must be vector of size 10", SCM_EOL);

  SCM *data =  (SCM *)Y(data_);
  // i -> exclusive j, jumping in the direction d
  // j >= i if d = 1, else i >= j if d = -1
  uint64_t i1   = scm_to_uint64(data[0]);
  uint64_t j1   = scm_to_uint64(data[1]);
  int64_t  d1   = scm_to_int64(data[2]);

  uint64_t i2   = scm_to_uint64(data[3]);
  uint64_t j2   = scm_to_uint64(data[4]);
  int64_t  d2   = scm_to_int64(data[5]);

  if (j1< 0 || i1 < 0 || (j1-i1)*d1 < 0 || d1 == 0)
    scm_misc_error("vector-copy-raw!", "faulty iteration data for source",
                   SCM_EOL);

  if (j2< 0 || i2 < 0 || (j2-i2)*d2 < 0 || d2 == 0)
    scm_misc_error("vector-copy-raw!", "faulty iteration data for sink",
                   SCM_EOL);
      
  uint64_t bits1 = scm_to_uint64(data[6]);
  uint64_t bits2 = scm_to_uint64(data[7]);

  int scm1 = (bits1 & i_scm) > 0 ? 1 : 0;
  int scm2 = (bits2 & i_scm) > 0 ? 1 : 0;
  
  uint64_t *ww1 = NULL;
  uint64_t *ww2 = NULL;
  
  if (scm1)
    ww1 = (uint64_t *)Y(data[8]);
  else
    ww1 = (uint64_t *)X(data[8]);

  if (scm2)
    ww2 = (uint64_t *)Y(data[9]);
  else
    ww2 = (uint64_t *)X(data[9]);

  int m1 = bits1 & m_mask;
  int m2 = bits2 & m_mask;

  int e1 = 0;
  if (m1 > 0)
    {
      if ((((bits1 & i_end) ? 1 : 0) ^ ((bits1 & i_native) > 0) ? 1 : 0) > 0)
	e1 = 1;
    }

  int e2 = 0;
  if (m2 > 0)
    {
      if ((((bits2 & i_end) ? 1 : 0) ^ ((bits2 & i_native) > 0) ? 1 : 0) > 0)
	e2 = 1;
    }

  int t1 = 0;
  if (scm1)
    t1 = 0;
  else if ((bits1 & i_float ) > 0)
    {
      t1 = (m1 == 2) ? 5 : 6;
    }
  else if ((bits1 & i_signed) > 0)
    {
      t1 = (m1+1) << 4;      
    }
  else
    {
      t1 = (m1+1);      
    }

  int t2 = 0;
  if (scm2)
    t2 = 0;
  else if ((bits2 & i_float ) > 0)
    {
      t2 = (m2 == 2) ? 5 : 6;
    }
  else if ((bits2 & i_signed) > 0)
    {
      t2 = (m2+1) << 4;      
    }
  else
    {
      t2 = (m2+1);      
    }

  int t = (t1 << 8) + t2;

  switch (t)
    {
    case 0:
      {
	SCM* w1 = (SCM *) ww1;
	SCM* w2 = (SCM *) ww2;
	SCM z1;
	SCM z2;
	HSCM;
	break;
      }

    case 0x101:
      {
	uint8_t* w1 = (uint8_t *) ww1;
	uint8_t* w2 = (uint8_t *) ww2;
	uint8_t z1;
	uint8_t z2;
	H(1,1,(uint8_t));
	break;
      }

    case 0x100:
      {
	uint8_t* w1 = (uint8_t *) ww1;
	SCM    * w2 = (SCM     *) ww2;
	uint8_t z1;
	SCM     z2;
	HSCM1(1,scm_from_uint8);
	break;
      }

    case 0x102:
      {
	uint8_t* w1 = (uint8_t *) ww1;
	uint16_t* w2 = (uint16_t *) ww2;
	uint8_t z1;
	uint16_t z2;
	
	H(1,2,(uint16_t));
	break;
      }
      
    case 0x103:
      {
	uint8_t* w1 = (uint8_t *) ww1;
	uint32_t* w2 = (uint32_t *) ww2;
	uint8_t z1;
	uint16_t z2;
	
	H(1,4,(uint32_t));
	break;
      }
      
    case 0x104:
      {
	uint8_t* w1 = (uint8_t *) ww1;
	uint64_t* w2 = (uint64_t *) ww2;
	uint8_t z1;
	uint64_t z2;

	H(1,8,(uint64_t));
	break;
      }

    case 0x105:
      {
	uint8_t* w1 = (uint8_t *) ww1;
	float* w2   = (float   *) ww2;
	uint8_t z1;
	float   z2;

	HQ(1,4,(float));
	break;
      }

    case 0x106:
      {
	uint8_t* w1 = (uint8_t *) ww1;
	double* w2  = (double  *) ww2;
	uint8_t z1;
	double  z2;

	HQ(1,8,(double));
	break;
      }
      
    case 0x120:
      {
	uint8_t* w1 = (uint8_t *) ww1;
	int16_t* w2 = (int16_t *) ww2;
	uint8_t z1;
	int16_t z2;

	H(1,2,(int16_t));
	break;
      }

    case 0x130:
      {
	uint8_t* w1 = (uint8_t *) ww1;
	int32_t* w2 = (int32_t *) ww2;	
	uint8_t z1;
	int32_t z2;

	H(1,4,(int32_t));
	break;
      }
      
    case 0x140:
      {
	uint8_t* w1 = (uint8_t *) ww1;
	int64_t* w2 = (int64_t *) ww2;
	uint8_t z1;
	int64_t z2;

	H(1,8,(int64_t));
	break;
      }

    case 0x200:
      {
	uint16_t* w1 = (uint16_t *) ww1;
	SCM    *  w2 = (SCM     *)  ww2;
	uint16_t z1;
	SCM      z2;
	HSCM1(2,scm_from_uint16);
	break;
      }

    case 0x202:
      {
	uint16_t* w1 = (uint16_t *) ww1;
	uint16_t* w2 = (uint16_t *) ww2;
	uint16_t z1;
	uint16_t z2;

	H(2,2,(uint16_t));
	break;
      }
      
    case 0x203:
      {
	uint16_t* w1 = (uint16_t *) ww1;
	uint32_t* w2 = (uint32_t *) ww2;
	uint16_t z1;
	uint32_t z2;

	H(2,4,(uint32_t));
	break;
      }
      
    case 0x204:
      {
	uint16_t* w1 = (uint16_t *) ww1;
	uint64_t* w2 = (uint64_t *) ww2;
	uint16_t z1;
	uint64_t z2;

	H(2,4,(uint64_t));
	break;
      }

    case 0x205:
      {
	uint16_t* w1 = (uint16_t *) ww1;
	float* w2    = (float    *) ww2;
	uint16_t z1;
	float    z2;

	HQ(2,8,(float));
	break;
      }

    case 0x206:
      {
	uint16_t* w1 = (uint16_t *) ww1;
	double* w2   = (double   *) ww2;
	uint16_t z1;
	double   z2;

	HQ(2,4,(double));
	break;
      }
      
    case 0x230:
      {
	uint16_t* w1 = (uint16_t *) ww1;
	int32_t* w2 = (int32_t *) ww2;
	uint16_t z1;
	int32_t z2;

	H(2,4,(uint64_t));
	break;
      }
      
    case 0x240:
      {
	uint16_t* w1 = (uint16_t *) ww1;
	int64_t* w2 = (int64_t *) ww2;
	uint16_t z1;
	int64_t z2;

	H(2,8,(int32_t));
	break;
      }

    case 0x300:
      {
	uint32_t* w1 = (uint32_t *) ww1;
	SCM    *  w2 = (SCM     *)  ww2;
	uint32_t z1;
	SCM      z2;
	HSCM1(4,scm_from_uint32);
	break;
      }

    case 0x303:
      {
	uint32_t* w1 = (uint32_t *) ww1;
	uint32_t* w2 = (uint32_t *) ww2;
	uint32_t z1;
	uint32_t z2;

	H(4,4,(int64_t));
	break;
      }
    case 0x304:
      {
	uint32_t* w1 = (uint32_t *) ww1;
	uint64_t* w2 = (uint64_t *) ww2;
	uint32_t z1;
	uint64_t z2;
	
	H(4,8,(uint32_t));
	break;
	
      }

    case 0x305:
      {
	uint32_t* w1 = (uint32_t *) ww1;
	float*    w2 = (float    *) ww2;
	uint32_t z1;
	float    z2;
	
	HQ(4,4,(float));
	break;
	
      }
      
    case 0x306:
      {
	uint32_t* w1 = (uint32_t *) ww1;
	double*   w2 = (double   *) ww2;
	uint32_t  z1;
	double    z2;
	
	HQ(4,8,(double));
	break;
	
      }
    case 0x340:
      {
	uint32_t* w1 = (uint32_t *) ww1;
	int64_t* w2  = (int64_t *) ww2;
	uint32_t z1;
	int64_t z2;
	
	H(4,8,(uint64_t));
	break;
      }
      

    case 0x400:
      {
	uint64_t* w1 = (uint64_t *) ww1;
	SCM    *  w2 = (SCM     *)  ww2;
	uint64_t z1;
	SCM      z2;
	HSCM1(8,scm_from_uint64);
	break;
      }

    case 0x404:
      {
	uint64_t* w1 = (uint64_t *) ww1;
	uint64_t* w2 = (uint64_t *) ww2;
	uint64_t z1;
	uint64_t z2;

	H(8,8,(int64_t));
	break;
      }

    case 0x405:
      {
	uint64_t* w1 = (uint64_t *) ww1;
	float*    w2 = (float    *) ww2;
	uint64_t z1;
	float    z2;

	HQ(8,4,(float));
	break;
      }

    case 0x406:
      {
	uint64_t* w1 = (uint64_t *) ww1;
	double*   w2 = (double   *) ww2;
	uint64_t z1;
	double   z2;

	HQ(8,8,(double));
	break;
      }

    case 0x1000:
      {
	int8_t* w1 = (int8_t *) ww1;
	SCM   * w2 = (SCM    *)  ww2;
	int8_t z1;
	SCM    z2;
	HSCM1(1,scm_from_int8);
	break;
      }

    case 0x1010:
      {
	int8_t* w1 = (int8_t *) ww1;
	int8_t* w2 = (int8_t *) ww2;
	int8_t z1;
	int8_t z2;

	H(1,1,(int8_t));
	break;
      }

    case 0x1020:
      {
	int8_t* w1 = (int8_t *) ww1;
	int16_t* w2 = (int16_t *) ww2;
	int8_t z1;
	int16_t z2;

	H(1,2,(int16_t));
	break;
      }

    case 0x1030:
      {
	int8_t* w1 = (int8_t *) ww1;
	int32_t* w2 = (int32_t *) ww2;
	int8_t z1;
	int32_t z2;

	H(1,4,(int32_t));
	break;
      }

    case 0x1040:
      {
	int8_t* w1 = (int8_t *) ww1;
	int64_t* w2 = (int64_t *) ww2;
	int8_t z1;
	int64_t z2;

	H(1,8,(int64_t));
	break;
      }

    case 0x1005:
      {
	int8_t* w1 = (int8_t *) ww1;
	float*  w2 = (float  *) ww2;
	int8_t z1;
	float  z2;

	HQ(1,4,(float));
	break;
      }

    case 0x1006:
      {
	int8_t* w1 = (int8_t *) ww1;
	double* w2 = (double *) ww2;
	int8_t z1;
	double z2;

	HQ(1,8,(double));
	break;
      }
      
    case 0x2000:
      {
	int16_t* w1 = (int16_t *) ww1;
	SCM   *  w2 = (SCM    *)  ww2;
	int16_t z1;
	SCM     z2;
	HSCM1(2,scm_from_int16);
	break;
      }

    case 0x2020:
      {
	int16_t* w1 = (int16_t *) ww1;
	int16_t* w2 = (int16_t *) ww2;
	int16_t z1;
	int16_t z2;

	H(2,2,(int16_t));
	break;
      }
      
    case 0x2030:
      {
	int16_t* w1 = (int16_t *) ww1;
	int32_t* w2 = (int32_t *) ww2;
	int16_t z1;
	int32_t z2;

	H(2,4,(int32_t));
	break;
      }

    case 0x2040:
      {
	int16_t* w1 = (int16_t *) ww1;
	int64_t* w2 = (int64_t *) ww2;
	int16_t z1;
	int64_t z2;

	H(2,8,(int64_t));
	break;
      }

    case 0x2005:
      {
	int16_t* w1 = (int16_t *) ww1;
	float*   w2 = (float   *) ww2;
	int16_t z1;
	float   z2;

	HQ(2,4,(float));
	break;
      }

    case 0x2006:
      {
	int16_t* w1 = (int16_t *) ww1;
	double*  w2 = (double  *) ww2;
	int16_t z1;
	double  z2;

	HQ(2,8,(double));
	break;
      }

    case 0x3000:
      {
	int32_t* w1 = (int32_t *) ww1;
	SCM   *  w2 = (SCM    *)  ww2;
	int32_t z1;
	SCM     z2;
	HSCM1(4,scm_from_int32);
	break;
      }

    case 0x3030:
      {
	int32_t* w1 = (int32_t *) ww1;
	int32_t* w2 = (int32_t *) ww2;
	int32_t z1;
	int32_t z2;

	H(4,4,(int32_t));
	break;
      }
      
    case 0x3040:
      {
	int32_t* w1 = (int32_t *) ww1;
	int64_t* w2 = (int64_t *) ww2;
	int32_t z1;
	int64_t z2;

	H(4,8,(int64_t));
	break;
      }

    case 0x3005:
      {
	int32_t* w1 = (int32_t *) ww1;
	float*   w2 = (float   *) ww2;
	int32_t z1;
	float   z2;

	HQ(4,4,(float));
	break;
      }

    case 0x3006:
      {
	int32_t* w1 = (int32_t *) ww1;
	double*  w2 = (double  *) ww2;
	int32_t z1;
	double  z2;

	HQ(4,8,(double));
	break;
      }

    case 0x4000:
      {
	int64_t* w1 = (int64_t *) ww1;
	SCM   *  w2 = (SCM    *)  ww2;
	int64_t z1;
	SCM     z2;
	HSCM1(8,scm_from_int64);
	break;
      }

    case 0x4040:
      {
	int64_t* w1 = (int64_t *) ww1;
	int64_t* w2 = (int64_t *) ww2;
	int64_t z1;
	int64_t z2;

	H(8,8,(int64_t));
	break;
      }

    case 0x4005:
      {
	int64_t* w1 = (int64_t *) ww1;
	float*   w2 = (float   *) ww2;
	int64_t z1;
	float   z2;

	HQ(8,4,(float));
	break;
      }

    case 0x4006:
      {
	int64_t* w1 = (int64_t *) ww1;
	double*  w2 = (double  *) ww2;
	int64_t z1;
	double  z2;

	HQ(8,8,(double));

	break;
      }

    case 0x5000:
      {
	float *  w1 = (float *) ww1;
	SCM   *  w2 = (SCM   *)  ww2;
	float z1;
	SCM   z2;
	HSCM1(4,scm_from_double);
	break;
      }

    case 0x505:
      {
	float*   w1 = (float   *) ww1;
	float*   w2 = (float   *) ww2;
	float  z1;
	float  z2;

	HQ(4,4,(float));
	break;
      }

    case 0x506:
      {
	float*   w1 = (float   *) ww1;
	double*  w2 = (double  *) ww2;
	float  z1;
	double z2;

	HQ(4,8,(double));
	break;
      }

    case 0x6000:
      {
	double* w1 = (double *) ww1;
	SCM   * w2 = (SCM    *)  ww2;
	double  z1;
	SCM     z2;
	HSCM1(8,scm_from_double);
	break;
      }

    case 0x605:
      {
	double*  w1 = (double  *) ww1;
	float*   w2 = (float   *) ww2;
        double z1;
	float  z2;

	HQ(8,4,(float));
	break;
      }

    case 0x606:
      {
	double*  w1 = (double  *) ww1;
	double*  w2 = (double  *) ww2;
	double z1;
	double z2;

	HQ(8,8,(double));
	break;
      }      
	
    default:
      scm_misc_error("vector-copy!","was not able to decode type info for ~a", scm_list_1(scm_from_int(t)));
    }

  return SCM_UNDEFINED;
}

#undef FUNC_NAME
#undef FUNCTION
#undef FUNCTIONB
#undef E
#undef B
#undef U
#undef V
#undef X
#undef Y
#undef K
#undef K1
#undef K2
#undef E
#undef E_
#undef ECM1
#undef ECM1_
#undef HCM
#undef HCM_
#undef H
#undef H2
#undef HQ
#undef HSCM1
