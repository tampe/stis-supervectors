;;HACK
(define-syntax-rule (define-module- name args ...)
  (cond-expand
   (guile-3
    (define-module name
      #:declarative? #f
      args ...))
   (guile
    (define-module name args ...))))


(define-module- (ice-9 supervectors)
  #:use-module (ice-9 supervectors core)
  #:use-module (ice-9 supervectors basic)
  #:use-module (ice-9 supervectors make)
  #:use-module (ice-9 supervectors functional)
  #:use-module (ice-9 supervectors copy)
  #:use-module (ice-9 supervectors typed)
  #:use-module (ice-9 supervectors append)
  #:use-module (ice-9 supervectors util)
  #:use-module (ice-9 supervectors printer)
  
  #:re-export (;; CORE
	       supervector-length
	       supervector?

	       ;;MAKE
	       make-supervector
	       
	       ;; BASIC	    
	       supervector-u8-ref
	       supervector-s8-ref
	       supervector-u16-ref
	       supervector-s16-ref
	       supervector-u32-ref
	       supervector-s32-ref
	       supervector-u64-ref
	       supervector-s64-ref
	       supervector-ieee-single-ref
	       supervector-ieee-double-ref
	       
	       supervector-u8-set!
	       supervector-s8-set!
	       supervector-u16-set!
	       supervector-s16-set!
	       supervector-u32-set!
	       supervector-s32-set!
	       supervector-u64-set!
	       supervector-s64-set!
	       supervector-ieee-single-set!
	       supervector-ieee-double-set!
	       
	       ;; FUNCTIONAL
	       supervector-tr
	       supervector-fold
	       supervector-fold2
	       supervector-stream
	       supervector-for-each
	       supervector-for-each2
	       supervector-unary
	       supervector-binary
	       

	       ;; COPY
	       supervector-copy!
	       supervector-copy
	       	       
	       ;; TYPED
	       supervector-ref
	       supervector-set!

	       ;; UTIL
	       supervector-count
	       
	       ;; APPEND
	       supervector-append!
	       supervector-prepend!
	       supervector-append
	       supervector-prepend
	       supervector-pop-left!
	       supervector-pop-right!
	       supervector-reverse!
	       supervector-reverse
	       supervector-insert!
	       supervector-remove!
	       )
  
  #:export ())
	   
#|
The idea of this data structure is to note that when we employ full
large datastructure scans we can allow for a much more rich and featurefull
datastructure then a simple bytevector. The reason is that we can divide
the data in byte chunks and spend most of the time scanning copying maping
those areas with usual methis, even optimized C - code taken advantage of advanced cpu opcodes are possible here. ANd by dividing it in chunks we get a lot of new features with essentiually no cose with more than complexity which we manage mostly in scheme. We gain many things,

1. Many new features that can vary on the pages

2. less memory hogs as
   a. it can use copy ion write semantics
   b. it does not need to fins 1GB continuous blocks

3. potentially faster operations as we can fast foorward the zeroe on write
   pages compared to pure bytevectors

4. we can allow for swaping and refferential datastructures to speed up copying
   even further

5. can get better fiber features of C programs that spends seconds or minutes on
   performing an operation because it will just spend a microsecond or such
   in C-land and then swap back to Scheme. CTRL-C will also work nicely.

6. We could potentially build a string library untop of these datastructures and
   also we could add features to pages that lead to a much richer interface. 
 
7. resizing is much faster end efficient

8. reversing is super quick

9. queues and stacks of byte data can have a very efficient implementations 

Drawback:
1. More complex as we need to consider boudaries
2. Slower one off operations like bytevector-u8-get as guile compiles the 
   core operatoins to quite effective JIT CPU encodings. But maybe we can
   disign some caching to make those operations much faster and even have
   suporting JIT operations.

|#



	      
 
  












