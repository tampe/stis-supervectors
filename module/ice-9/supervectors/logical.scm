;;HACK
(define-syntax-rule (define-module- name args ...)
  (cond-expand
   (guile-3
    (define-module name
      #:declarative? #f
      args ...))
   (guile
    (define-module name args ...))))


(define-module- (ice-9 supervectors logical)
  #:use-module (ice-9 control)
  #:use-module (ice-9 supervectors functional)
  #:use-module (ice-9 supervectors core)
  #:use-module (ice-9 supervectors copy)
  #:use-module (ice-9 supervectors macros)
  #:export (supervector-or-map
	    supervector-and-map
	    
	    supervector-not!
	    supervector-not

	    supervecor-and!
	    supervecor-or!
	    supervecor-xor!

	    supervecor-and
	    supervecor-or
	    supervecor-xor))

(define (dist x y) (> x y) (- x y) (- y x))

(define (len s1 s2)
  (if (number? s1)
      (if (number? s2)
	  1
	  (supervector-length s2))
      (if (number? s2)
	  (supervector-length s1)
	  (min (supervector-length s1) (supervector-length s2)))))

(define* (supervector-or-map f s #:optional (i 0) (n #f))
  (set! n (if n n (supervector-length s)))
  
  (let/ec ret	      
    (supervector-chunk-fold
	   
     ;; for the chunk
     (lambda (b i j d seed)
       (define N (+ 1 (dist i j)))
       (define x (d-ref  b))
       (if (f x)
	   (ret x)
	   #f))
       
     ;; for the vector
     (lambda (b i j d seed)
       (let ((v (v-ref b)))
	 (standard-loop-1
	      #f (bits-ref b) i j d #f (get-reffer)
	   (lambda (ref k)
	     (let ((xx (ref v k)))
	       (if (f xx)
		   (ret xx)
		   #f))))
	 #f))
     
     ;; For the bug
     (lambda x (error "BUG"))

     ;; seed
     #f
     
     s i n)))

(define* (supervector-and-map f s #:optional (i 0) (n #f))
  (set! n (if n n (supervector-length s)))
  
  (let/ec ret	      
    (supervector-chunk-fold
	   
     ;; for the chunk
     (lambda (b i j d seed)
       (define N (+ 1 (dist i j)))
       (define x (d-ref  b))
       (if (f x) x (ret #f)))
       
     ;; for the vector
     (lambda (b i j d seed)
       (let ((v (v-ref b))
	     (x #f))
	 (standard-loop-1
	     #f (bits-ref b) i j d #f (get-reffer)
	   (lambda (ref k)
	     (let ((xx (ref v k)))
	       (if (f xx)
		   (set! x xx)
		   (ret #f)))))
	 x))
     
     ;; For the bug
     (lambda x (error "BUG"))

     ;; seed
     #t

     s i n)))

(define* (supervector-not! s #:optional (i 0) (n #f))
  (set! n (if n n (- (supervector-length s) i)))
  
  (supervector-unary
   (lambda (x) (not x))
   s i n
   #:ctrl
   (lambda x (values negate #f))))

(define* (supervector-not s #:optional (i 0) (n #f))
  (set! n (if n n (- (supervector-length s) i)))
  (let ((ss (supervector-copy s)))
    (supervector-unary
     (lambda (x) (not x))
     ss i n
     #:ctrl
     (lambda x (values negate #f)))))

(define* (supervector-and! s1 s2 #:optional (i1 0) (i2 0) (n #f))
  (set! n (if n n (len s1 s2)))
  
  (supervector-binary
   (lambda (x y) (and x y))
   (lambda (x)
     (if x
	 (values nothing 0)
	 (values  replace #f)))
   
   (lambda (x)
     (if x
	 (values nothing 0)
	 (values replace #f)))

   (lambda (x y) (and x y))
   s1 i1 s2 i2 n))

(define* (supervector-or! s1 s2 #:optional (i1 0) (i2 0) (n #f))
  (set! n (if n n (len s1 s2)))
  
  (supervector-binary
   (lambda (x y) (or x y))

   (lambda (x)
     (if x
	 (values replace x)
	 (values nothing 0)))
   
   (lambda (x)
     (if x
	 (values replace x)
	 (values nothing 0)))

   (lambda (x y) (or x y))
   s1 i1 s2 i2 n))

(define (xor x y) (if x (if y #f x) (if y y #f)))


(define* (supervector-xor! s1 s2 #:optional (i1 0) (i2 0) (n #f))
  (set! n (if n n (len s1 s2)))
  
  (supervector-binary

   xor

   (lambda (x)
     (if x
	 (values negate  0)
	 (values nothing 0)))
   
   (lambda (x)
     (if x
	 (values negate  0)
	 (values nothing 0)))

   xor
   
   s1 i1 s2 i2 n))

(define-syntax-rule (mk supervector-and supervector-and!!)
  (define* (supervector-add s #:optional (i 0) (n #f))
    (set! n (if n n (- (supervector-length s) i)))
    (let ((ss (supervector-copy s i n)))
      (supervector-and! s ss i i n)
      s)))

(mk supervector-and supervector-and!)
(mk supervector-or  supervector-or! )
(mk supervector-xor supervector-xor!)
