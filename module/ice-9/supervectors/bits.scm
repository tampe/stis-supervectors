;;HACK
(define-syntax-rule (define-module- name args ...)
  (cond-expand
   (guile-3
    (define-module name
      #:declarative? #f
      args ...))
   (guile
    (define-module name args ...))))


(define-module- (ice-9 supervectors bits)
  #:use-module (ice-9 supervectors functional)
  #:use-module (ice-9 supervectors printer)
  #:use-module (ice-9 supervectors make)
  #:use-module (ice-9 supervectors typed)
  #:use-module (ice-9 supervectors macros)
  #:use-module (ice-9 supervectors basic)
  #:use-module (ice-9 supervectors core)
  #:use-module (ice-9 supervectors copy)
  #:use-module (ice-9 supervectors typeinfo)
  #:use-module (ice-9 supervectors resize)
  #:use-module (ice-9 supervectors c)
  #:use-module (rnrs bytevectors)
  #:export (make-superbits
	    superbits
	    superbits-length
	    superbits-resize!
	    superbits-copy!
	    superbits-copy
	    
	    superbits-ref
	    superbits-set!
	    superbits-set-bit!
	    superbits-clear-bit!
	    superbits-flip-bit!
	 	    
	    superbits-clear-bits!
	    superbits-set-bits!
	    superbits-flip-bits!
	    superbits-lognot!
	    superbits-logand!
	    superbits-logior!
	    superbits-logxor!
	    
	    superbits-clear-bits
	    superbits-set-bits
	    superbits-flip-bits
	    superbits-lognot
	    superbits-logand
	    superbits-logior
	    superbits-logxor

	    ;; Set operations
	    superbits-union!
	    superbits-intersection!
	    superbits-complement! 
	    superbits-set-minus!
	    superbits-set-plus!
	    
	    superbits-union
	    superbits-intersection
	    superbits-complement
	    superbits-set-minus
	    superbits-set-plus

	    superbits-fill!
	    superbits-clear-all-bits!
	    superbits-set-all-bits!
	    superbits-first-1
	    superbits-first-0

	    superbits-chunk-find-1
	    superbits-chunk-find-0

	    superbits-1-indices
	    superbits-0-indices

	    superbits-count-zeros
	    superbits-count-bits
	    ))

(define (dist i j) (if (> i j) (- i j) (- j i)))

(define* (superbits-copy! s1 i1 s2 i2 n #:key (swap? #f) (ref? #f))
  (let* ((ii1 (quotient i1 64))
	 (kk1 (modulo   i1 64))

	 (ii2 (quotient i2 64))
	 (kk2 (modulo   i2 64))

	 (n1  (+ n i1))
	 (n2  (+ n i2))	 
	 
	 (j1  (quotient (- n1 1) 64))
	 (l1  (modulo   (- n1 1) 64))
	 
	 (j2  (quotient (- n2 1) 64))
	 (l2  (modulo   (- n2 1) 64))

	 (N1  (+ (- j1 ii1) 1))
	 (N2  (+ (- j2 ii2) 1))
	 (N   (max N1 N2)))

    (when (< (superbits-length s1) n1)
      (superbits-resize! s1 n1))

    (when (< (superbits-length s2) n2)
      (superbits-resize! s2 n2))
    
    (let* ((x   (supervector-ref s2 ii2))
	   (mx  (- (ash 1 kk2) 1))
	   (mxc (- (ash 1 64) (ash 1 kk2)))

	   (y   (supervector-ref s2 j2))
	   (my  (- (ash 1 (+ l2 1)) 1))
	   (myc (- (ash 1 64) (ash 1 (+ l2 1)))))

      (when (and (= (superbits-length s1) n1) (= (superbits-length s2) n2))
	(set!    y  (sd-ref s1))
	(sd-set! s2 (sd-ref s1)))
      
      (cond
       ;; Fast path
       ((and (= kk1 0) (= kk2 0))
	(supervector-copy! s1 ii1 s2 ii2 N
			   #:swap? swap?
			   #:ref?  ref?
			   #:cfkn1 loop-copy1
			   #:cfkn  loop-copy))
      
       ((>= kk1 kk2)
	#|           mask3
	mask1 mask2  kk1 ...

	       kk2 ...
	mask3b mask1b mask2b	              
      |#
      
	(let* ((k2     kk2)
	       (k1     (- kk1 kk2))
	       (k3     (- 64 k1 k2))
	       (mask1  (- (ash 1 k1) 1))
	       (mask2  (- (ash 1 (+ k1 k2)) (ash 1 k1)))
	       (mask3  (- (ash 1 64) (ash 1 (+ k1 k2))))
	       (q1     (+ k2 k3))
	       (q2     (- k1))
	       (q3     (- k1))
	       (mask1b (ash mask1 q1))
	       (mask2b (ash mask2 q2))
	       (mask3b (ash mask3 q3)))
       
	  (supervector-copy! s1 (+ ii1 1) s2 ii2 (max 0 (- N 1))
	    #:k1    kk1
	    #:k2    kk2			   
	    #:swap? swap?
	    #:ref?  ref?
	    #:cfkn2 (lambda (x1 x2)
		      (logior (ash (logand mask1 x1) q1)
			      (logand x2 (lognot mask1b))))
	    #:cfkn1 (loop-masked-copy1 mask1 q1 mask1b)
	    #:cfkn  (loop-masked-copy  mask1 q1 mask1b))
	
	  (supervector-copy! s1 (+ ii1 1) s2 (+ ii2 1) (max 0 (- N 1))
	    #:k1    kk1
	    #:k2    kk2			   			   
	    #:swap? swap?
	    #:ref?  ref?
	    #:cfkn2 (lambda (x1 x2)
		      (logior (ash (logand mask2 x1) q2)
			      (logand x2 (lognot mask2b))))
	    #:cfkn1 (loop-masked-copy1 mask2 q2 mask2b)
	    #:cfkn  (loop-masked-copy  mask2 q2 mask2b))
	
	  (supervector-copy! s1 ii1 s2 ii2 (max 0 N)
	    #:k1    kk1
	    #:k2    kk2			   			   
	    #:swap? swap?
	    #:ref?  ref?
	    #:cfkn2 (lambda (x1 x2)
		      (logior (ash (logand mask1 x1) q3)
			      (logand x2 (lognot mask1b))))
	    #:cfkn1 (loop-masked-copy1 mask3 q3 mask3b)
	    #:cfkn  (loop-masked-copy  mask3 q3 mask3b))))

       (else
	#|          
	mask2  kk1 ...
               mask3   mask1

        mask1b mask2b  kk2 ...
                       mask3b
	|#

	(let* ((k3     (- 64 kk2))
	       (k2     kk1)
	       (k1     (- 64 k3 k2))
	       
	       (mask1  (- (ash 1 64) (ash 1 (+ k2 k3))))
	       (mask2  (- (ash 1 k2) 1))
	       (mask3  (- (ash 1 (+ k2 k3))  (ash 1 k2)))
	     
	       (mask1b (ash mask1 (- (+ k2 k3))))
	       (mask2b (ash mask2 k1))
	       (mask3b (ash mask3 k1)))

	  (when (> N 1)
	    (supervector-copy! s1 ii1 s2 (+ ii2 1) (max 0 (- N 1))
	       #:k1    kk1
	       #:k2    kk2
	       #:swap? swap?
	       #:ref?  ref?
	       #:cfkn2 (lambda (x1 x2)
			 (logior (ash (logand mask1 x1) (- (+ k2 k3)))
				 (logand x2 (lognot mask1b))))
	       #:cfkn1 (loop-masked-copy1 mask1 (- (+ k2 k3)) mask1b)
	       #:cfkn  (loop-masked-copy  mask1 (- (+ k2 k3)) mask1b)))
	
	  (when (> N 1)
	    (supervector-copy! s1 (+ ii1 1) s2 (+ ii2 1) (max 0 (- N 1))
	       #:k1    kk1
	       #:k2    kk2			   
	       #:swap? swap?
	       #:ref?  ref?
	       #:cfkn2 (lambda (x1 x2)
			 (logior (ash (logand mask2 x1) k1)
				 (logand x2 (lognot mask2b))))
	       #:cfkn1 (loop-masked-copy1 mask2 k1 mask2b)
	       #:cfkn  (loop-masked-copy  mask2 k1 mask2b)))

	  (when (> N 0)
	    (supervector-copy! s1 ii1 s2 ii2 (max 0 N)
	       #:k1    kk1
	       #:k2    kk2			   
	       #:swap? swap?
	       #:ref?  ref?
	       #:cfkn2 (lambda (x1 x2)
			 (logior (ash (logand mask3 x1) k1)
				 (logand x2 (lognot mask3b))))
	       #:cfkn1 (loop-masked-copy1 mask3 k1 mask3b)
	       #:cfkn  (loop-masked-copy  mask3 k1 mask3b))))))

      (supervector-set! s2 ii2 (logior (logand x mx)
				       (logand (supervector-ref s2 ii2)
					       mxc)))

      (supervector-set! s2 j2 (logior (logand y myc)
				      (logand (supervector-ref s2 j2)
					      my))))))

(define* (superbits-copy s
			 #:optional (i 0) (n #f)
			 #:key      (ref? #f) (ro? #t) (binsize #f))
  (when (not binsize)
    (set! binsize (sbz-ref s)))
  
  (when (not n)
    (set! n (- (superbits-length s) i)))
  
  (let ((ss (make-superbits n #:ro? #f #:binsize binsize)))
    (superbits-copy! s i ss 0 n #:ref? ref?)
    (when ro?
      (supervector-make-ro! ss)) 
    ss))
			

(define* (make-superbits n #:optional (x 0)
			 #:key
			 (val     #f)
			 (ref?    #f)
			 (ro?     #f)
			 (binsize #f))
  (cond
   (x
    (set! x (- (ash 1 64) 1)))
   
   ((not x)
    (set! x 0))
   
   (else
    (error "default superbits is only valid for 0/1/#t/#f")))

  (when val
    (set! x val))
   
  (cond
   ((supervector? n)
    (make-supervector n
		      #:bits    (sbits-ref n)
		      #:ref?    ref?
		      #:ro?     ro?
		      #:binsize binsize))

   (else
    (let* ((n2 (if (number? n) n
		   (* 8 (bytevector-length n))))
	   (nn (+ 1 (quotient (- n2 1) 64)))
	   (k  (modulo (- n2 1) 64))
	   (s  (make-supervector (if (number? n) nn n)
				 x
				 #:m            8
				 #:ro?          ro?
				 #:resizable?   1
				 #:level        8
				 #:bitcorrect   (+ k 1)
				 #:superbit?    1
				 #:ref?         ref?
				 #:binsize      (if binsize
						    binsize
						    (ash 1 (+ 4 8))))))
      s))))

(define (superbits-resize! s n)
  (let* ((m (superbits-length s))	
	 (i1 (quotient (- m 1) 64))
	 (i2 (quotient (- n 1) 64))
	 (k  (modulo   n 64)))
  
    (if (> n m)
	(let ((d (- i2 i1)))
	  (when (> d 0)
	    (supervector-expand-right! s d))
	  (sbits-set! s (set-e (sbits-ref s) k)))
	
	(let ((d (- i1 i2)))
	  (when (> d 0)
	    (supervector-shrink-right! s d))
	  (sbits-set! s (set-e (sbits-ref s) k)))))
  (values))
	
	
(define (superbits . l)
  (let lp ((n 0) (l l) (r '()))
    (if (pair? l)
	(let lp2 ((n n) (i 0) (s 0) (l l))
	  (if (and (pair? l) (< i 64))
	      (lp2 (+ n 1)
		   (+ i 1)
		   (if (car l)
		       (logior (ash 1 (id i)) s)
		       s)
		   (cdr l))
	      (lp n l (cons s r))))
	
	(let* ((n (- n 1))
	       (k (modulo n 64))
	       (n (+ 1 (quotient n 64)))
	       (v (make-bytevector (* 8 n))))

	  (let lp ((i 0) (l (reverse! r)))
	    (when (< i n)
	      (bytevector-u64-set! v (* 8 i) (car l) (native-endianness))
	      (lp (+ i 1) (cdr l))))
	  
	  (make-supervector v
			    #:m            8
			    #:resizable?   1
			    #:level        8
			    #:bitcorrect   (+ k 1)
			    #:superbit?    1
			    #:ro?          1
			    #:binsize (ash 1 (+ 4 8)))))))

(define (superbits-length s)
  (+ (* 64 (- (supervector-length s) 1))
     (+ 1 (get-e (sbits-ref s)))))

(define (superbits-ref s n)
  (let ((i (modulo   n 64))
	(N (quotient n 64)))
    (> (logand (ash 1 i) (supervector-ref s N)) 0)))

(define (superbits-set! s n val)
  (if val
      (superbits-set-bit! s n)
      (superbits-clear-bit! s n)))


(define (superbits-set-bit! s n)
  (let ((i (modulo   n 64))
	(N (quotient n 64)))
    (supervector-set! s N (logior (ash 1 i) (supervector-ref s N)))))

(define (superbits-clear-bit! s n)
  (let ((i (modulo   n 64))
	(N (quotient n 64)))
    (supervector-set! s N (logand (lognot (ash 1 i)) (supervector-ref s N)))))

(define (superbits-flip-bit! s n)
  (let ((i (modulo   n 64))
	(N (quotient n 64)))
    (supervector-set! s N (logxor (ash 1 i) (supervector-ref s N)))))

(define* (superbits-lognot! s #:optional (i 0) (n #f))
  (when (not n)
    (set! n (- (supervector-length s) i)))

  (sd-set! s (logand (- (ash 1 64) 1) (lognot (sd-ref s))))
  
  (supervector-unary
   (lambda (x) (logand (- (ash 1 64) 1) (lognot x)))
   s i n
   #:ctrl
   (lambda x (values invert #f))))

(define-syntax-rule (mk-superbit-binary superbits-logand! OP F1 F2
					loop-masked-logand
					loop-masked-logand1
					loop-logand
					loop-logand1)
  
  (define* (superbits-logand! s1 s2 #:optional (i1 0) (i2 0) (n #f)
			      #:key (divide? #t) (ref? #f))

    (when (and (not (supervector? s1)) s1)
      (set! s1 (make-superbits n 1 #:ro? 1)))

    (when (eq? s1 #f)
      (set! s1 (make-superbits n 0 #:ro? 1)))

    (set! n (if n n (max (- (superbits-length s1) i1)
			 (- (superbits-length s2) i2))))

    (let* ((ii1 (quotient i1 64))
	   (kk1 (modulo   i1 64))

	   (ii2 (quotient i2 64))
	   (kk2 (modulo   i2 64))

	   (n1  (+ n i1))
	   (n2  (+ n i2))	 
	 
	   (j1  (quotient (- n1 1) 64))
	   (l1  (modulo   (- n1 1) 64))
	 
	   (j2  (quotient (- n2 1) 64))
	   (l2  (modulo   (- n2 1) 64))

	   (N1  (+ (- j1 ii1) 1))
	   (N2  (+ (- j2 ii2) 1))
	   (N   (max N1 N2)))

      (when (< (superbits-length s1) n1)
	(superbits-resize! s1 n1))

      (when (< (superbits-length s2) n2)
	(superbits-resize! s2 n2))
    
      (let* ((x   (supervector-ref s2 ii2))
	     (mx  (- (ash 1 kk2) 1))
	     (mxc (- (ash 1 64) (ash 1 kk2)))

	     (y   (supervector-ref s2 j2))
	     (my  (- (ash 1 (+ l2 1)) 1))
	     (myc (- (ash 1 64) (ash 1 (+ l2 1)))))

	(when (and (= (superbits-length s1) n1) (= (superbits-length s2) n2))
	  (set!    y  (OP (sd-ref s1) (sd-ref s2)))
	  (sd-set! s2 (OP (sd-ref s1) (sd-ref s2))))


	(cond
	 ((and (= kk1 0) (= kk2 0))
	  (superbits-binary
	   OP F1 F2 OP
	   s1 ii1 s2 ii2 N
	   #:divide? divide?
	   #:ref? ref?
	   #:cfkn1 loop-logand1
	   #:cfkn  loop-logand))
	  
	 ((>= kk1 kk2)
	  (let* ((k2     kk2)
	     (k1     (- kk1 kk2))
	     (k3     (- 64 k1 k2))
	     (mask1  (- (ash 1 k1) 1))
	     (mask2  (- (ash 1 (+ k1 k2)) (ash 1 k1)))
	     (mask3  (- (ash 1 64) (ash 1 (+ k1 k2))))
	     (q1     (+ k2 k3))
	     (q2     (- k1))
	     (q3     (- k1))
	     (mask1b (ash mask1 q1))
	     (mask2b (ash mask2 q2))
	     (mask3b (ash mask3 q3)))
	  
	    (superbits-binary
	     OP F1 F2 OP
	     s1 (+ ii1 1) s2 ii2 (max 0 (- N 1))
	     #:k1      kk1
	     #:k2      kk2
	     #:divide? divide?
	     #:ref?    ref?
	     #:cfkn2   (lambda (x1 x2)
			 (logior (OP (ash (logand mask1 x1) q1)
				     (logand x2 mask1b))
				 (logand x2 (lognot mask1b))))
	     #:cfkn1   (loop-masked-logand1 mask1 q1 mask1b)

	     #:cfkn    (loop-masked-logand mask1 q1 mask1b))

	    (superbits-binary
	     OP F1 F2 OP
	     s1 (+ ii1 1) s2 (+ ii2 1) (max 0 (- N 1))
	     #:k1      kk1
	     #:k2      kk2
	     #:divide? divide?
	     #:ref?    ref?
	     #:cfkn2 (lambda (x1 x2)
		       (logior (OP (ash (logand mask2 x1) q2)
				   (logand x2 mask2b))
			       (logand x2 (lognot mask2b))))
	     #:cfkn1 (loop-masked-logand1 mask2 q2 mask2b)

	     #:cfkn    (loop-masked-logand mask2 q2 mask2b))

	    (superbits-binary
	     OP F1 F2 OP
	     s1 ii1 s2 ii2 (max 0 N)
	     #:k1      kk1
	     #:k2      kk2
	     #:divide? divide?
	     #:ref?    ref?
	     #:cfkn2   (lambda (x1 x2)
			 (logior (OP (ash (logand mask3 x1) q3)
				     (logand x2 mask3b))
				 (logand x2 (lognot mask3b))))
	     #:cfkn1   (loop-masked-logand1 mask1 q3 mask3b)
	     
	     #:cfkn    (loop-masked-logand mask3 q3 mask3b))))
	 
	 
	 (else
	  (let* ((k3     (- 64 kk2))
		 (k2     kk1)
		 (k1     (- 64 k3 k2))

		 (mask1  (- (ash 1 64) (ash 1 (+ k2 k3))))
		 (mask2  (- (ash 1 k2) 1))
		 (mask3  (- (ash 1 (+ k2 k3))  (ash 1 k2)))
	     
		 (mask1b (ash mask1 (- (+ k2 k3))))
		 (mask2b (ash mask2 k1))
		 (mask3b (ash mask3 k1)))

       
	    (superbits-binary
	     OP F1 F2 OP
	     s1 ii1       s2 (+ ii2 1) (max 0 (- N 1))
	     #:k1      kk1
	     #:k2      kk2
	     #:divide? divide?
	     #:ref?    ref?
	     #:cfkn2   (lambda (x1 x2)
			 (logior (OP (ash (logand mask1 x1) (- (+ k2 k3)))
				     (logand x2 mask1b))
				 (logand x2 (lognot mask1b))))
	     #:cfkn1   (loop-masked-logand1 mask1 (- (+ k2 k3)) mask1b)

	     #:cfkn    (loop-masked-logand mask1 (- (+ k2 k3)) mask1b))

	    (supervector-binary
	     OP F1 F2 OP
	     s1 (+ ii1 1) s2 (+ ii2 1) (max 0 (- N 1))
	     #:k1      kk1
	     #:k2      kk2
	     #:divide? divide?
	     #:ref?    ref?
	     #:cfkn2   (lambda (x1 x2)
			 (logior (OP (ash (logand mask2 x1) k1)
				     (logand x2 mask2b))
				 (logand x2 (lognot mask2b))))

	     #:cfkn1   (loop-masked-logand1 mask1 k1 mask1b)

	     #:cfkn    (loop-masked-logand mask2 k1 mask2b))	  
	    
	    (supervector-binary
	     OP F1 F2 OP
	     s1 ii1       s2 ii2            (max 0 N)
	     #:k1      kk1
	     #:k2      kk2
	     #:divide? divide?
	     #:ref?    ref?
	     #:cfkn2 (lambda (x1 x2)
		       (logior (OP (ash (logand mask3 x1) k1)
				   (logand x2 mask3b))
			       (logand x2 (lognot mask3b))))
	    
	     #:cfkn1 (loop-masked-logand1 mask1 k1 mask3b)

	     #:cfkn
	     (loop-masked-logand mask3 k1 mask3b)))))

	;; Make sure to fix the endpoint
	(supervector-set! s2 ii2 (logior (logand x mx)
					 (logand (supervector-ref s2 ii2)
						 mxc)))

	(supervector-set! s2 j2 (logior (logand y myc)
					(logand (supervector-ref s2	j2)
						my)))))))


(mk-superbit-binary superbits-logand! logand
  (lambda (x)
    (if (= x 0)
	(values replace 0)
	(values nothing 0)))
  
  (lambda (x)
    (if (= x 0)
	(values replace 0)
	(values nothing 0)))
  
  loop-masked-logand
  loop-masked-logand1
  loop-logand
  loop-logand1)

(define-syntax-rule (mk superbits-logand  superbits-logand!) 
  (define* (superbits-logand s1 s2 #:optional (i1 0) (i2 0) (n #f)
			     #:key
			     (binsize #f)
			     (divide? #t)
			     (ref?    #f)
			     (ro?     #t))

    (when (and ro? ref?)
      (error "can't reference and be ro? at the same time in binary op"))

    (set! n (if n n (max (- (superbits-length s1) i1)
			 (- (superbits-length s2) i2))))

    (when (< (supervector-length s1) (+ i1 n))
      (superbits-resize! s1 (+ i1 n)))
    
    (let ((s3 (superbits-copy s2 i2 #:ref? ref? #:binsize binsize #:ro? #f)))
      (superbits-logand! s1 s3 i1 0 n #:divide? divide? #:ref? ref?)
      (when ro?
	(supervector-make-ro! s3))
      s3)))

(mk superbits-logand  superbits-logand!)

(mk-superbit-binary superbits-logior! logior
  (lambda (x)
    (if (= x 0)
	(values nothing 0)
	(values replace x)))
   
  (lambda (x)
    (if (= x 0)
	(values nothing 0)
	(values replace x)))
  loop-masked-logior
  loop-masked-logior1
  loop-logior
  loop-logior1)

(mk superbits-logior  superbits-logior!)


(mk-superbit-binary superbits-logxor! logxor
     (lambda (x)
       (if (= x 0)
	   (values nothing 0)
	   (values invert  0)))

     (lambda (x)
       (if (= x 0)
	   (values nothing 0)
	   (values invert  0)))
     loop-masked-logxor
     loop-masked-logxor1
     loop-logxor
     loop-logxor1)


(mk superbits-logxor  superbits-logxor!)


(mk-superbit-binary superbits-clear-bits! (lambda (x y) (logand y (lognot x))) 
     (lambda (x)
       (if (= x 0)
	   (values nothing 0)
	   (values replace 0)))
   
     (lambda (y)
       (if (= y 0)
	   (values nothing 0)
	   (values replace 0)))

     loop-masked-logclear
     loop-masked-logclear1
     loop-logclear
     loop-logclear1)


(mk superbits-clear-bits  superbits-clear-bits!)


(define* (superbit-lognot! s #:optional (i 0) (n #f))
  (set! n (if n n (- (superbits-length s) i)))

  (let* ((k1 (modulo i 64))
	 (ii (quotient i 64))
	 (N  (+ i n))
	 (k2 (modulo N 64))
	 (nn (+ 1 (quotient (- N 1) 64))))
    
    (let ((x (if (> k1 0) (supervector-ref s ii      ) #f))
	  (y (if (> k2 0) (supervector-ref s (- nn 1)) #f)))
      
      (superbits-lognot! s ii nn)
      
      (when (> k1 0)
	(let ((mask (- (ash k1 1) 1)))
	  (supervector-set!
	   s ii (logior (logand mask x)
			(logand (lognot mask) (supervector-ref s ii))))))
      
      (when (> k2 0)
	(let ((mask (- (ash k2 1) 1)))
	  (supervector-set!
	   s ii (logior (logand (lognot mask) y)
			(logand mask(supervector-ref s ii)))))))))

    
(define* (superbits-lognot s #:optional (i 0) (n #f))
  (let ((ss (superbits-copy s i n)))
    (superbits-lognot! ss 0 n)
    ss))

(define superbits-set-bits!  superbits-logior!)
(define superbits-set-bits   superbits-logior )
(define superbits-flip-bits! superbits-logxor!)
(define superbits-flip-bits  superbits-logxor )

(define superbits-union!         superbits-logior!)
(define superbits-intersection!  superbits-logand!)
(define superbits-complement!    superbits-lognot!)
(define superbits-set-minus!     superbits-clear-bits!)
(define superbits-set-plus!      superbits-logxor!)

(define superbits-union         superbits-logior)
(define superbits-intersection  superbits-logand)
(define superbits-complement    superbits-lognot)
(define superbits-set-minus     superbits-clear-bits)
(define superbits-set-plus      superbits-logxor)

(define* (superbits-fill! s p #:optional (i 0) (n #f))
  (if p
      (superbits-logior! #t s i n)
      (superbits-logand! #f s i n)))


(define* (supervector-clear-all-bits! s p #:optional (i 0) (n #f))
  (superbits-logand! #f s i n))

(define* (supervector-set-all-bits! s p #:optional (i 0) (n #f))
  (superbits-logior! #t s i n))

(define iid (lambda (x) x))

(define-syntax-rule (mk-chunk-find superbits-chunk-find-1 val val* logandf lognotf lognotff)
  (define* (superbits-chunk-find-1 f f2 seed s #:optional (i 0) (n #f))
    (set! n (if n n (- (superbits-length s) i)))

    (let* ((N    (+ i n))
	   (ii   (quotient i 64))
	   (iii  ii)
	   (k1   (modulo   i 64))
	   (nn   (+ i n))
	   (k2   (modulo nn 64))
	   (nn   (+ 1 (quotient (- N 1) 64)))
	   (nn   (- nn ii))
	   (nnn  nn))

      (when (> k1 0)
	(let ((mask (- (ash 1 k1) 1)))
	  (set! seed (f (logandf (lognotf mask)
				 (supervector-ref s ii)) seed)))
	(set! nnn (- nnn 1))
	(set! iii (+ iii 1)))

      (set! nn nnn)
      
      (when (> k2 0)
	(set! nnn (- nnn 1)))

      (when (> nnn 0)
	(set!
	 seed
	 (supervector-chunk-fold
	  ;; for the chunk
	  (lambda (b i j d seed)
	    (define N (+ 1 (dist i j)))
	    (define x (d-ref  b))
	    (cond
	     ((= x val)
	      (+ seed (* 64 N)))
	     ((= x val*)
	      seed)
	     (else
	      (f2 x N seed))))
       
	  ;; for the vector
	  (lambda (b i j d seed)
	    (let ((v (v-ref b)))
	      (standard-loop-1
	          #f (bits-ref b) i j d #f (get-reffer)
	        (lambda (ref k)
		  (set! seed (f (ref v k) seed)))))
	    seed)
	  
	  ;; For the bug
	  (lambda x (error "BUG"))
       
	  seed s iii nnn)))

      (when (> k2 0)
	(let ((mask (- (ash 1 k2) 1)))
	  (set!
	   seed (f (logandf (lognotff mask)
			    (supervector-ref s (- (+ ii nn) 1))) seed))))

      seed)))

     
(define ones (- (ash 1 64) 1))

(mk-chunk-find superbits-chunk-find-1 ones 0    logand lognot iid)
(mk-chunk-find superbits-chunk-find-0 0    ones logior iid    lognot)

(define (bitcount x)
  (let lp ((x x) (n 64))
    (if (= n 2)
	(case x
	  ((0)   0)
	  ((1 2) 1)
	  ((3)   2))
	      
	(let* ((n2 (quotient n 2))
	       (e  (ash 1 n2))		   
	       (x1 (logand x (- e 1)))
	       (x2 (ash (logand x (lognot x1)) (- n2))))
	  (if (= x1 0)
	      (lp x2 n2)
	      (let ((n1 (lp x1 n2)))
		(if (= x2 0)
		    n1
		    (+ n1 (lp x2 n2)))))))))

(define* (superbits-count-bits s1 #:optional (i1 0) (n #f)
			       #:key (s2 #f) (i2 i1))

  (if (not s2)
      (superbits-chunk-find-1
       (lambda (x seed)
	 (+ seed (bitcount x)))
       
       (lambda (x n seed)
	 (+ (* n (bitcount x)) seed))
       
       0 s1 i1 n)
      
      (cond
       ((not (or (supervector? s1) (supervector? s2)))
	(if (and (= i1 0) (= i2 0) (> n 0) (and s1 s2)) 1 0))

       ((not (supervector? s2))
	(superbits-count-bits s2 i2 n #:s2 s1 #:i2 i1))

       (else
	(let ((ss (make-superbits s2)))
	  (superbits-logand! s1 ss i1 i2 n)
	  (let ((N 0))
	    (superbits-chunk-find-1
	     (lambda (x seed)
	       (+ seed (bitcount x)))
	 
	     (lambda (x n seed)
	       (+ (* n (bitcount x)) seed))
	     
	     i2 ss i2 n)))))))

(define* (superbits-count-zeros s1 #:optional (i1 0) (n #f)
				#:key (i2 0) (s2 #f))
  (set! n (if n n (- (superbits-length s1) i1)))
  
  (if (not s2)
      (- n (superbits-count-bits s1 i1 n))
      (let ((n (superbits-count-bits s2 i2 n)))  
	(- n (superbits-count-bits s1 i1 n #:s2 s2 #:i2 i2)))))

(define-syntax-rule (mk-indices superbits-1-indices
				superbits-count-bits
				superbits-chunk-find-1
				ones zeros ee)
  (define* (superbits-1-indices s #:optional (i 0) (n #f) #:key (s2 #f) (i2 0))
    (let* ((N (superbits-count-bits s i n #:s2 s2 #:i2 i2))
	   (I 0)
	   (b (make-vector N)))

      (define (find-64 x seed)
	(let lp ((x x) (n 64) (seed seed))
	  (if (= n 1)
	      (begin
		(when (= x ee)
		  (vector-set! b I seed)
		  (set! I (+ I 1)))
		(+ seed 1))
	      
	      (let* ((n2 (quotient n 2))
		     (e  (ash 1 n2))		   
		     (x1 (logand x (- e 1)))
		     (x2 (ash (logand x (lognot x1)) (- n2))))
		(if (= x1 (logand zeros (- e 1)))
		    (lp x2 n2 (+ seed n2))
		    (let ((seed (lp x1 n2 seed)))
		      (if (= x2 (logand zeros (- e 1)))
			  (+ seed n2)
			  (lp x2 n2 seed))))))))
      
      (define (find-empty x n seed)
	(let lp ((i 0) (seed seed))
	  (if (< i n)
	      (lp (+ i 1) (find-64 x seed))
	      seed)))
	
      (superbits-chunk-find-1 find-64 find-empty 0 s i n)
      
      b)))

(mk-indices superbits-1-indices
	    superbits-count-bits
	    superbits-chunk-find-1
	    ones 0 1)

(mk-indices superbits-0-indices
	    superbits-count-zeros
	    superbits-chunk-find-0
	    0 ones 0)

