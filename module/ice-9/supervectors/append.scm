;;HACK
(define-syntax-rule (define-module- name args ...)
  (cond-expand
   (guile-3
    (define-module name
      #:declarative? #f
      args ...))
   (guile
    (define-module name args ...))))


(define-module- (ice-9 supervectors append)
  #:use-module (ice-9 supervectors core)
  #:use-module (ice-9 supervectors make)
  #:use-module (ice-9 supervectors copy)
  #:use-module (ice-9 supervectors typed)
  #:use-module (ice-9 supervectors functional)
  #:use-module (ice-9 supervectors resize)
  
  #:export (supervector-append
	    supervector-append!
	    supervector-prepend
	    supervector-prepend!
	    supervector-pop-left!
	    supervector-pop-right!
	    supervector-reverse
	    supervector-reverse!
	    supervector-remove!
	    supervector-insert!))


(define (fold f seed x)
  (if (pair? x)
      (fold f (f (car x) seed) (cdr x))
      seed))

(define* (supervector-append s l #:key
			     (bits     #f)
			     (binsize  *binsize*)
			     (ro?      #f)
			     (ref?     #f))  
  (let* ((l    (cons s l))
	 (ln   (map supervector-length l))
	 (n    (fold + 0 ln))
	 (bits (if bits bits (sbits-ref (car l))))
	 (s    (make-supervector n
				 #:bits    (clear-bits bits)
				 #:binsize binsize)))
    
    (let lp ((i 0) (l l) (ln ln))
      (when (pair? l)
	(let ((ss (car l))
	      (n  (car ln)))
	  (supervector-copy! ss 0 s i n #:ref? ref?)	  
	  (lp (+ i n) (cdr l) (cdr ln)))))
    
    s))

(define* (supervector-append! s l #:key (ref? #f))
  (let* ((ln   (map supervector-length l))
	 (nn   (fold + 0 ln))
	 (n    (supervector-length s))
	 (bs   (map sbits-ref l)))
    
    (supervector-expand-right! s nn)
    
    (let lp ((i n) (l l) (ln ln))
      (when (pair? l)
	(let ((ss (car l))
	      (n  (car ln)))
	  (supervector-copy! ss 0 s i n #:ref? ref?)
	  (lp (+ i n) (cdr l) (cdr ln)))))
    
    s))

(define* (supervector-prepend
	 s l #:key (bits #f) (binsize *binsize*) (ro? #f) (ref? #f))
  
  (supervector-append
   s l #:bits bits #:binsize binsize #:ro? ro? #:ref? ref?))

(define* (supervector-prepend! s l #:key (binsize *binsize*) (ro? #f) (ref? #f))
  (let* ((ln (map supervector-length l))
	 (nn (fold + 0 ln))
	 (n  (supervector-length s))
	 (bs (map sbits-ref l)))
    
    (supervector-expand-left! s nn)
    
    (let lp ((i nn) (l l) (ln ln))
      (when (pair? l)
	(let ((ss (car l))
	      (n  (car ln)))
	  (supervector-copy! ss 0 s (- i n) n #:ref? ref?)
	  (lp (- i n) (cdr l) (cdr ln)))))
    
    s))
  
(define* (supervector-pop-right! s n #:key (binsize n) (ref? #f))
  (let* ((bits (sbits-ref s))
	 (s2   (make-supervector n #:bits bits #:binsize binsize))
	 (N    (supervector-length s)))
    (supervector-copy! s (- N n) s2 0 n #:ref? ref?)
    (supervector-shrink-right! s n)
    s2))

(define* (supervector-pop-left! s n #:key (binsize n) (ref? #f))
  (let* ((bits (sbits-ref s))
	 (s2   (make-supervector n #:bits bits #:binsize binsize)))
    (supervector-copy! s 0 s2 0 n #:ref? ref?)
    (supervector-shrink-right! s n)
    s2))
  
(define (supervector-reverse! s)
  (let ((t (w+ref s)))
    (w+set! s (w-ref s))
    (w-set! s t))
  
  (let ((t (n+ref s)))
    (n+set! s (n-ref s))
    (n-set! s t)))

(define* (supervector-reverse s #:key (binsize #f) (ref? #f))
  (let* ((n    (supervector-length s))
	 (bits (sbits-ref s))
	 (s2   (make-supervector n #:bits bits #:binsize binsize)))    	   
    (supervector-copy! s 0 s2 0 n #:ref? ref?)
    (supervector-reverse! s2)
    s2))

(define (supervector-remove! s i n)
  (supervector-copy! s (+ i n) s i (- (supervector-length s) n) #:ref? #t)
  (supervector-shrink-right! s n))

(define* (supervector-insert! s i s2 #:key (ref? #f))
  (define n (supervector-length s2))
  (supervector-expand-right! s n)
  (supervector-copy! s i s (+ i n) (- (supervector-length s) i) #:ref? #t)
  (supervector-copy! s2 0 s i n #:ref? ref?))

