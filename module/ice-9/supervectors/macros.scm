;;HACK
(define-syntax-rule (define-module- name args ...)
  (cond-expand
   (guile-3
    (define-module name
      #:declarative? #f
      args ...))
   (guile
    (define-module name args ...))))


(define-module- (ice-9 supervectors macros)
  #:use-module (ice-9 supervectors core)
  #:use-module (ice-9 supervectors typeinfo)
  #:use-module (rnrs bytevectors)
  #:export-syntax
  (AR AS ARS

      mk-get-setter

      get-setter
      get-reffer
      get-ref-setter

      get-setter-f
      get-reffer-f
      get-ref-setter-f

      standard-loop-1
      standard-loop-1*
      standard-loop-1-f
      standard-loop-2
      standard-loop-3

      iter-data

      neg pos index
      ))

(define-inlinable (neg i nw)    (- (- nw i) 1))
(define-inlinable (pos i nw)    (- i nw))
(define-inlinable (index i d n) (if (> d 0) i (- n i 1)))

(define-inlinable (iter-data bits m n lam)
  (let* ((dir (get-bit bits i-dir))
	 (d   (if (> dir 0) 1  (- 1)))
	 (i   (if (> dir 0) 0  (- n 1)))
	 (j   (- n i 1)))
    (lam i j d)))


(define-syntax dodo
  (syntax-rules ()
    ((_ 1 code1 code2)
     code2)
    
    ((_ _ code1 code2)
     code1)))

(define-syntax ARS
  (syntax-rules ()
    ((_ loop e f m: ((m a b) ...))
     (ARS loop e f f m: ((m a b) ...)))
    
    ((_ loop e fref fset m: ((m a b) ...))
     (case m:
       ((m)
	(dodo m
	      (if (= e 0)
		  (loop (lambda (v i    ) (fset (a v i            'little)))
			(lambda (v i val)       (b v i (fref val) 'little)))
		   
		  
		  (loop (lambda (v i    ) (fset (a v i            'big   )))
			(lambda (v i val)       (b v i (fref val) 'big   ))))

	      (loop (lambda (v i    ) (fset (a v i          )))
		    (lambda (v i val)       (b v i (fref val))))))
    
       ...))))

(define-syntax-rule (AR loop e f m: ((m a) ...))
  (case m:
    ((m)
     (dodo m
	   (if (= e 0)
	       (loop (lambda (v i) (f (a v i 'little))))
	       (loop (lambda (v i) (f (a v i 'big   )))))
	   (loop (lambda (v i)     (f (a v i)       )))))
    ...))

(define-syntax-rule (AS loop e f m: ((m a) ...))
  (case m:
    ((m)
     (dodo m
	   (if (= e 0)
	       (loop (lambda (v i x) (a v i (f x) 'little)))
	       (loop (lambda (v i x) (a v i (f x) 'big   ))))
	   (loop     (lambda (v i x) (a v i (f x)        )))))
    ...))

(define-syntax BR
  (lambda (x)
  (syntax-case x ()      
    ((_ AR loop (1 inv) e m a)
     #'(if (> inv 0)
	   (AR loop e (lambda (x) (logand (- (ash 1 (* 8 m)) 1)
					  (lognot x)))
	       m a)
	   (AR loop e (lambda (x) x)
	       m a)))

    ((_ AR loop (2) e m a)
     #'(AR loop e (lambda (x) (not x)) m a))

    ((_ AR loop (3 mm k) e m a)
     #'(AR loop e (lambda (x) (+ mm (* k x))) m a))
    
    ((_ AR loop (4 m1 k1 m2 k2) e m a)
     (if (eq? 'ARS (syntax->datum #'AR))
	 #'(AR loop e
	       (lambda (x) (+ m1 (* k1 x)))
	       (lambda (x) (+ m2 (* k2 x))) m a)
	 #'(BR AR loop (1 0) e m a))))))
	 
    
(define-syntax-rule (CR AR loop signed? inv e m a b)
  (if (> signed? 0)
      (BR AR loop inv e m a)
      (BR AR loop inv e m b)))

(define-syntax-rule (DR AR loop float? signed? inv e m a b c)
  (if (> float? 0)
      (BR AR loop inv e m c)
      (CR AR loop signed? inv e m a b)))

(define-syntax-rule (ER AR loop typed? float? signed? inv e m a b c)
  (if (> typed? 0)
      (DR AR loop float? signed? inv e m a b c)
      (BR AR loop inv e m b)))

(define-syntax-rule (FR AR loop scm? typed? float? signed? inv e m a b c d)
  (if (> scm? 0)
      (BR AR loop inv 0 1 d)
      (ER AR loop typed? float? signed? inv e m a b c)))


(define-syntax-rule (simplify-reacher reacher a ...)
  (lambda (doinv bits bits1 loop)
    (lambda ()
      ((reacher doinv bits bits1 loop) a ...))))

(define-syntax-rule (mk-get-setter get-setter do-inv? set AS a u c d)
  (define-inlinable (get-setter do-inv2? bits bits1 loop )
    (lambda* (#:key (is-typed    #f)
		    (is-scm      #f)
		    (is-float    #f)
		    (is-integer  #f)
		    (is-signed   #f)
		    (is-unsigned #f)
		    (is-endian   #f)
		    (is-m        #f)
		    (is-alg      #f)
		    (is-neg      #f)
		    (is-inv      #f)
		    (b           #f))
       
	  (let ((inv (if (and do-inv? do-inv2? (if is-inv (= is-inv 1)  #t))
			 (logand bits i-invert)
			 0))
	    
		(neg  (if (and do-inv? do-inv2? (if is-neg (= is-neg 1)  #t))
			  (logand bits i-negate)
			  0))
	    
		(a.s  (if (and (or (eq? set 2)
				   (and do-inv? do-inv2?))
			       (if is-alg (= is-alg 1)  #t)
			       b)
		      
			  (let ((x (d-ref b))
				(y (sc-ref b)))
			
			    (if (or (not (if is-alg (= is-alg 1) #t))
				    (and (eq? x 0) (eq? y 1)))
				#f
				(if set
				    (if (eq? set 1)
					(cons (- (/ x y)) (/ 1 y))
					(cons (cons x            y      )
					      (cons (- (/ x y))  (/ 1 y)) ))
				    (cons x y))))		      
			  #f)))
	    
	 (call-with-values (lambda () (expand-bits bits))
	   (lambda (-m -signed -float -scm -typed -e)
	     
	     (define (check is-m -m val . l)
	       (if is-m
		   (if (= val -m)
		       val
		       (error (format #f
				      "setter/reffer not valid for type ~a"
				      l)))
		   -m))
	     
	     (define m (if is-m
			   (begin
			     (when (not (eq? is-m -m))
			       (check 0 1 2 'm))
			     is-m)
			   -m))
	     
	     (define e (check is-endian -e
			      (if (eq? is-endian 'big)
				  i-end
				  0)
			      'end))
	     
	     (define signed
	       (cond
		(is-signed
		 (check is-signed -signed i-sign 's))
		(is-unsigned
		 (check is-unsigned -signed 0 'us))
		(else
		 -signed)))

	     (define float
	       (cond
		(is-float
		 (check i-float -float i-float 'fl))
		(is-integer
		 (check i-float -float 0 'int))
		(else
		 -float)))

	     (define scm
	       (cond
		(is-scm
		 (check is-scm -scm is-scm 'scm))
		(else
		 -scm)))

	     (define typed
	       (cond
		(is-typed
		 (check is-typed -typed is-typed 'tp))
		(else
		 -typed)))

	     #|
	     (when bits1
	       (when (not (can-expand bits1 bits #t))
		 (error "cannot expand values")))
	     |#
	     
	     (when (and (> inv 0) (> float 0))
	       (error "bug float types cannot be inverted"))

	     (let* ((typed   (if (> scm 0)   0 typed))
		    (float   (if (= float 0) 0 float))
		    (signed  (if (> float 0) 0 signed)))

	       (cond
		((> inv 0)
		 ;; invert the bits (lognot)
		 (FR AS loop scm typed float signed (1 1) e m a u c d))
		
		((> neg 0)
		 ;; negate the values (not)
		 (FR AS loop scm typed float signed (2) e m a u c d))
		
		(a.s
		 (if (or (not set) (= set 1))
		     (FR AS loop scm typed float signed
			 (3 (car a.s) (cdr a.s))
			 e m a u c d)
		     (FR AS loop scm typed float signed
			 (4 (caar a.s) (cdar a.s) (cadr a.s) (cddr a.s))
			 e m a u c d)))
		
		(else
		 (FR AS loop scm typed float signed
		     (1 0) e m a u c d))))))))))
  
(mk-get-setter get-setter #t 1 AS
	       ((1 bytevector-s8-set! )
		(2 bytevector-s16-set!)
		(4 bytevector-s32-set!)
		(8 bytevector-s64-set!))
	       
	       ((1 bytevector-u8-set! )
		(2 bytevector-u16-set!)
		(4 bytevector-u32-set!)
		(8 bytevector-u64-set!))

	       ((4 bytevector-ieee-single-set!)
		(8 bytevector-ieee-double-set!))

	       ((1 vector-set!)))


(mk-get-setter get-reffer #t #f AR
	       ((1 bytevector-s8-ref)
		(2 bytevector-s16-ref)
		(4 bytevector-s32-ref)
		(8 bytevector-s64-ref))
	       
	       ((1 bytevector-u8-ref)
		(2 bytevector-u16-ref)
		(4 bytevector-u32-ref)
		(8 bytevector-u64-ref))
	       
	       ((4 bytevector-ieee-single-ref)
		(8 bytevector-ieee-double-ref))
	       
	       ((1 vector-ref)))


(mk-get-setter get-ref-setter #t 2 ARS
	       ((1 bytevector-s8-ref  bytevector-s8-set!)
		(2 bytevector-s16-ref bytevector-s16-set!)
		(4 bytevector-s32-ref bytevector-s32-set!)
		(8 bytevector-s64-ref bytevector-s64-set!))
	       
	       ((1 bytevector-u8-ref   bytevector-u8-set!)
		(2 bytevector-u16-ref  bytevector-u16-set!)
		(4 bytevector-u32-ref  bytevector-u32-set!)
		(8 bytevector-u64-ref  bytevector-u64-set!))

	       ((4 bytevector-ieee-single-ref bytevector-ieee-single-set!)
		(8 bytevector-ieee-double-ref bytevector-ieee-double-set!))

	       ((1 vector-ref vector-set!)))

;; Use these if we want faster compilation we do not explode the number of loops
(define get-setter-f get-setter)
(define get-reffer-f get-reffer)
(define get-ref-settfer-f get-ref-setter)


(define-syntax-rule (standard-loop-1 doinv bits i j d bits2 (get-setter l ...)
				   lam)
  ((get-setter doinv bits bits2
     (lambda (set)
       (let* ((m (get-m bits))
	      (d (* m d))
	      (i (* m i))
	      (j (* m j)))
	 (let lp ((k i))
	   (lam set k)
	   (when (not (= k j))
	     (lp (+ k d)))))))
   l ...))

(define-syntax-rule (standard-loop-1* doinv bits i j d bits2 (get-setter l ...)
				   lam)
  ((get-setter doinv bits bits2
     (lambda (ref set)
       (let* ((m (get-m bits))
	      (d (* m d))
	      (i (* m i))
	      (j (* m j)))
	 (let lp ((k i))
	   (lam ref set k)
	   (when (not (= k j))
	     (lp (+ k d)))))))
   l ...))

(define-syntax-rule (standard-loop-1-f doinv bits i j d bits2 (get-setter l ...)
				   lam)
  ((get-setter doinv bits bits2
     (lambda (set)
       (let* ((m (get-m bits))
	      (d (* m d))
	      (i (* m i))
	      (j (* m j)))
	 (let lp ((k i))
	   (let ((r (lam set k)))
	     (if(not (= k j))
		(lp (+ k d))
		r))))))
   l ...))





(define-syntax-rule (standard-loop-2 do-inv?
				     (bits1 i1 j1 d1)
				     (bits2 i2 j2 d2)
				     (get-setter l1 ...)
				     (get-reffer l2 ...)
				     lam)
  ((get-setter do-inv? bits1 bits2
     (lambda (set)
       ((get-reffer do-inv? bits2 #f
	  (lambda (ref)
	    (let* ((m1 (get-m bits1))
		   (d1 (* m1 d1))
		   (i1 (* m1 i1))
		   (j1 (* m1 j1))
		   (m2 (get-m bits2))
		   (d2 (* m2 d2))
		   (i2 (* m2 i2))
		   (j2 (* m2 j2)))

	    (let lp ((k1 i1) (k2 i2))
	      (lam set k1 ref k2)	      
	      (when (not (= k1 j1))
		(lp (+ k1 d1) (+ k2 d2))))))) l2 ...))) l1 ...))





(define-syntax-rule (standard-loop-3
		     do-inv
		     (bits1 i1 j1 d1)
		     (bits2 i2 j2 d2)
		     (get-reffer     l1 ...)
		     (get-ref-setter l2 ...)
		     lam)
  ((get-reffer #f bits1 bits2
     (lambda (ref1)
       ((get-ref-setter do-inv bits2 #f
	 (lambda (ref2 set2)
	   (let* ((m1 (get-m bits1))
		  (dd1 (* m1 d1))
		  (ii1 (* m1 i1))
		  (jj1 (* m1 j1))
		  (m2 (get-m bits2))
		  (dd2 (* m2 d2))
		  (ii2 (* m2 i2))
		  (jj2 (* m2 j2)))

	     (let lp ((k1 ii1) (k2 ii2))
	       (lam ref1 k1 ref2 set2 k2)
	       (when (not (= k1 jj1))
		 (lp (+ k1 dd1) (+ k2 dd2))))))) l2 ...))) l1 ...))
