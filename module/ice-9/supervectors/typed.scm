;;HACK
(define-syntax-rule (define-module- name args ...)
  (cond-expand
   (guile-3
    (define-module name
      #:declarative? #f
      args ...))
   (guile
    (define-module name args ...))))


(define-module- (ice-9 supervectors typed)
  #:use-module (ice-9 supervectors core)
  #:use-module (ice-9 supervectors macros)
  #:use-module (ice-9 supervectors expand)
  #:use-module (ice-9 supervectors typeinfo)
  #:export (supervector-ref supervector-set!))

(define (supervector-ref s i)
  (define binsize (sbz-ref s))
  (define n-      (n-ref s))
  (define n+      (n+ref s))
  (cond
   ((< i 0)
    (error "index underflow"))

   ((>= i (+ n- n+))
    (error (format #f "index overflow index: ~a >= ~a" i (list '+ n- n+))))

   (else
    (let ()
      (define (f w ii n)
	(let* ((N  (quotient ii binsize))
	       (i  (modulo   ii binsize))
	       (c  (vector-ref w N)))
	  (let lp ((c c) (i i))
	    (let* ((v    (v-ref   c))
		   (bits (bits-ref c))
		   (m    (get-m bits))
		   (d    (get-dir bits))
		   (n    (sz-ref  c))
		   (i    (index i d n)))
	      (cond
	       ((supervector? v)
		(supervector-ref v i))
	     
	       ((bin? v)
		(lp v i))

	       ((not v)
		(d-ref c))

	       (else
		((get-reffer #t bits #f
		   (lambda (ref)
		     (ref v (* m i)))))))))))
	    
      (if (< i n-)
	  (f (w-ref s) (neg i n-) n-)
	  (f (w+ref s) (pos i n-) n+))))))

(define (supervector-set! s i val)
  (define (read-only? b) (> (get-bit (bits-ref b) i-ro) 0))
  (define binsize (sbz-ref s))
  (define n-      (n-ref s))
  (define n+      (n+ref s))
  (cond
   ((< i 0)
    (error "index underflow"))

   ((>= i (+ n- n+))
    (error "index overflow"))

   (else
    (let ()
      (define (f w ii n)
	(let* ((N  (quotient ii binsize))
	       (i  (modulo   ii binsize))
	       (c  (vector-ref w N)))
	  (let lp ((c c) (i i))
	    (let* ((v (v-ref   c))
		   (bits (bits-ref c))
		   (m    (get-m bits))
		   (d    (get-dir bits))
		   (n (sz-ref  c))
		   (i (index i d n)))
	      (cond
	       ((supervector? v)
		(supervector-ref v i))
	     
	       ((bin? v)
		(if (read-only? v)
		    (let ((val2 (supervector-ref s i)))
		      (if (or (and (number? val) (= val val2))
			      (eq? val val2))
			  (values)
			  (begin
			    (fix-cow c i i)
			    (lp c i))))
		    (lp v i)))

	       ((not v)
		(if (equal? (d-ref c) val)
		    (values)
		    (begin
		      (populate c i)
		      (lp c i))))
		      

	       (else
		((get-setter #t bits #f
		   (lambda (set)
		     (set v (* m i) val))))))))))

      (let ((bits (sbits-ref s)))
	(when (> (get-bit bits i-ro) 0)
	  (error "can't set ro? supervector"))
	
	(if (< i n-)
	    (f (w-ref s) (neg i n-) n-)
	    (f (w+ref s) (pos i n-) n+)))))))

