;;HACK
(define-syntax-rule (define-module- name args ...)
  (cond-expand
   (guile-3
    (define-module name
      #:declarative? #f
      args ...))
   (guile
    (define-module name args ...))))


(define-module- (ice-9 supervectors printer)
  #:use-module (srfi srfi-9 gnu)
  #:use-module (ice-9 control)
  #:use-module (rnrs bytevectors)
  #:use-module (ice-9 supervectors core)
  #:use-module (ice-9 supervectors macros)
  #:use-module (ice-9 supervectors typed)
  #:use-module (ice-9 supervectors functional)
  #:use-module (ice-9 supervectors typeinfo))
		
(set-record-type-printer! <bin>
  (lambda (bin port)
    (let* ((bits (bits-ref bin))
	   (mm   (get-m bits))
	   (n    (* mm (sz-ref bin)))
	   (m    (min n (* mm 25)))
	   (dir  (get-dir bits))
	   (di   (if (> dir 0) mm (- mm)))
	   (i0   (if (> dir 0) 0 (- n mm)))
	   (j0   (- n i0 mm))
	   (fl   (get-bit bits i-float))
	   (sg   (get-bit bits i-sign))
	   (scm  (get-bit bits i-scm))
	   (v    (v-ref bin))
	   (!    (if (= (get-bit bits i-ro) 0) "!" ""))
	   (d    (if (> scm 0) "" (* 8 mm)))
	   (ch   (if (> scm 0) "scm" (if (> fl 0) "f" (if (> sg 0) "s" "u")))))

      
      (format port "<bin-~a~a~a " ch d !)

      ;; need the functional tool here

      (cond
       ((not v)
	(format port "~a:~a" (sz-ref bin) (d-ref bin)))

       ((or (vector? v) (bytevector? v))
	(let ((sc (sc-ref bin))
	      (d  (d-ref  bin)))
	  ((get-reffer #t bits #f			
	     (lambda (ref)	  
	       (when (> n 0)
		 (let lp ((i i0))
		   (format port "~a "
			   (if (and (= sc 1) (= d 0))
			       (ref v i)
			       (+ d (* sc (ref v i)))))
		   (when (not (= i j0))
		     (lp (+ i di)))))))))

	(when (> n m)
	  (format port "... ")))
       
       (else
	(format port "~a" v)))

      	   
      (format port ">"))))

(define pr-superstring
  (lambda (ss port)
    (define (hex x)
      (char->integer (car (string->list (number->string x 16)))))
    
    (let* ((n 0)
	   (l (map integer->char
		   (let/ec out		
		     (supervector-fold
		      (lambda (x seed)
			(set! n (+ n 1))
			(when (> n 1000)			      
			  (out seed))
			(cond
			 ((or (= x 92) (= x 34))
			  (cons* x 92 seed))
			 ((<= x 13)
			  (cond
			   ((= x 13)
			    (cons* 114 92 seed))
			   ((= x 12)
			    (cons* 102 92 seed))
			   ((= x 11)
			    (cons* 118 92 seed))
			   ((= x 10)
			    (cons* 110 92 seed))
			   ((= x 9)
			    (cons* 116 92 seed))
			   ((= x 8)
			    (cons* 98 92 seed))
			   ((= x 7)
			    (cons* 97 92 seed))
			   ((= x 0)
			    (cons* 48 92 seed))
			   ((<= x 6)
			    (cons* (hex (modulo   x 16))
				   (hex (quotient x 16))
				   120 92 seed))))
			 
			 ((< x 32)
			  (cons* (hex (modulo   x 16))
				 (hex (quotient x 16))
				 120
				 92 seed))
			 
			 (else
			  (cons x seed))))
		      '()
		      ss)))))
      
      (format port "sup\"~a~a\""
	      ((@ (guile) list->string) (reverse l))
	      (if (>= n 1000) " ..." "")))))


(define pr-supervector
  (lambda (s port)
    (let* ((bits (sbits-ref s))
	   (mm   (get-m bits))
	   (w+   (w+ref s))
	   (fl   (get-bit bits i-float))
	   (sg   (get-bit bits i-sign))
	   (scm  (get-bit bits i-scm))
	   (d    (if (> scm 0) "" (* 8 mm)))
	   (ch   (if (> scm 0) "scm" (if (> fl 0) "f" (if (> sg 0) "s" "u")))))
      
      (format port "<sup-~a~a " ch d)

      (let ((m+ (vector-length w+)))
	(let lp ((i 0))
	  (when (< i m+)
	    (if (= i (- m+ 1))
		(format port "~a"  (vector-ref w+ i))
		(format port "~a " (vector-ref w+ i)))
	    (lp (+ i 1)))))
      	   
      (format port ">"))))

(define pr-superbit
  (lambda (s port)
    (format port "#sup*")
    (let* ((bits (sbits-ref s))
	   (k    (+ 1 (get-e bits)))
	   (n    (supervector-length s)))
      (let lp ((i 0))
	(cond
	 ((> i 128)
	  (format port "...")
	  (values))
	 
	 ((< i (- n 1))
	  (let ((b64 (supervector-ref s i)))
	    (let lp2 ((j 0))
	      (if (< j 64)
		  (begin
		    (format port "~d" (logand #x1 (ash b64 (- j))))
		    (lp2 (+ j 1)))
		  
		  (lp (+ i 1))))))

	 ((< i n)
	  (let ((b64 (supervector-ref s i)))
	    (let lp2 ((j 0))
	      (if (< j k)
		  (begin
		    (format port "~d" (logand #x1 (ash b64 (- j))))
		    (lp2 (+ j 1)))
		  (values))))))))))

(set-record-type-printer! <supervector>
  (lambda (s port)
    (let* ((bits (sbits-ref s)))
      (cond
       ((> (get-bit bits i-superbit) 0)
	(pr-superbit s port))
       ((> (get-bit bits i-superstr) 0)
	(pr-superstring s port))
       (else       
	(pr-supervector s port))))))
 
