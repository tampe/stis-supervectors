;;HACK
(define-syntax-rule (define-module- name args ...)
  (cond-expand
   (guile-3
    (define-module name
      #:declarative? #f
      args ...))
   (guile
    (define-module name args ...))))


(define-module- (ice-9 supervectors core)
  #:use-module (ice-9 supervectors typeinfo)
  #:use-module (rnrs bytevectors)
  #:use-module (srfi srfi-9)
  #:export-syntax
  (vv-ref
   aif

   <uq>
   
   supervector?
   make-supervector0
   w-ref     w-set!
   w+ref     w+set!
   n-ref     n-set!
   n+ref     n+set!
   sbz-ref   sbz-set!
   sd-ref    sd-set!
   sbits-ref sbits-set!
   
   
   bin?
   mk-bin0
   v-ref     v-set! 
   d-ref     d-set!  
   sz-ref    sz-set!
   bs-ref    bs-set!
   bits-ref  bits-set! 
   j-ref     j-set!
   sc-ref    sc-set!
   
   get-bin1 get-bin2 last-bin
         
   supervector-length

   clear-bits
   clear-constant-data
   clear-constant-bits
   )
  
  #:export
  (   
   <supervector>
   <bin>
   
   b-copy-ref
   b-copy
   b-copy!

   core-supervector-copy!
   core-supervector-count-for-combine   
   *binsize*

   id
   
   make-bytevector*
   bytevector-copy*!
   bytevector-length*
   
   can-expand
   can-add
   can-scale
   can-negate
   can-invert

   negate-bin
   invert-bin
   add-bin
   scale-bin
   
   read-only?
   DD
   supervector-make-ro!
   ))

;; Default binsize optimised for speed, may bi different in different arcs
(define *binsize* (ash 1 13))

;; Use this to prevent compiler bugs!
(define (id x) x)

(define-syntax aif
  (syntax-rules ()
    ((_ (it . l) p a b)
     (call-with-values (lambda () p)
       (lambda (it . l)
	 (if it a b))))
    
    ((_ it p a b)
     (let ((it p))
       (if it a b)))))

(define mask1   #xffff)
(define imask1  (lognot mask1))
(define mask2   #xffff0000)
(define imask2  (lognot mask2))
(define mask3   #xffff00000000)
(define imask3  (lognot mask3))

(define-inlinable (j-ref x)
  (let ((x (x-ref x)))
    (if (number? x)
	(logand x mask1)
	(vector-ref x 0))))

(define-inlinable (j-set! x v)
  (let ((xx (x-ref x)))
    (if (number? xx)
	(x-set! x (logior (logand xx imask1) v))
	(vector-set! xx 0 v))))
  
(define-inlinable (bs-ref x)
  (let ((x (x-ref x)))
    (if (number? x)
	(ash (logand x mask2) -16)
	(vector-ref x 1))))
  
(define-inlinable (bs-set! x v)
  (let ((xx (x-ref x)))
    (if (number? xx)
	(x-set! x (logior (logand xx imask2) (ash v 16)))
	(vector-set! xx 1 v))))

(define-inlinable (sz-ref x)
  (let ((x (x-ref x)))
    (if (number? x)
	(ash (logand x mask3) -32)
	(vector-ref x 2))))

(define-inlinable (sz-set! x v)
  (let ((xx (x-ref x)))
    (if (number? xx)
	(x-set! x (logior (logand xx imask3) (ash v 32)))
	(vector-set! xx 2 v))))

(define-record-type <uq>  
  (mk-uq u q)
  uq?
  (u u-ref u-set!)
  (q q-ref q-set!))

(define-record-type <bin>
  (mk-bin00 v ds x bits)
  bin?
  (v     v-ref   v-set!     )           ; heteregenous data
					; #f in case of not populated
					; <supervector> in case of tree
					; <bin> in case of reference cow or ref
					; bytevector in case of fixed data
					; vector in case of scm data
  
  (ds    ds-ref  ds-set!     )          ; default value and scale
				 
  
  (x     x-ref   x-set!    )            ; default values
    
  (bits  bits-ref bits-set! ))          ; bitvector containing type info


(define-inlinable (mk-bin0 v d sz bs bits j scale)
  (let ((x (if (< bs (ash 1 16))
	       (logior j (ash bs 16) (ash sz 32))
	       (vector j bs sz))))
    (mk-bin00 v (if (= scale 1) d (mk-uq d scale)) x bits)))

(define-inlinable (d-ref b)
  (let ((x (ds-ref b)))
    (if (uq? x)
	(u-ref x)
	x)))

(define-inlinable (sc-ref b)
  (let ((x (ds-ref b)))
    (if (uq? x)
	(q-ref x)
	1)))

(define-inlinable (d-set! b v)
  (let ((x (ds-ref b)))
    (if (uq? x)
	(u-set! x v)
	(ds-set! b v))))

(define-inlinable (sc-set! b v)
  (let ((x (ds-ref b)))
    (if (uq? x)
	(if (eq? v 1)
	    (ds-set!  b (u-ref x))
	    (q-set! x v))
	(if (eq? v 1)
	    (values)
	    (ds-set! b (mk-uq x v))))))

	
;; length of <bin>
(define nbin 4)

(define-record-type <supervector>
  (make-supervector0 w- w+ n- n+ sbz sbits sd) 
  supervector?
  (w-    w-ref     w-set!      )        ; tail
  (w+    w+ref     w+set!      )        ; head 
  (n-    n-ref     n-set!      )        ; length in tail
  (n+    n+ref     n+set!      )        ; length in head
  (sbz   sbz-ref   sbz-set!    )        ; size of maxbin
  (sbits sbits-ref sbits-set!  )	; type info      
  (sd    sd-ref    sd-set!     ))       ; default value

(define* (make-bytevector* n #:optional (init  0) #:key (bits #f))
  (if (and bits (> (logand i-scm bits) 0))
      (make-vector n init)
      (make-bytevector n init)))

(define (bytevector-copy*! v1 i1 v2 i2 n)
  (if (bytevector? v1)
      (bytevector-copy! v1 i1 v2 i2 n)
      
      (let lp ((i1 i1) (i2 i2) (k 0))
	(if (< k n)
	    (when
	      (vector-set! v2 i2 (vector-ref v1 i1))
	      (lp (+ i1 1) (+ i2 1) (+ k 1)))))))

(define (bytevector-length* v)
  (if (bytevector? v)
      (bytevector-length v)
      (vector-length v)))


(define (b-copy-ref b)
  (apply mk-bin00 (map (lambda (i) (struct-ref b i)) (iota nbin))))

(define (b-copy b)
  (let* ((b2 (b-copy-ref b))
	 (v  (v-ref b2)))
    (when v
      (v-set! b2
	      (cond
	       ((bytevector? v)
		(bytevector-copy v))
	       ((supervector? v)
		(s-copy v))
	       (else
		v))))
    b2))

(define (b-copy! b1 b2)
  (for-each
   (lambda (i)
     (struct-set! b2 i (struct-ref b1 i)))
   (iota nbin)))

(define (s-copy s)
  (define (cpv v)
    (let* ((n  (vector-length v))
	   (vv (make-vector n)))
      (let lp ((i 0))
	(when (< i n)
	  (vector-set! vv i (b-copy (vector-ref v i)))))))
  
  (make-supervector0
   (cpv (struct-ref s 0))
   (cpv (struct-ref s 1))
   (struct-ref s 2)
   (struct-ref s 3)
   (struct-ref s 4)
   (struct-ref s 5)
   (struct-ref s 6)))


(define-inlinable (supervector-length s) (+ (n+ref s) (n-ref s)))

;; the idea is that the first bytevector may be different then the rest
;; but we try to keep them uniform to allow trees wit very small depth (1)
(define-inlinable (get-bin1 s)
  (vector-ref (w+ref s) 0))

(define-inlinable (get-bin2 s)
  (vector-ref (w+ref s) 1))

;; when looking at extending bits
(define-inlinable (last-bin s)
  (vector-ref (w+ref s) (- (length (w+ref s)) 1)))


(define-inlinable (clear-bits b)
  (let* ((bits (bits-ref b))
	 (bits (clear-bit bits i-cow))
	 (bits (clear-bit bits i-extend)))
    (bits-set! b bits)))

(define core-supervector-copy!
  (lambda x (error "we need to patch this after this module")))

(define-inlinable (clear-constant-bits bits)
  (let* ((bits (clear-bit bits i-invert))
	 (bits (clear-bit bits i-negate)))
    bits))
  
(define-inlinable (clear-constant-data b2)
  (let* ((bits (bits-ref  b2)))
    (bits-set! b2 (clear-constant-bits bits))
    (sc-set! b2 1)
    (d-set! b2 0)))

;; We can add and scale if we have not negated or inverted bits
(define (can-add b)
  (let ((bits (bits-ref b)))
    (= (get-bit bits (logior i-invert i-negate)) 0)))

(define (can-scale b)
  (let ((bits (bits-ref b)))
    (= (get-bit bits (logior i-invert i-negate)) 0)))

;; We can negate (not x) if no add and scale and inverted bits
(define (can-negate b)
  (let ((bits (bits-ref b)))
    (and
     (= (get-bit bits (logior i-invert)) 0)
     (eq? (d-ref  b) 0)
     (eq? (sc-ref b) 1))))

;; We can invert bits (lognot x) if no add and scale and negated expression
(define (can-invert b)
  (let ((bits (bits-ref b)))
    (and
     (= (get-bit bits (logior i-negate)) 0)
     (eq? (d-ref  b) 0)
     (eq? (sc-ref b) 1))))

(define-inlinable (negate-bin b)
  (let* ((bits (bits-ref b))
	 (bits (logxor i-negate bits)))
    (bits-set! b bits)))

(define-inlinable (invert-bin b)
  (let* ((bits (bits-ref b))
	 (bits (logxor i-invert bits)))
    (bits-set! b bits)))

(define (add-bin b val)
  (d-set! b (+ (d-ref b) val)))

(define (scale-bin b val)
  (d-set!  b (* (d-ref  b) val))
  (sc-set! b (* (sc-ref b) val)))

(define (read-only? b)
  (> (get-bit (bits-ref b) i-ro) 0))

(define-inlinable (vv-ref v i j d)
  (if (bin? v)
      (let* ((vv (v-ref v))
	     (d2 (get-dir (bits-ref v)))
	     (ii (if (> d2 0) (+ (j-ref v) i) (- (j-ref v) i)))
	     (jj (if (> d2 0) (+ (j-ref v) j) (- (j-ref v) j)))
	     (dd (* d d2)))
	(values vv ii jj dd))
      (values v i j d)))

(define (DD f)
  (lambda (a1 b1 c1 d1 a2 b2 c2 d2 seed)
    (call-with-values (lambda () (f a1 b1 c1 d1 a2 b2 c2 d2))
      (lambda (x)
	(values x #t)))))

(define supervector-make-ro! #f)
