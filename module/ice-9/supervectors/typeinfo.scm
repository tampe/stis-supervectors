;;HACK
(define-syntax-rule (define-module- name args ...)
  (cond-expand
   (guile-3
    (define-module name
      #:declarative? #f
      args ...))
   (guile
    (define-module name args ...))))


(define-module- (ice-9 supervectors typeinfo)
  #:use-module (rnrs bytevectors)
  #:export-syntax
  (
   i-sg       ; signed?
   i-fl       ; float?
   i-scm      ; scm?
   i-typed    ; typed?
   i-exact    ; exaxt?
   i-dir      ; dir 1 = 1 / 0 = -1
   i-ro       ; read only?
   i-end      ; endian (little = 0, big = 1)
   i-expand   ; expandable?
   i-cow      ; copy on write reference
   i-ref      ; sharing?
   i-invert   ; inverted?
   i-extend   ; extendable?
   i-sign     ; signed integer
   i-resize   ; signed integer
   i-float    ; float type
   i-negate   ; logical not
   i-superbit ;
   i-superstr ;
   i-native   
   i-algebra
   i-strict
   
   inv-bit
   get-bit
   set-bit
   clear-bit

   l-mask
   e-mask
   m-mask
   
   get-m
   get-e
   get-level
   get-dir
   
   set-level
   set-m
   set-e
   set-type
   
   clear-bits
   expand-bits
   )
  
  #:export
  (expand-due-to-val?
   get-type-from-val
   make-bits))



#|
When we working with the type information we will essentially update the
bits functionally that allows for some compiler optimisations and also
has a clean interface. So in the code we need to check is the bit is greater
than zero or not
|#

(define-inlinable (get-bit   bits i) (logand bits i))
(define-inlinable (clear-bit bits i) (logand bits (lognot i)))
(define-inlinable (set-bit   bits i) (logior (clear-bit bits i) i))
(define-inlinable (inv-bit   bits i) (logxor bits i))


(define m-mask     #xf)
(define l-mask     #xf0)
(define l-shift    4)
(define e-mask     #x3f00)
(define e-shift    8)
(define xx         (+ 4 4 6))

(define-syntax-rule (define-bit x)
  (begin
    (define x (ash 1 xx))
    (set! xx (+ xx 1))))

(define-bit i-sign )   ; signed?
(define-bit i-float)   ; float?
(define-bit i-scm  )   ; scm?
(define type-mask   (logior m-mask i-sign i-float i-scm))

(define-bit i-typed )  ; typed?
(define-bit i-exact )  ; exaxt?
(define-bit i-dir   )  ; dir 1 = 1 / 0 = -1
(define-bit i-ro    )  ; read only?
(define-bit i-end   )  ; the endian type
(define-bit i-expand)  ; expandable? alows expanding types u8 -> s16 ...
(define-bit i-cow   )  ; copy on write reference
(define-bit i-ref   )  ; sharing?
(define-bit i-invert)  ; inverted?
(define-bit i-negate)  ; negate!!
(define-bit i-extend)  ; extend bit in case we invert a bin and want to extend
(define-bit i-resize)  ; is resizing of the supervector is allowed
(define-bit i-superbit)
(define-bit i-superstr)
(define-bit i-algebra)
(define-bit i-strict)
(values)	       ; this bit is copied to the extension.


(define i-native (if (eq? (native-endianness) 'big) i-end 0))

(define-inlinable (get-level bits)
  (ash (logand bits l-mask) (- l-shift)))

(define-inlinable (get-e bits)
  (ash (logand bits e-mask) (- e-shift)))

(define-inlinable (get-m bits)
  (ash 1 (logand bits m-mask)))

(define-inlinable (get-dir bits)
  (if (> (get-bit bits i-dir) 0) 1 -1))

(define-inlinable (set-type bits type)
  (logior (clear-bit bits type-mask) type))

(define-inlinable (set-level bits level)
  (logior (clear-bit bits l-mask) (ash level l-shift)))

(define-inlinable (set-e bits level)
  (logior (clear-bit bits e-mask) (ash level e-shift)))

(define-inlinable (set-extend bits extend)
  (logior (clear-bit bits e-mask) (ash extend e-shift)))

(define-inlinable (set-m bits m)
  (logior (clear-bit bits m-mask)
	  (case m
	    ((1) 0)
	    ((2) 1)
	    ((4) 2)
	    ((8) 3))))

(define-inlinable (expand-bits bits)
  (values
   (get-m bits   )
   (logand bits  i-sign)
   (logand bits  i-float)
   (logand bits  i-scm)
   (logand bits  i-typed)
   (logand bits  i-end)))

(define (m-it x)
  (case x
    ((1)  0)
    ((2)  1)
    ((4)  2)
    ((8)  3)
    ((16) 4)))
  

(define* (make-bits #:key
		    (bits       #f)
		    (m          #f)   ;; 1 2 4 8  basic sizes		
		    (signed?    #f)   ;; signed type
		    (float?     #f)   ;; floating point value
		    (scm?       #f)   ;; scm type
		    (typed?     #f)   ;; allow type variations 
		    (endian     #f)
		    (superbit?  #f)
		    (superstr?  #f)
		    (bitcorrect 0)
		    (resizable? #f)   ;; needed for queues and stacks
		    (ro?        #f)   ;; read only
		    (dir        #f)   ;; if we shall scan form left to right
		    (expand?    #f)   ;; allow expansion
		    (level      #f))  ;; depth of tree compression


  (define (check a? i-a  bits)
    (if a? (if (= a? 1) i-a 0) (get-bit bits i-a)))
  
  (when (or (< level 0) (>= level  16))
    (error "level must be beteen 0 and 15"))
  
  (let* ((bits       (if bits  bits    (logior i-dir i-native)))
	 (m          (m-it (if m     m       (get-m     bits))))
	 (level      (if level level   (get-level bits)))
	 (signed?    (check signed?    i-sign   bits))
	 (float?     (check float?     i-float  bits))
	 (scm?       (check scm?       i-scm    bits))
	 (typed?     (if (not typed?)
			 i-typed
			 (check typed?     i-typed  bits)))
	 (endian     (if endian
			 (if (eq? endian 'big)
			     i-end
			     0)
			 (get-bit bits i-end)))
	 (superbit?  (check superbit?  i-superbit bits))
	 (superstr?  (check superstr?  i-superstr bits))
	 (resizable? (check resizable? i-resize bits))
	 (ro?        (check ro?        i-ro     bits))
	 (expand?    (check expand?    i-expand bits))
	 (dir        (if dir
			 (and (> dir 0) i-dir)
			 (get-bit bits  i-dir))))

    (when (> scm? 0)
      (set! typed? 0))

    (when (= typed? 0)
      (set! signed? 0)
      (set! float?  0))

    (when (> float? 0)
      (set! signed? 0))
    
    (logior
     m
     (ash (max 0 (- bitcorrect 1)) e-shift)
     signed?
     float?
     scm?
     typed?
     endian
     (ash level l-shift)
     resizable?
     ro?
     dir
     expand?
     superbit?
     superstr?)))

;; returns (m signed? float? scm?)
(define (get-type-from-val x)
  (if (number? x)
      (if (integer? x)
	  (if (< x 0)
	      (let ((x (- x)))
		(cond
		 ((<= x (ash 1 7))
		  (values 1 i-sign 0 0))
		 ((<= x (ash 1 15))
		  (values 2 i-sign 0 0))
		 ((<= x (ash 1 31))
		  (values 4 i-sign 0 0))
		 ((<= x (ash 1 63))
		  (values 8 i-sign 0 0))))
	      (cond
	       ((< x (ash 1 8))
		(values 1 0 0 0))
	       ((< x (ash 1 16))
		(values 2 0 0 0))
	       ((< x (ash 1 32))
		(values 4 0 0 0))
	       ((< x (ash 1 64))
		(values 8 0 0 0))))
	  (if (real? x)
	      (values 8 0 i-float 0)
	      (values 1 0 0       i-scm)))
      (values 1 0 0 i-scm)))
	  

;; Can we expand 1 -> 2? e.g u8 -> s16
(define (can-expand bits1 bits2 exp-ok?)
  (let ((m1      (get-m bits1))		    
	(m2      (get-m bits2))
	(sgn1    (get-bit bits1 i-sign))
	(sgn2    (get-bit bits2 i-sign))
	(scm1    (get-bit bits1 i-scm))
	(scm2    (get-bit bits2 i-scm))	
	(exp     (get-bit bits1 i-expand))
	(sg1     (get-bit bits1 i-sign))
	(sg2     (get-bit bits2 i-sign))
	(fl1     (get-bit bits1 i-float))
	(fl2     (get-bit bits2 i-float))
	(exact1  (get-bit bits1 i-exact))
	(exp1    (get-bit bits1 i-expand))
	(typed1  (get-bit bits1 i-typed))
	(typed2  (get-bit bits2 i-typed)))

    (cond
     ((or (> scm1 0) (> typed1 0))
      (set! sg1 0)
      (set! fl1 0))

     ((> fl1 0)
      (set! sg1 0)))
    
    (cond
     ((or (> scm2 0) (> typed2 0))
      (set! sg2 0)
      (set! fl2 0))

     ((> fl2 0)
      (set! sg2 0)))
        
    (or
     (and (= m1 m2) (= sg1 sg1) (= fl1 fl2)  (= scm1 scm2))
     (and (or exp-ok? (> exp1 0))
	  (or (<= (+ (if (> sgn1 0) (/ 1 2) 0)
		     m1)
		  (+ (if (> sgn2 0) (/ 1 2) 0)
		     m2))
		   
	      (and (> fl2 0)
		   (or (> fl1 0) (= exact1 0)))
		    
	      (and (> scm2 0) (or (> scm1 0) (> exp 0))))))))


(define (expand-due-to-val? bits val)
  (call-with-values (lambda () (expand-bits bits))
    (lambda (m1 signed1 float1 scm1 type1 . u)
      (call-with-values (lambda () (get-type-from-val val))
	(lambda (m2 signed2 float2 scm2)
	  (let ((strict1 (get-bit bits i-strict))
		(m   (max m1 m2))
		(sg  (logior signed1 signed2))
		(fl  (logior float1  float2 ))
		(scm (logior scm1    scm2 )))

	    (when (> scm 0)
	      (set! m  1)
	      (set! fl 0)
	      (set! sg 0))
	      
	    (when (> fl 0)
	      (set! sg 0))
	      	    
	    (let* ((bits2
		    (cond
		     ((> scm 0)
		      (if (> scm1 0)
			  (begin
			    (set! m 1)
			    (set! m2 1)
			    (set! m1 1)
			    #f)
			  (begin
			    (set! m  1)
			    (set! m2 0)
			    scm)))
		     
		     ((> fl 0)
		      (if (> float1 0)
			  (if (> strict1 0)
			      (if (> float2 0)
				  #f
				  (begin
				    (set! m2 (+ m2 1))
				    (set! m1 (if (> m2 m1) 4 m1))
				    (set! m  (max m m1))
				    #f))
			      (begin
				(set! m2 0)
				#f))
			  fl))
		     
		     ((> sg 0)				
		      (if (> signed1 0)
			  (if (> signed2 1)
			      #f
			      (begin
				(when (= m1 m2)
				  (set! m2 (+ m2 1))
				  (set! m  (+ m1 1))			       
				  #f)))
			  sg))
		     (else
		      #f)))
	      
		   (bits2 (if bits2
			      (set-m bits2 m)
			      (if (<= m2 m1)
				  #f
				  (logior m sg fl scm)))))
	      
		   
	      (if bits2
		  (set-type bits bits2)
		  #f))))))))
   	
