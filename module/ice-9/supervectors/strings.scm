;;HACK
(define-syntax-rule (define-module- name args ...)
  (cond-expand
   (guile-3
    (define-module name
      #:declarative? #f
      args ...))
   (guile
    (define-module name args ...))))


(define-module- (ice-9 supervectors strings)
  #:use-module (ice-9 supervectors typed)
  #:use-module (ice-9 supervectors util)
  #:use-module (ice-9 supervectors core)
  #:use-module (ice-9 supervectors make)
  #:use-module (ice-9 supervectors functional)
  #:use-module (ice-9 supervectors append)
  #:use-module (ice-9 supervectors copy)
  #:use-module (ice-9 supervectors logical)
  #:use-module (ice-9 supervectors printer)
  #:use-module (srfi srfi-9)
  #:use-module (ice-9 control)
  #:use-module (rnrs bytevectors)
  #:use-module (srfi srfi-9 gnu)
  #:export (make-superstring
	    superstring?
	    superstring-length

	    superstring-append!
	    superstring-prepend!
	    
	    superstring-stream
	    superstring-fold
	    superstring-fold2
	    superstring-fold-right
	    superstring-map
	    superstring-map!
	    superstring-for-each
	    superstring-for-each-index

	    list->superstring
	    superstring->list
	    superstring-copy
	    superstring-copy!

	    superstring-utf8-length
	    superstring-bytes-per-char

	    superstring-ref
	    superstring-set!
	    superstring-append
	    superstring-append/shared

	    superstring=?
	    superstring<?
	    superstring<=?
	    superstring>?
	    superstring>=?
	    superstring-ci=?
	    superstring-ci<?
	    superstring-ci<=?
	    superstring-ci>?
	    superstring-ci>=?

	    superstring-sub
	    superstring-fill!

	    superstring-upcase!
	    superstring-upcase
	    superstring-downcase!
	    superstring-downcase
	    superstring-titlecase!
	    superstring-titlecase
	    superstring-capitalize!
	    superstring-capitalize

	    superstring-any
	    superstring-every
	    sub-superstring-to-list
	    superstring-move!
	    superstring-take
	    superstring-take-right
	    superstring-drop
	    superstring-drop-right
	    superstring-join
	    
	    superstring-pad
	    superstring-pad-right
	    superstring-trim
	    superstring-trim-right
	    superstring-trim-both

	    superstring-compare
	    superstring-compare-ci

	    superstring-split

	    superstring-unfold
	    superstring-unfold-right

	    superstring-filter
	    superstring-replace
	    superstring-delete
	    superstring-tokenize
	    superstring-tabulate

	    superstring-prefix-length
	    superstring-prefix-length-ci
	    superstring-suffix-length
	    superstring-suffix-length-ci

	    superstring-prefix?
	    superstring-prefix-ci?
	    superstring-suffix?
	    superstring-suffix-ci?

	    superstring-index
	    superstring-index-right
	    superstring-skip
	    superstring-skip-right

	    superstring-count
	    superstring-contains
	    superstring-contains-ci

	    superstring-reverse!
	    superstring-reverse
	    ))
	    
#|
Copy of guile's string functionality supervecor style
|#

;; Wrappers so that we can quickly move to the standard string utility if
;; we are dealing with plain guile strings
(define-syntax define-sup
  (lambda (x)
    (syntax-case x ()
      ((_ (s ...) (name args ...) code ...)
       (with-syntax ((supname (datum->syntax #'name (string->symbol
						     (string-append
						      "super"
						      (symbol->string
						       (syntax->datum
							#'name)))))))
	  #'(define (supname args ...)
	      (if (and ((@ (guile) string?) s) ...)
		  ((@ (guile) name) args ...)
		  (let ()
		    code ...))))))))

(define-syntax define-sup*
  (lambda (x)
    (syntax-case x ()
      ((_ (s ...) (name args ...) code ...)
       (with-syntax ((supname (datum->syntax #'name (string->symbol
						     (string-append
						      "super"
						      (symbol->string
						       (syntax->datum
							#'name)))))))
	 #'(define (supname . l)
	     (apply
	      (lambda* (args ...)
		 (if (and ((@ (guile) string?) s) ...)
		     (apply (@ (guile) name) l)
		     (let ()
		       code ...)))
	      l)))))))
			  

;; LIBGUILE string.h
(define superstring? supervector?)

(define* (make-superstring m #:optional (initchar #\space)
			   #:key
			   (binsize #f)
			   (level   7) 
			   (ro?     #f))
  (cond
   ((string? m)
    (make-supervector (list->vector (map char->integer (string->list m)))
		      #:binsize    binsize
		      #:superstr?  1
		      #:resizable? 1
		      #:m          1
		      #:level      level
		      #:ro?        ro?))
   (else
    (make-supervector m (char->integer initchar)
		      #:expand?   1     ;; So that we can expand from u8 to u32
		      #:signed?   0     ;; Only positive numbers
		      #:typed?    1     ;; we will allow uniform access
		      #:level     level ;; indicates how much we will compress
		      #:m         1     ;; the init size of the char
		      #:ro?       ro?   ;; ro? will enable implicit sharing
		      #:binsize   binsize
		      #:superstr? 1
		      #:resizable? 1
		      ))))


(define* (superstring-stream f seed s
			     #:optional
			     (i1 0)
			     (j (supervector-length s))
			     (s2 s)
			     (i2 i1))
			     
  
  (supervector-stream (lambda (x seed)
			(call-with-values
			    (lambda () (f (integer->char x) seed))
			  (lambda (x seed)
			    (values (char->integer x) seed))))
		      seed s i1 s2 i2 (- j i1)))


(define* (superstring-fold f seed s
			   #:optional			   
			   (i 0)
			   (j (supervector-length s))
			   #:key
			   (dir 1))
  
  (supervector-fold (lambda (x seed) (f (integer->char x) seed))
		    seed s i (- j i) #:dir dir))

(define (superstring-append! s1 s2)
  (set! s2 (if (string? s2) (make-superstring s2) s2))
  (supervector-append! s1 (list s2)))

(define (superstring-prepend! s1 s2)
  (set! s2 (if (string? s2) (make-superstring s2) s2))
  (supervector-prepend! s1 (list s2)))

(define* (superstring-fold2 f seed s1 s2
			   #:optional
			   (i1 0)
			   (i2 0)
			   (n  #f)
			   #:key (dir1 1) (dir2 1))
  
  (supervector-fold2 (lambda (x y seed)
		       (f (integer->char x)
			  (integer->char y)
			  seed))
		     
		     seed
		     s1 s2 i1 i2
		     (if n n (min (- (supervector-length s1) i1)
				  (- (supervector-length s2) i2)))
		     #:dir1 dir1
		     #:dir2 dir2))

(define* (superstring-fold-right f seed s
				 #:optional
				 (i (supervector-length s))
				 (j 0))
  
  (supervector-fold (lambda (x seed) (f (integer->char x) seed))
		    seed s i (- i j) #:dir -1))

(define* (superstring-map f s
			  #:optional
			  (start 0)
			  (end   (superstring-length s)))
  (let ((ret (make-superstring (- end start))))
    (supervector-tr
     (lambda (x) (char->integer (f (integer->char x))))
     s start ret 0 (- end start))
    ret))

(define* (superstring-map! f s
			   #:optional
			   (start 0)
			   (end   (superstring-length s)))
  (supervector-tr
   (lambda (x) (char->integer (f (integer->char x))))
   s start s start (- end start)))

(define* (superstring-for-each f s
			       #:optional
			       (start 0)
			       (end   (superstring-length s)))
  (supervector-for-each (lambda (x) (f (integer->char x)))
			s start (- end start)))

(define* (superstring-for-each-index f s
				     #:optional
				     (start 0)
				     (end   (superstring-length s)))
  (supervector-fold (lambda (x s)
		      (f x s)
		      (+ s 1))
		    0 start (- end start)))


(define* (list->superstring l #:key
		       (binsize #f)
		       (level  7)  ;; High compression ratio fast if only latin1
		       (ro?   #f)) ;; ro? or not
  
  (make-superstring (list->vector l)
		    #:binsize binsize
		    #:level   level
		    #:ro?     ro?))


(define* (superstring-copy s #:optional (i 0) (j (supervector-length s)))
  (supervector-copy s i (- j i)))

(define* (superstring-copy! s2 i2 s1
			    #:optional (i1 0) (j1 (supervector-length s1)))
  (supervector-copy! s1 i1 s2 i2 (- j1 i1)))

(define (superstring->list s)
  (reverse! (superstring-fold cons '() s)))

(define-sup  (s) (string-length s) (supervector-length s))

(define-sup (s) (string-utf8-length s)
  (define (f code s)
    (cond
     ((<= code #x7f)
      (+ s 1))

     ((<= code #x7ff)
      (+ s 2))

     ((<= code #x7ffff)
      (+ s 3))
     ((<= code #x10ffff)
      (+ s 4))))

  (supervector-fold f 0 s))

(define-sup (s) (string-bytes-per-char s)
  (supervector-memsize s #:count-empty? #t))

(define-sup (s) (string-ref s i)
  (integer->char (supervector-ref s i)))

(define-sup (s) (superstring-set! s i v)
  (supervector-set! s i (char->integer v)))

(define (superstring-append . l)
  (define s (make-superstring (let lp ((l l) (n 0))
				(if (pair? l)
				    (lp (cdr l)
					(+ n (superstring-length (car l))))
				    n))))
  (let lp ((i 0) (l l))
    (if (pair? l)
	(let ((n (superstring-length (car l))))
	  (supervector-copy! (car l) 0 s i (superstring-length (car l)))
	  (lp (+ i n) (cdr l)))
	s)))



;;(string-normalize-nfd s)
;;(string-normalize-nfkd str)
;;(string-normalize-nfc str)
;;(string_normalize_nfkc str)

;; scheme/r5rs
(define-syntax-rule (mk-test string=? char=)
  (define (string=? x y . l)
    (define (f x y)
      (let/ec out
        (superstring-fold2
	 (lambda (x y s)
	   (when (not  (char= x y))
	     (out #f))
	   #t)
	 #t
	 x y)))

    (aif it (f x y)
	 (if (pair? l)
	     (apply string=? y l)
	     it)
	 #f)))
     
(mk-test superstring=?     char=?)
(mk-test superstring<?     char<?)
(mk-test superstring<=?    char<=?)
(mk-test superstring>?     char>?)
(mk-test superstring>=?    char>=?)
(mk-test superstring-ci=?  char-ci=?)
(mk-test superstring-ci<?  char-ci<?)
(mk-test superstring-ci<=? char-ci<=?)
(mk-test superstring-ci>?  char-ci>?)
(mk-test superstring-ci>=? char-ci>=?)


(define-sup* (s) (string-sub s start #:optional (end (superstring-length s)))
  (supervector-copy s start (- end start)))


(define-sup* (s) (string-fill! s ch
			       #:optional
			       (start 0)
			       (end   (superstring-length s)))
  
  (let ((s0 (make-superstring (- end start) ch)))
    (supervector-copy! s0 0 s start (- end start))))


;;number->string string->number
;;symbol->string string->symbol

;;rnrs/unicode.h
(define-sup (s) (string-upcase! s)
  (superstring-map! char-upcase s))

(define-sup (s) (string-upcase s)
  (superstring-map char-upcase s))

(define-sup (s) (string-downcase! s)
  (superstring-map! char-downcase s))

(define-sup (s) (string-downcase s)
  (superstring-map char-downcase s))

(define-sup (s) (string-titlecase! s)
  (superstring-stream
   (lambda (x s)
     (if (char-alphabetic? x)
	 (if s
	     (values (char-titlecase x) #f)
	     (values x                  #f))
	 (values x #t)))
   #t s))

(define-sup (s) (string-titlecase s)
  (let ((ss (superstring-copy s)))
    (superstring-titlecase! ss)
    ss))

(define-sup (s) (string-capitalize! s)
  (superstring-stream
   (lambda (x s)
     (if (char-alphabetic? x)
	 (if s
	     (values (char-upcase x) #f)
	     (values x               #f))
	 (values x #t)))
   #t s))

(define-sup (s) (string-capitalize s)
  (let ((ss (superstring-copy s)))
    (superstring-capitalize! ss)
    ss))

(define-sup  (s) (superstring-null? s) (= 0 (superstring-length s)))


(define* (superstring-any pred s
			  #:optional (start 0) (end (superstring-length s)))
  
  (supervector-or-map (lambda (x) (pred (integer->char x))) s start
		      (- end start)))

(define* (superstring-every pred s
			    #:optional (start 0) (end (superstring-length s)))
  (supervector-and-map (lambda (x) (pred (integer->char x)))
		       s start (- end start)))



(define* (sub-superstring-to-list s
				  #:optional
				  (start 0)
				  (end   (superstring-length s)))
  (superstring->list (supervector-copy s start (- end start) #:ref? #t)))


(define* (superstring-join ls #:optional (delim " ") (grammar 'infix))
  (case grammar
    ((infix strict-infix)
     (cond
      ((null? ls)
       (when (eq? grammar 'strict-infix)
	 (error "strict join does not allow empty list"))
       
       "")
      
      ((= (length ls) 1)
       (car ls))

      (else
       (let* ((n  (length ls))
	      (m  (string-length delim))
	      (N  (let lp ((l ls) (i 0))
		    (if (pair? l)
			(lp (cdr l) (+ i (superstring-length (car l))))
			i)))
	      (M  (+ N (* m (- n 1))))
	      (ss (make-superstring M))
	      (a  (car ls))
	      (u  (make-superstring delim))
	      (l  (cdr ls)))
	 (supervector-copy! a 0 ss 0 (supervector-length a))
	 (let lp ((l l) (i (supervector-length a)))
	   (when (pair? l)
	     (let* ((x  (car l))
		    (nn (supervector-length x)))
	       (supervector-copy! u       0 ss i m)
	       (supervector-copy! x       0 ss (+ i m) nn)
	       (lp (cdr l) (+ i nn m)))))
	 ss))))

    ((suffix)
     (cond
      ((null? ls)
       (when (eq? grammar 'strict-infix)
	 (error "strict join does not allow empty list"))
       
       "")
      
      ((= (length ls) 1)
       (superstring-append (car ls) delim))

      (else
       (let* ((n  (length ls))
	      (m  (string-length delim))
	      (N  (let lp ((l ls) (i 0))
		    (if (pair? l)
			(lp (cdr l) (+ i (superstring-length (car l))))
			i)))
	      (M  (+ n (* m n)))
	      (ss (make-superstring M))
	      (u  (make-superstring delim)))
	 
	 (let lp ((l ls) (i 0))
	   (when (pair? l)
	     (let* ((x  (car l))
		    (nn (supervector-length x)))

	       (supervector-copy! x 0 ss i nn)
	       (supervector-copy! u 0 ss (+ i nn) m)

	       (lp (cdr l) (+ i nn m)))))
	 
	 ss))))

    ((prefix)
     (cond
      ((null? ls)
       (when (eq? grammar 'strict-infix)
	 (error "strict join does not allow empty list"))
       
       "")
      
      ((= (length ls) 1)
       (superstring-append delim (car ls)))
      
      (else
       (let* ((n  (length ls))
	      (m  (string-length delim))
	      (N  (let lp ((l ls) (i 0))
		    (if (pair? l)
			(lp (cdr l) (+ i (superstring-length (car l))))
			i)))
	      (M  (+ n (* m n)))
	      (ss (make-superstring M))
	      (u  (make-superstring delim)))
	 
	 (let lp ((l ls) (i 0))
	   (when (pair? l)
	     (let* ((x  (car l))
		    (nn (supervector-length x)))

	       (supervector-copy! u 0 ss i       m )
	       (supervector-copy! x 0 ss (+ i m) nn)	    

	       (lp (cdr l) (+ i nn m)))))
	 
	 ss))))))
	 

(define-sup (s1 s2) (string-move! s1 i1 j1 s2 i2)
  (supervector-copy! s1 i1 s2 i2 (- j1 i1)))

(define-sup (s) (string-take s n)
  (let ((ret (make-superstring n)))
    (superstring-copy! ret 0 s 0 n)
    ret))

(define-sup (s) (string-drop s n)
  (let* ((n2  (- (superstring-length s) n))
	 (ret (make-superstring n2)))
    (superstring-copy! ret 0 s n (+ n n2))
    ret))

(define-sup (s) (string-take-right s n)
  (let ((n2  (- (superstring-length s) n))
	(ret (make-superstring n)))
    (superstring-copy! ret 0 s n2 (+ n n2))
    ret))

(define-sup (s) (string-drop-right s n)
  (let* ((n2  (- (superstring-length s) n))
	 (ret (make-superstring n2)))
    (superstring-copy! ret 0 s 0 n2)
    ret))


(define-sup* (s) (string-pad s n
				  #:optional
				  (ch    #\space)
				  (start 0      )
				  (end   (supervector-length s)))
  (let* ((N1  (- end start))
	 (N2  (min n N1))
	 (m   (- n N2))
	 (i1  start)
	 (i2  m))
    (let ((ret (make-superstring N2 ch)))
      (superstring-copy! ret i2 s i1 N2)
      ret)))

(define-sup* (s)  (string-pad-right s n
				    #:optional
				    (ch    #\space)
				    (start 0      )
				    (end   (supervector-length s)))
  (let* ((N  (- end start))
	 (j1 (- N (+ start (max 0 (- n N))) 1))
	 (i1 (+ (- j1 n) 1))
	 (N  (+ 1 (- j1 start)))
	 (j2 (- n N)))
    (let ((ret (make-superstring n ch)))
      (superstring-copy! ret 0 s i1 n)
      ret)))

(define-sup* (s) (string-trim s
			      #:optional
			      (cp char-whitespace?)
			      (start 0)
			      (end   (superstring-length s)))
  (let ((n (let/ec out
	     (superstring-fold
	      (lambda (x n)
		(cond
		 ((char? cp)
		  (if (equal? x cp)
		      (+ n 1)
		      (out n)))
		 ((procedure? cp)
		  (if (cp x)
		      (+ n 1)
		      (out n)))

		 (else
		  (if (char-set-contains? cp x)
		      (+ n 1)
		      (out n)))))
	      0 s start (- end start)))))
    (superstring-drop s n)))

(define-sup* (s) (string-trim-right s
				    #:optional
				    (cp char-whitespace?)
				    (start 0)
				    (end   (superstring-length s)))
  (let ((n (let/ec out
	     (superstring-fold
	      (lambda (x n)
		(cond
		 ((char? cp)
		  (if (equal? x cp)
		      (+ n 1)
		      (out n)))
		 ((procedure? cp)
		  (if (cp x)
		      (+ n 1)
		      (out n)))
		 
		 (else
		  (if (char-set-contains? cp x)
		      (+ n 1)
		      (out n)))))
	      0 s start (- end start)
	      #:dir -1))))

    (superstring-drop-right s n)))

(define-sup* (s) (string-trim-both s
				   #:optional
				   (cp char-whitespace?)
				   (start 0)
				   (end   (superstring-length s)))
  (let ((ss (superstring-trim-right s cp start end)))
    (superstring-trim ss cp)))


   

(define-sup* (s1 s2) (string-compare s1 s2 < = >
				     #:optional
				     (start1 0)
				     (start2 0)
				     (end   (superstring-length s1)))
  (let/ec out
    (superstring-fold2
     (lambda (x1 x2 seed)
       (cond
	((char<? x1 x2)
	 (out (< x1 x2)))
	((char>? x1 x2)
	 (out (> x1 x2)))
	(else
	 (= x1 x2))))
     #f s1 s2
     start1
     start2
     (- end start1))))

(define-sup* (s1 s2) (superstring-compare-ci s1 s2 < = >
			    #:optional
			    (start1 0)
			    (start2 0)
			    (end   (superstring-length s1)))
  (let/ec out
    (superstring-fold2
     (lambda (x1 x2 seed)
       (cond
	((char-ci<? x1 x2)
	 (out (< x1 x2)))
	((char-ci>? x1 x2)
	 (out (> x1 x2)))
	(else
	 (= x1 x2))))
     #f s1 s2
     start1 start2 (- end start1))))

(define-sup (s) (string-split s chp)
  (let ((l '())
	(i0 0))
    (let ((j (superstring-fold
	      (lambda (x j)
		(cond
		 ((char? chp)
		  (when (char=? x chp)
		    (set! l  (cons (superstring-copy s i0 j) l))
		    (set! i0 (+ j 1))))
		 ((procedure? chp)
		  (when (chp x)
		    (set! l  (cons (superstring-copy s i0 j) l))
		    (set! i0 (+ j 1))))
		 (else
		  (when (char-set-contains? chp x)
		    (set! l  (cons (superstring-copy s i0 j) l))
		    (set! i0 (+ j 1)))))
		(+ j 1))
	      0 s)))
      (reverse! (cons (superstring-copy s i0 j) l)))))
		   

(define* (superstring-unfold p f g seed #:optional
			     (base  "")
			     (final (lambda (x) #f)))
  (let ((s (make-superstring base)))
    (let lp ((seed seed))
      (if (p seed)
	  (aif it (final seed)
	       (begin
		 (superstring-append! s it)
		 s)
	       s)
	  (begin
	    (superstring-append (f seed))
	    (lp (g seed)))))))

(define* (superstring-unfold-right p f g seed #:optional
				   (base  "")
				   (final (lambda (x) #f)))
  (let ((s (make-superstring base)))
    (let lp ((seed seed))
      (if (p seed)
	  (aif it (final seed)
	       (begin
		 (superstring-prepend! s it)
		 s)
	       s)
	  (begin
	    (superstring-prepend! s (f seed))
	    (lp (g seed)))))))


(define-sup* (s1 s2) (string-replace s1 s2
				     #:optional
				     (start1 0)
				     (end1   (supervector-length s1))
				     (start2 0)
				     (end2   (supervector-length s2)))
  
  (let* ((n1  (- end1 start1))
	 (n2  (- end2 start2))
	 (ret (make-supervector (+ n1 (- n2 n1)))))
    (superstring-copy! s1 0 ret 0 start1)
    (superstring-copy! s2 start2 ret start1 n2)
    (superstring-copy! s2 (+ start2 n1) ret (+ start1 n2)
		       (- (supervector-length s1) end1))
    ret))

(define-sup* (s) (string-filter chp s
				#:optional
				(start 0)
				(end   (supervector-length s)))
  (let* ((l (supervector-fold
	     (lambda (x seed)
	       (cond
		((char? chp)
		 (if (char=? chp x)
		     (cons x seed)
		     seed))
		((procedure? chp)
		 (if (chp x)
		     (cons x seed)
		     seed))
		(else
		 (if (char-set-contains? chp x)
		     (cons s seed)
		     seed))))
	     '()
	     s
	     start
	     (- end start))))
    (make-superstring (list->vector (reverse! l)))))
    
	 

(define-sup* (s) (string-delete chp s
				#:optional
				(start 0)
				(end   (supervector-length s)))
  (let ((l 
	 (supervector-fold
	  (lambda (x seed)
	    (cond
	     ((char? chp)
	      (if (not (char=? chp x))
		  (cons x seed)
		  seed))
	     ((procedure? chp)
	      (if (not (chp x))
		  (cons x seed)
		  seed))
	     (else
	      (if (not (char-set-contains? chp x))
		  (cons x seed)
		  seed))))
	  '()
	  s	  
	  start
	  (- end start))))
    
    (make-superstring (list->vector (reverse! l)))))


(define-sup* (s) (string-tokenize s
				  #:optional
				  (set   char-set:graphic)
				  (start 0)
				  (end   (superstring-length s)))
  (let ((l  '())
	(j0  0))
    
    (let ((x
	   (superstring-fold
	    (lambda (x seed)
	      (if (char-set-contains? x set)
		  (if (car seed)
		      (cons #t (+ 1 (cdr seed)))
		      (begin
			(set! j0 seed)
			(cons #t (+ 1 (cdr seed)))))
		  (if (car seed)
		      (begin
			(set! l (cons (superstring-copy s j0 (car seed)) l))
			(cons #f (+ 1 (cdr seed))))
		      (cons #f (+ 1 (cdr seed))))))
	    (cons #f 0)
	    s
	    start
	    (- end start))))
      
      (when (car x)
	(set! l (cons (superstring-copy s j0 (cdr x)) l)))

      (reverse! l))))

(define (superstring-tabulate proc len)
  (make-superstring (string-tabulate proc len)))


(define-sup* (s1 s2) (string-prefix-length s1 s2
					   #:optional
					   (i1 0)
					   (j1 (superstring-length s1))
					   (i2 0)
					   (j2 (superstring-length s2)))
  (define n (min (- j1 i1) (- j2 i2)))

  (let/ec ret
     (superstring-fold2
      (lambda (x1 x2 seed)
	(if (char=? x1 x2)
	    (+ seed 1)
	    (ret seed)))
      0 s1 s2 i1 i2 n)))

(define-sup* (s1 s2) (string-prefix-length-ci s1 s2
					      #:optional
					      (i1 0)
					      (j1 (superstring-length s1))
					      (i2 0)
					      (j2 (superstring-length s2)))
  (define n (min (- j1 i1) (- j2 i2)))

  (let/ec ret
     (superstring-fold2
      (lambda (x1 x2 seed)
	(if (char-ci=? x1 x2)
	    (+ seed 1)
	    (ret seed)))
      0 s1 s2 i1 i2 n)))

(define-sup* (s1 s2) (string-suffix-length s1 s2
					   #:optional
					   (i1 0)
					   (j1 (superstring-length s1))
					   (i2 0)
					   (j2 (superstring-length s2)))
  (define n (min (- j1 i1) (- j2 i2)))

  (let/ec ret
     (superstring-fold2
      (lambda (x1 x2 seed)
	(if (char=? x1 x2)
	    (+ seed 1)
	    (ret seed)))
      0 s1 s2 i1 i2 n #:dir1 -1 #:dir2 -1)))

(define-sup* (s1 s2) (string-suffix-length-ci s1 s2
					      #:optional
					      (i1 0)
					      (j1 (superstring-length s1))
					      (i2 0)
					      (j2 (superstring-length s2)))
  (define n (min (- j1 i1) (- j2 i2)))

  (let/ec ret
     (superstring-fold2
      (lambda (x1 x2 seed)
	(if (char-ci=? x1 x2)
	    (+ seed 1)
	    (ret seed)))
      0 s1 s2 i1 i2 n)))

(define-sup* (s1 s2) (string-prefix? s1 s2
				         #:optional
					 (i1 0)
					 (j1 (superstring-length s1))
					 (i2 0)
					 (j2 (superstring-length s2)))

    (define n (min (- j1 i1) (- j2 i2)))

    (let/ec ret
       (superstring-fold2
	(lambda (x1 x2 seed)
	  (if (char=? x1 x2)
	      #t
	      (ret #f)))
	0 s1 s2 i1 i2 n)))

(define-sup* (s1 s2) (string-prefix-ci? s1 s2
				         #:optional
					 (i1 0)
					 (j1 (superstring-length s1))
					 (i2 0)
					 (j2 (superstring-length s2)))

    (define n (min (- j1 i1) (- j2 i2)))

    (let/ec ret
       (superstring-fold2
	(lambda (x1 x2 seed)
	  (if (char-ci=? x1 x2)
	      #t
	      (ret #f)))
	0 s1 s2 i1 i2 n)))


(define-sup* (s1 s2) (string-suffix? s1 s2
				         #:optional
					 (i1 0)
					 (j1 (superstring-length s1))
					 (i2 0)
					 (j2 (superstring-length s2)))

    (define n (min (- j1 i1) (- j2 i2)))

    (let/ec ret
       (superstring-fold2
	(lambda (x1 x2 seed)
	  (if (char=? x1 x2)
	      #t
	      (ret #f)))
	0 s1 s2 i1 i2 n #:dir1 -1 #:dir2 -1)))


(define-sup* (s1 s2) (string-suffix-ci? s1 s2
				         #:optional
					 (i1 0)
					 (j1 (superstring-length s1))
					 (i2 0)
					 (j2 (superstring-length s2)))

    (define n (min (- j1 i1) (- j2 i2)))

    (let/ec ret
       (superstring-fold2
	(lambda (x1 x2 seed)
	  (if (char-ci=? x1 x2)
	      #t
	      (ret #f)))
	0 s1 s2 i1 i2 n #:dir1 -1 #:dir2 -1)))


(define-syntax-rule (mk-ind string-index ID DIR)
  (define-sup* (s) (string-index s chpred
				 #:optional
				 (start 0)
				 (end   (superstring-length s)))
    (let/ec ret
      (superstring-fold
       (lambda (x s)
	 (cond
	  ((char? chpred)
	   (if (ID (char=? x chpred))
	       (ret s)
	       (+ s 1)))

	  ((char-set? chpred)
	   (if (ID (char-set-contains? chpred x))
	       (ret s)
	       (+ s 1)))

	  (else
	   (if (ID (chpred x))
	       (ret s)
	       (+ s 1)))))
       0 s start (- end start) #:dir DIR))))

(define (id x) x)
(mk-ind string-index       id   1)
(mk-ind string-index-right id  -1)
(mk-ind string-skip        not  1)
(mk-ind string-skip-right  not -1)

(define-sup* (s) (string-count s chpred
			       #:optional
			       (start 0)
			       (end   (superstring-length s)))
  (let/ec ret
    (superstring-fold
     (lambda (x s)
       (cond
	((char? chpred)
	 (if (char=? x chpred)
	     (+ s 1)
	     s))
	
	((char-set? chpred)
	 (if (char-set-contains? chpred x)
	     (+ s 1)
	     s))
	(else
	 (if (chpred x)
	     (+ s 1)
	     s))))
       0 s start (- end start))))

(define-syntax-rule (mk-contains string-conatins char=?)
  (define-sup* (s1 s2) (string-contains? s1 s2
					 #:optional
					 (i1 0)
					 (j1 (superstring-length s1))
					 (i2 0)
					 (j2 (superstring-length s2)))

    (define n1 (- j1 i1))
    (define n2 (- j2 i2))

    (let/ec ret
      (superstring-fold
       (lambda (x i)
	 (if (< (- j1 i) n2)
	     (ret #f)
	     (if (let/ec ret2
	  	   (superstring-fold2
		    (lambda (x1 x2 s)
		      (if (char=? x1 x2)
			  #t
			  (ret2 #f)))
		    #f s1 s2 i n2))
		 (ret #t)
		 (+ i 1))))
       #f s1 0 n1))))

(mk-contains string-contains    char=?   )
(mk-contains string-contains-ci char-ci=?)

(define-sup* (s) (string-reverse! s #:optional (i #f) (j #f))
  (if (or i j)
      (begin
	(set! i (if i i 0))
	(set! j (if j j (- (supervector-length s) i)))
	(let ((ss (supervector-copy s i (- j i) #:ref? #t)))
	  (supervector-reverse! ss)
	  (supervector-copy! ss 0 s i (- j i) #:ref? #t)))
      (supervector-reverse! s)))

(define-sup* (s) (string-reverse s #:optional (i #f) (j #f))
  (set! i (if i i 0))
  (set! j (if j j (- (supervector-length s) i)))
  (let ((ss (supervector-copy s)))
    (superstring-reverse! ss i (- j i))))

(define (superstring-append-shared . l)
  (define s (make-superstring (let lp ((l l) (n 0))
				(if (pair? l)
				    (lp (cdr l)
					(+ n (superstring-length (car l))))
				    n))))
  (let lp ((i 0) (l l))
    (if (pair? l)
	(let ((n (superstring-length (car l))))
	  (supervector-copy! (car l) 0 s i
			     (superstring-length (car l)) #:ref? #t)
	  (lp (+ i n) (cdr l)))
	s)))

