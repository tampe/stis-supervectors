;;HACK
(define-syntax-rule (define-module- name args ...)
  (cond-expand
   (guile-3
    (define-module name
      #:declarative? #f
      args ...))
   (guile
    (define-module name args ...))))


(define-module- (ice-9 supervectors algebraic)
  #:use-module (ice-9 supervectors functional)
  #:use-module (ice-9 supervectors core)
  #:use-module (ice-9 supervectors macros)
  #:use-module (ice-9 supervectors copy)
  #:use-module (ice-9 supervectors typed)
  
  #:export ( supervector-sum
	     supervector-min
	     supervector-max
	     
	     supervector-add!
	     supervector-sub!
	     supervector-mul!
	     supervector-div!

	     supervector-add
	     supervector-sub
	     supervector-mul
	     supervector-div

	     supervector-modulo!
	     supervector-quotient!

	     supervector-modulo
	     supervector-quotient))

(define (dist x y) (> x y) (- x y) (- y x))

(define (len s1 s2)
  (if (number? s1)
      (if (number? s2)
	  1
	  (supervector-length s2))
      (if (number? s2)
	  (supervector-length s1)
	  (min (supervector-length s1) (supervector-length s2)))))

(define* (supervector-sum s #:optional (i 0) (n #f))
  (set! n (if n n (supervector-length s)))
  
  (supervector-chunk-fold
   ;; for the chunk
   (lambda (b i j d seed)
     (define N (+ 1 (dist i j)))
     (define x (d-ref  b))
     (+ (* x N) seed))
       
   ;; for the vector
   (lambda (b i j d seed)
     (let ((v (v-ref b)))
       (standard-loop-1
	#f (bits-ref b) i j d #f (get-reffer)
	(lambda (ref k)
	  (set! seed (+ (ref v k) seed)))))
     seed)
	  
   ;; For the bug
   (lambda x (error "BUG"))
       
   0 s i n))

(define* (supervector-min s #:optional (i 0) (n #f))
  (set! n (if n n (supervector-length s)))

  (if (= n 0)
      (inf)      
      (supervector-chunk-fold
       ;; for the chunk
       (lambda (b i j d seed)
	 (define x (d-ref  b))
	 (min x seed))
       
       ;; for the vector
       (lambda (b i j d seed)
	 (let ((v (v-ref b)))
	   (standard-loop-1
	    #f (bits-ref b) i j d #f (get-reffer)
	    (lambda (ref k)
	      (set! seed (min (ref v k) seed)))))
	 seed)
	  
       ;; For the bug
       (lambda x (error "BUG"))
       
       (supervector-ref s i) s i n)))

(define* (supervector-max s #:optional (i 0) (n #f))
  (set! n (if n n (supervector-length s)))

  (if (= n 0)
      (- (inf))
      (supervector-chunk-fold
       ;; for the chunk
       (lambda (b i j d seed)
	 (define x (d-ref  b))
	 (max x seed))
       
       ;; for the vector
       (lambda (b i j d seed)
	 (let ((v (v-ref b)))
	   (standard-loop-1
	    #f (bits-ref b) i j d #f (get-reffer)
	    (lambda (ref k)
	      (set! seed (max (ref v k) seed)))))
	 seed)
	  
       ;; For the bug
       (lambda x (error "BUG"))
       
       (supervector-ref s i) s i n)))

  
(define* (supervector-add! s1 s2 #:optional (i1 0) (i2 0) (n #f))
  (set! n (if n n (len s1 s2)))
  
  (cond
   ((and (number? s1) (number? s2))
    (+ s1 s2))
   
   ((number? s1)
    (supervector-unary
     (lambda (x) (+ x s1))
     
     s2 i1 n

     #:ctrl
     (lambda x (values add s1))))
   
   ((number? s2)
    (supervector-add! s2 s1 i2 i1 n))
   
   (else
    (supervector-binary
     +
     (lambda (x)
       (values add x))
   
     (lambda (x)
       (values add x))
     +
     s1 i1 s2 i2 n
     #:flat? #t))))

(define* (supervector-sub! s1 s2 #:optional (i1 0) (i2 0) (n #f))
  (set! n (if n n (len s1 s2)))
  
  (cond
   ((and (number? s1) (number? s2))
    (- s1 s2))
   
   ((number? s2)
    (supervector-unary
     (lambda (x) (- x s2))
     s1 i1 n
     #:ctrl
     (lambda x (values add (- s2)))))
   
   ((number? s1)
    (supervector-mul! -1 s2)
    (supervector-add! s1 s2))
   
   (else
    (supervector-binary
     -
     (lambda (x)
       (if (= x 0)
	   (values scale -1)
	   (values #f     0)))
     
     (lambda (x)
       (values add (- x)))     
     -   
     s1 i1 s2 i2 n
     #:flat? #t))))

(define* (supervector-mul! s1 s2 #:optional (i1 0) (i2 0) (n #f))
  (set! n (if n n (len s1 s2)))

  (cond
   ((and (number? s1) (number? s2))
    (* s1 s2))
   
   ((number? s2)
    (supervector-unary
     (lambda (x) (* x s2))
     s1 i1 n
     #:ctrl (lambda x (values scale s2))))
   
   ((number? s1)
    (supervector-mul s2 s1))

   (else
    (supervector-binary
     *
     (lambda (x)
       (if (= x 0)
	   (values replace 0)
	   (values scale   x)))
     (lambda (x)
       (if (= x 0)
	   (values replace 0)
	   (values scale   x)))
     *
     s1 i1 s2 i2 n))))

(define (revdiv x y) (/ y x))

(define* (supervector-div! s1 s2 #:optional (i1 0) (i2 0) (n #f))
  (set! n (if n n (len s1 s2)))

  (cond
   ((and (number? s1) (number? s2))
    (/ s1 s2))
   
   ((number? s2)
    (supervector-unary
     (lambda (x) (/ x s2))
     s1 i1 n
     #:ctrl
     (lambda x (values scale (/ 1 s2)))))

   ((number? s1)
    (supervector-unary
     (lambda (x) (/ s1 x))
     s2 i2 n))
  
   (else
    (supervector-binary
     /
     (lambda (x)
       (if (= x 0)
	   (values replace 0)
	   (values #f 0)))
     (lambda (x)
       (if (= x 1)
	   (values nothing 0)
	   (values scale (/ 1 x))))
     /
     s1 i1 s2 i2 n))))
  
(define-syntax-rule (mk supervector-add supervector-add!)
  (define* (supervector-add s1 s2 #:optional (i1 0) (i2 0) (n #f))
    (if (number? s2)
	(if (number? s1)
	    (values)
	    (set! s1 (supervector-copy s1)))
	(set! s2 (supervector-copy s2)))

    (supervector-add! s1 s2 i1 i2 n)
    (if (number? s2)
	s1
	s2)))

(mk supervector-add supervector-add!)
(mk supervector-mul supervector-mul!)
(mk supervector-sub supervector-sub!)
(mk supervector-div supervector-div!)

(define* (supervector-modulo! s y #:optional (i 0) (n #f))
  (set! n (if n n (supervector-length s)))
  
  (supervector-unary
   (lambda (x) (modulo x y))
   s i n
   #:ctrl
   (lambda (x)
     (if (< x y)
	 (values nothing 0)
	 (values #f      0)))))

(define* (supervector-quotient! s y #:optional (i 0) (n #f))
  (set! n (if n n (supervector-length s)))
  
  (if (= y 1)
      (values)
      (supervector-unary
       (lambda (x) (quotient x y))
       s i n)))
