;;HACK
(define-syntax-rule (define-module- name args ...)
  (cond-expand
   (guile-3
    (define-module name
      #:declarative? #f
      args ...))
   (guile
    (define-module name args ...))))


(define-module- (ice-9 supervectors c)
  #:use-module (ice-9 supervectors typeinfo)
  #:use-module (ice-9 supervectors core)
  #:export (loop-logand
	    loop-logior
	    loop-logxor
	    loop-logclear
	    loop-copy
	    loop-set-plus
	    
	    loop-logand1
	    loop-logior1
	    loop-logxor1
	    loop-logclear1
	    loop-copy1
	    loop-set-plus1
	    
	    loop-masked-logand
	    loop-masked-logior
	    loop-masked-logxor
	    loop-masked-logclear
	    loop-masked-copy
	    loop-masked-set-plus
	    
	    loop-masked-logand1
	    loop-masked-logior1
	    loop-masked-logxor1
	    loop-masked-logclear1
	    loop-masked-copy1
	    loop-masked-set-plus1
	    
	    loop-general-copy
	    ))
  
(catch #t
  (lambda ()
    (load-extension "libstis-supervectors" "supervector_init"))
  (lambda x
    (pk x)
    (backtrace)
    (let ((file  
           (%search-load-path "src/.libs/libstis-supervectors.so")))
      (if 
       file
       (catch #t
         (lambda ()          
           (load-extension file "supervector_init"))
         (lambda x
           (warn
            "libstis-supervector is not loadable!")))
       (warn 
        "libstis-supervector is not present, did you forget to make it?")))))

(supervector-constants
 (vector
    i-invert  
    i-negate  
    i-scm     
    i-sign
    i-end     
    i-algebra 
    i-native
    i-float
    <uq>))

(define (loop-general-copy do a1 b1 c1 d1 a2 b2 cd d2)
  (super-bits-copy a1 b1 c1 d1 a2 b2 cd d2 do))

(define (loop-logand do a1 b1 c1 d1 a2 b2 cd d2)
  (super-bits-binary a1 b1 c1 d1 a2 b2 cd d2 do 0))

(define (loop-logior do a1 b1 c1 d1 a2 b2 cd d2)
  (super-bits-binary a1 b1 c1 d1 a2 b2 cd d2 do 1))

(define (loop-logxor do a1 b1 c1 d1 a2 b2 cd d2)
  (super-bits-binary a1 b1 c1 d1 a2 b2 cd d2 do 2))

(define (loop-logclear do a1 b1 c1 d1 a2 b2 cd d2)
  (super-bits-binary a1 b1 c1 d1 a2 b2 cd d2 do 3))

(define (loop-copy do a1 b1 c1 d1 a2 b2 cd d2)
  (super-bits-binary a1 b1 c1 d1 a2 b2 cd d2 do 4))

(define (loop-set-plus do a1 b1 c1 d1 a2 b2 cd d2)
  (super-bits-binary a1 b1 c1 d1 a2 b2 cd d2 do 6))



(define (loop-logand1 do a1 a2 b2 cd d2)
  (super-bits-binary1 a1 a2 b2 cd d2 do 0))

(define (loop-logior1 do a1 a2 b2 cd d2)
  (super-bits-binary1 a1 a2 b2 cd d2 do 1))

(define (loop-logxor1 do a1 a2 b2 cd d2)
  (super-bits-binary1 a1 a2 b2 cd d2 do 2))

(define (loop-logclear1 do a1 a2 b2 cd d2)
  (super-bits-binary1 a1 a2 b2 cd d2 do 3))

(define (loop-copy1 do a1 a2 b2 cd d2)
  (super-bits-binary1 a1 a2 b2 cd d2 do 4))

(define (loop-set-plus1 do a1 a2 b2 cd d2)
  (super-bits-binary1 a1 a2 b2 cd d2 do 6))






(define (loop-masked-logand . l)
  (lambda (do a1 b1 c1 d1 a2 b2 c2 d2)
    (super-bits-masked-binary a1 b1 c1 d1 a2 b2 c2 d2 (cons do l) 0)))

(define (loop-masked-logior . l)
  (lambda (do a1 b1 c1 d1 a2 b2 c2 d2)
    (super-bits-masked-binary a1 b1 c1 d1 a2 b2 c2 d2 (cons do l) 1)))

(define (loop-masked-logxor . l)
  (lambda (do a1 b1 c1 d1 a2 b2 c2 d2)
    (super-bits-masked-binary a1 b1 c1 d1 a2 b2 c2 d2 (cons do l) 2)))

(define (loop-masked-logclear . l)
  (lambda (do a1 b1 c1 d1 a2 b2 c2 d2)
    (super-bits-masked-binary a1 b1 c1 d1 a2 b2 c2 d2 (cons do l) 3)))

(define (loop-masked-copy . l)
  (lambda (do a1 b1 c1 d1 a2 b2 c2 d2)
    (super-bits-masked-binary a1 b1 c1 d1 a2 b2 c2 d2 (cons do l) 4)))

(define (loop-masked-set-plus . l)
  (lambda (do a1 b1 c1 d1 a2 b2 c2 d2)
    (super-bits-masked-binary a1 b1 c1 d1 a2 b2 c2 d2 (cons do l) 6)))






(define (loop-masked-logand1 . l)
  (lambda (do a1 a2 b2 cd d2)
    (super-bits-masked-binary1 a1 a2 b2 cd d2 (cons do l) 0)))

(define (loop-masked-logior1 . l)
  (lambda (do a1 a2 b2 cd d2)
    (super-bits-masked-binary1 a1 a2 b2 cd d2 (cons do l) 1)))

(define (loop-masked-logxor1 . l)
  (lambda (do a1 a2 b2 cd d2)
    (super-bits-masked-binary1 a1 a2 b2 cd d2 (cons do l) 2)))

(define (loop-masked-logclear1 . l)
  (lambda (do a1 a2 b2 cd d2)
    (super-bits-masked-binary1 a1 a2 b2 cd d2 (cons do l) 3)))

(define (loop-masked-copy1 . l)
  (lambda (do a1 a2 b2 cd d2)
    (super-bits-masked-binary1 a1 a2 b2 cd d2 (cons do l) 4)))

(define (loop-masked-set-plus1 . l)
  (lambda (do a1 a2 b2 cd d2)
    (super-bits-masked-binary1 a1 a2 b2 cd d2 (cons do l) 6)))



