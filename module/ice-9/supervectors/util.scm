;;HACK
(define-syntax-rule (define-module- name args ...)
  (cond-expand
   (guile-3
    (define-module name
      #:declarative? #f
      args ...))
   (guile
    (define-module name args ...))))


(define-module- (ice-9 supervectors util)
  #:use-module (ice-9 supervectors core)
  #:use-module (ice-9 supervectors typeinfo)
  #:use-module (rnrs bytevectors)
  #:use-module (ice-9 supervectors functional)
  #:export (supervector-memsize
	    supervector->list
	    supervector-count))

(define (supervector->list s)
  (reverse! (supervector-fold cons '() s)))

(define* (supervector-memsize s #:key (count-empty? #f))
  (define w- (w-ref s))
  (define w+ (w+ref s))

  (define n1 (vector-length w-))
  (define n2 (vector-length w+))

  (define (mem bin)
    (let ((v (v-ref bin)))
      (if v
	  (if (bytevector? v)
	      (bytevector-length v)
	      (supervector-memsize v #:count-empty? count-empty?))
	  (if count-empty?
	      (* (get-m (bits-ref bin)) (sz-ref bin))))))
  
  (let lp ((i 0) (size 0))
    (if (< i n1)
	(let ((bin (vector-ref w- i)))
	  (lp (+ i 1) (+ (mem bin) size)))
	(let lp ((i 0) (size size))
	  (if (< i n2)
	      (let ((bin (vector-ref w+ i)))
		(lp (+ i 1) (+ (mem bin) size)))
	      size)))))

(define (supervector-count pred s)
  (supervector-op
   (lambda (x s)
     (if (pred x)
	 (+ s 1)
	 s))
   
   (lambda (x n seed)
     (if (pred x)
	 (+ n seed)
	 seed))
   0
   s))

