;;HACK
(define-syntax-rule (define-module- name args ...)
  (cond-expand
   (guile-3
    (define-module name
      #:declarative? #f
      args ...))
   (guile
    (define-module name args ...))))


(define-module- (ice-9 supervectors iterator)
  #:use-module (ice-9 supervectors core)
  #:use-module (ice-9 supervectors typeinfo)
  #:use-module (ice-9 supervectors basic)
  #:use-module (ice-9 supervectors macros)
  #:use-module (ice-9 supervectors make)
  #:use-module (rnrs bytevectors)
  #:use-module (ice-9 match)
  #:use-module (ice-9 control)

  #:export
  (supervector-chunk-init
   supervector-chunk-next
   ))


(define-inlinable (dist x y) (if (> x y) (- x y) (- y x)))

;; s     : supervector
;; dir   : 1 = left->ight, -1 = right -> left
;; start : start index
;; end   : end index, #f = use end of supervector
(define* (supervector-chunk-init s #:optional (start 0) (end #f) #:key (dir 1))
  (define n1 (n-ref s))
  (define n2 (n+ref s))
  (define n  (if (> dir 0)
		 (if end end (+ n1 n2))
		 start))
  (define n- (if (> dir 0) (min n n1) (min n n2)))
  (define n+ (max 0 (- (min n (+ n1 n2)) n-)))
  
  (set! end (if end end (if (> dir 0) (+ n1 n2) 0)))

  (if (> dir 0)
      (list
       start
       (vector -1 (min n- n)        n- (w-ref s) (sbz-ref s) s)
       (vector  1 (min (+ n- n+) n) n- (w+ref s) (sbz-ref s) s))
      
      (list
       end
       (vector -1 (min n- n)        n- (w+ref s) (sbz-ref s) s)
       (vector  1 (min (+ n- n+) n) n- (w-ref s) (sbz-ref s) s))))


(define (supervector-count-for-combine q s c d i dir)
  (let ((n-      (n-ref s))
	(n+      (n+ref s))
	(binsize (sbz-ref s)))
    
    (if (> dir 0)
	(if (>= i n-)
	    (let* ((i (- i n-))
		   (I (quotient i binsize))
		   (i (modulo   i binsize)))
	      (maybe-combine-bin0 q d I i dir (w+ref s)))
	    
	    (let* ((ii (index i -1 n-))
		   (I  (quotient ii binsize))
		   (ii (modulo   ii binsize)))
	      (call-with-values
		  (lambda () (maybe-combine-bin0 q d I ii -1 (w-ref s)))
		(lambda (sh1 n1 c1)
		  (if (= n1 (- n- i))
		      (let* ((i (- i n-))
			     (I (quotient i binsize))
			     (i (modulo   i binsize)))
			(call-with-values
			    (lambda () (maybe-combine-bin0 q d I i 1 (w+ref s)))
			  (lambda (sh2 n2 c2)
			    (cond
			     ((eq? c1 q)
			      (values sh2 n2 c2))
			     ((eq? c2 q)
			      (values sh1 n1 c1))
			     ((equal? c1 c2)
			      (values (or sh1 sh2) (+ n1 n2) c1))
			     (else
			      (values sh1 n1 c1))))))
		      (values sh1 n1 c1))))))
	
	(if (< i n-)
	    (let* ((i (index i -1 binsize))
		   (I (quotient i binsize))
		   (i (modulo   i binsize)))
	      (maybe-combine-bin0 q d I i 1 (w-ref s)))
	    
	    (let* ((ii (- i n-))
		   (I  (quotient ii binsize))
		   (ii (modulo   ii binsize)))
	      (call-with-values
		  (lambda () (maybe-combine-bin0 q d I ii -1 (w+ref s)))
		(lambda (sh1 n1 c1)
		  (if (= n1 (- n- i))
		      (let* ((i (- i n-))
			     (I (quotient i binsize))
			     (i (modulo   i binsize)))
			(call-with-values
			    (lambda () (maybe-combine-bin0
					q d I i -1 (w-ref s)))
			  (lambda (sh2 n2 c2)
			    (cond
			     ((eq? c1 q)
			      (values sh2 n2 c2))
			     ((eq? c2 q)
			      (values sh1 n1 c1))
			     ((equal? c1 c2)
			      (values (or sh1 sh2) (+ n1 n2) c1))
			     (else
			      (values sh1 n1 c1))))))
		      (values sh1 n1 c1)))))))))
	   

(define (maybe-combine-bin-and-match-size d I1 i0 dir w)
  (define b (vector-ref w I1))
  (if (and d (>= d 8) (if (> dir 0) (< (- (sz-ref b) i0) d) (< i0 d)))
      (call-with-values (lambda () (maybe-combine-bin0 (list 'q) d I1 i0 dir w))
	(lambda (shared? nn c)
	  (if (and (>= nn d) (not shared?))
	      (mk-bin nn c nn
		      (bits-ref b))
	      b)))
      b))

(define (maybe-combine-bin0 q d I1 i0 dir w)
  (define b (vector-ref w I1))
  (let lp ((dir dir)
	   (c   q)
	   (n   0)
	   (I   I1)
	   (i   i0)
	   (b   b)
	   (v   (v-ref b)))
    
    (define (ret sh? nn c)
      (cond
       ((>= (+ nn n) d)
	(values sh? (+ nn n) c))
	 
       ((and I (> nn 0) (< (+ I 1) (vector-length w)))
	(let* ((I (+ I 1))
	       (b (vector-ref w I))
	       (v (v-ref  b)))
	  (if (bin? b)
	      (lp dir c (+ n nn) (+ I 1) 0 b v)
	      (values sh? nn c))))
       
       (else
	(values sh? nn c))))

    (cond
     ((not v)
      (let ((cc (d-ref b))
	    (nn (if (> dir 0) (- (sz-ref b) i) (+ i 1))))
	(if (eq? c q)
	    (ret #f nn cc)
	    (if (equal? c cc)
		(ret #f nn cc)
		(ret #f 0  cc)))))

     ((bin? v)
      (let* ((j (j-ref n))
	     (d (get-dir b))	   
	     (i (if (> d 0) (+ i j) (- (sz-ref b) i 1)))
	     (d (* d dir)))
	(call-with-values (lambda () (lp d c n #f i v (v-ref v)))
	  (lambda (shared? n cc)
	    (set! shared? (or shared? (not (read-only? b))))
	    (when (not shared?)
	      (when (and (= i 0) (= n (sz-ref b)))
		(v-set! b #f)
		(d-set! b cc)
		(clear-constant-data b)))
	    (ret shared? n cc)))))
     
     ((supervector? v)
      (let ()
	(define N (supervector-length v))
	
	(aif (shared? nn cc) (supervector-count-for-combine
			      q v c d i dir)
	     (ret #t nn cc)
	     (begin
	       (when (and (= i 0) (= nn (supervector-length v)))
		 (v-set! b #f)
		 (d-set! b cc)
		 (clear-constant-data b))
	       (ret #f nn cc)))))

     (else
      (let* ((nn  0)
	     (d   (get-dir (bits-ref b)))
	     (j   (j-ref   b))
	     (i   (if (> d 0) (+ j i) (- j i)))
	     (d   (* d dir))
	     (j   (if (> d 0) (- (sz-ref b) 1) 0))
	     (nnn (let/ec out
		    (standard-loop-1 #t
		      (bits-ref b) i j d #f (get-reffer #:b b)
		     (lambda (ref k)
		       (if (eq? c q)
			   (begin
			     (set! c (ref v k))
			     (set! nn (+ nn 1)))
			   (if (equal? c (ref v k))
			       (set! nn (+ nn 1))
			       (out nn))))))))
	
	(when (and (= i 0) (= nnn (sz-ref b)))
	  (v-set! b #f)
	  (d-set! b c)
	  (clear-constant-data b))

	(ret #f nnn c))))))

     	  
  
(define* (supervector-chunk-next iter #:key (minsize 1) (d #f))
  (match iter
   ((i . (and l (data . u)))
    (let ((n (vector-ref data 1)))
      (if (< i n)
	  (let* ((dir  (vector-ref data 0))
		 (n-   (vector-ref data 2))
		 (w    (vector-ref data 3))
		 (size (vector-ref data 4))
		 (i0   (index i dir n-))
		 (i1   (modulo   i0 size))	       
		 (I1   (quotient i0 size))
		 (bin  (maybe-combine-bin-and-match-size d I1 i1 dir w))
		 
		 (size (sz-ref bin))
		 (bits (bits-ref bin))
		 (di2  (get-dir bits))
		 (di3  (* dir di2))
		 
		 (i2   (index i1 di2 size))
		 (j2   (if (> di3 0)
			   (- (+ i2 (min (- size i2) (- n i))) 1)
			   (- i2 (- (min (+ 1 i2) (- n i)) 1))))
		 
		 (in   (+ (dist i2 j2) 1))
		 (in2  (let ((in2 (max in minsize)))
			 (when (> in2 in)
			   (let* ((s     (vector-ref data 5))
				  (ntot (+ (n+ref s) (n-ref s)))
				  (i    (index i dir ntot)))
			     (set! size (+ size (- in2 in)))
			     (if (> di3 0)
				 (begin
				   (set! i2 i)
				   (set! j2 (+ i2 size)))
				 (begin
				   (set! i2 i)
				   (set! j2 (- i2 size))))))
			 in2))
		 (d    di3))

	    ;; cow pages is referenses that when we write we will copy the data
	    (let lp ((bin bin) (dir di2) (ii i2) (jj j2))
	      (let ((v (v-ref bin)))
		(if (and (bin? v) (not (read-only? v)))
		    (let* ((j   (j-ref bin))
			   (d   (get-dir (bits-ref bin)))
			   (d   (* d dir))
			   (ii1 (if (> d 0) (+ j ii) (- j ii)))
			   (jj1 (if (> d 0)
				    (+ ii1  (- in 1))
				    (- ii1  in 1))))
		      (lp v d ii1 jj1))

		    (if (> in2 in)
			;; At an interface - use slow interface ret supervector
			(values (vector-ref data 5) i i2 j2 1 l)
			(values bin                 i ii jj d l))))))

	  
	  (if (pair? u)
	      (supervector-chunk-next (cons i u))
	      (values #f #f #f #f #f #f)))))))
	   
