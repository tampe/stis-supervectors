;;HACK
(define-syntax-rule (define-module- name args ...)
  (cond-expand
   (guile-3
    (define-module name
      #:declarative? #f
      args ...))
   (guile
    (define-module name args ...))))


(define-module- (ice-9 supervectors copy      )
  #:use-module (ice-9 supervectors core      )
  #:use-module (ice-9 supervectors typeinfo  )
  #:use-module (ice-9 supervectors macros    ) 
  #:use-module (ice-9 supervectors basic     )
  #:use-module (ice-9 supervectors expand    )
  #:use-module (ice-9 supervectors make      )
  #:use-module (ice-9 supervectors functional)
  #:use-module (ice-9 supervectors c         )
  #:use-module (rnrs bytevectors)
  #:export (supervector-copy!
	    supervector-copy	    
	    test-supervector-copy))

(define-inlinable (dist x y) (if (> x y) (- x y) (- y x)))

(define mask (logior m-mask i-scm i-float i-end))
		      
(define-inlinable (bits-ok-for-copy bits1 bits2)
  (and (= (get-bit bits1 i-invert)  0)
       (= (get-bit bits1 i-negate)  0)
       (= (get-bit bits1 i-algebra) 0)
       (= (logand mask bits1)
	  (logand mask bits2))))
	       
(define-inlinable (bits-ok-for-transfer bits1 bits2)
  (or (= bits1 bits2)
      (= (logand (lognot (logior l-mask e-mask)) bits1)
	 (logand (lognot (logior l-mask e-mask)) bits2))
      (or (> (get-bit bits2 i-scm) 0)
	  (and (<= (get-m bits1) (get-m bits2))
	       (or (and (> (get-bit bits2 i-float) 0)
			(= (get-bit bits1 i-scm)   0))
		   
		   (and (= 0 (get-bit bits1 (logior i-float i-scm)))
			(or (< (get-m bits1) (get-m bits2))
			    (= (get-bit bits2 i-sign)
			       (get-bit bits2 i-sign)))))))))

(define* (supervector-copy! s1 i1 s2 i2 n
			    #:key
			    (swap?   #f)
			    (ref?    #f)
			    (cfkn    loop-general-copy)
			    (cfkn1   #f)
			    (cfkn2   #f)
			    (k1      0)
			    (k2      0))
  (define f-empty11
    (lambda (b1 i1 j1 d1 b2 i2 j2 d2)      
      (define n2 (+ 1 (dist i2 j2)))
      (define N2 (sz-ref b2))
      (define x (d-ref b1))
      
      (call-with-values (lambda () (vv-ref (v-ref b2) i2 j2 d2))
	(lambda (v i2 j2 d2)
	  (if (and (= n2 N2) (= k1 0) (= k2 0))
	      (begin
		(clear-constant-data b2)
		(v-set! b2 #f)
		(d-set! b2 x)
		#f)
	  
	      (let ((x (d-ref b1)))
		(if (fix-cow b2 i2 j2)
		    #t
		    (begin
		      (if cfkn1
			  (cfkn1 #t x b2 i2 j2 d2)			  
			  
			  (standard-loop-1 #t (bits-ref b2)
			     i2 j2 d2 (bits-ref b1)
			     (get-setter)
			    (lambda (set k)
			      (set v k x))))
		  
		      #f))))))))
  
  (define f-empty12
    (lambda (b1 ii1 j1 d1 b2 ii2 j2 d2)
      (define n2    (+ 1 (dist ii2 j2)))
      (define N2    (sz-ref b2))
      (define bits1 (bits-ref b1))
      (define bits2 (bits-ref b2))

      (if (= n2 N2)	  
	  (if (and (= k1 0) (= k2 0) (> (get-bit bits1 i-ro) 0))
	      (begin
		(when (> (get-m bits1) (get-m bits2))
		  (if (> (get-bit bits2 i-expand) 0)
		      (set! bits2 (set-m bits2 (get-m bits1)))
		      (error "can't expand to satisfy copy!")))
		
		(v-set! b2 b1)
		
		(j-set! b2 ii1)
		
		(bits-set! b2 (set-bit bits2 i-cow))
		
		(clear-constant-data b2)
		
		#f)
	      (begin
		(when (and (> (get-bit bits2 i-expand) 0)
			   (> (get-m bits1) (get-m bits2)))
		  (set! bits2 (set-m bits2 (get-m bits1))))
		
		(if (populate b2 ii2 n2)
		    #t
		    (f-bin b1 ii1 j1 d1 b2 ii2 j2 d2))))
	  (begin
	    (if (populate b2 ii2)
		#t
		(f-bin b1 ii1 j1 d1 b2 ii2 j2 d2))))))
  
  (define f-empty2
    (lambda (b1 i1 j1 d1 b2 i2 j2 d2)
      (define bits2 (bits-ref b2))
      (define x1    (d-ref b1))
      (define n2    (+ 1 (dist i2 j2)))
      (define N2    (sz-ref b2))

      (cond
       ((= n2 N2)
	(if cfkn2	    
	    (d-set! b2 (cfkn2 x1 (d-ref b2)))
	    (d-set! b2 x1))
	#f)

       (else
	(if (populate b2 i2)
	    #t
	    (f-empty11 b1 i1 j1 d1 b2 i2 j2 d2))))))

  (define (f-split b1 i1 j1 d1 b2 i2 j2 d2)
    (define (run s1 s2)
      (supervector-copy! s1 i1 s2 i2 (+ (dist i1 j1) 1)
			 #:swap?   swap?
			 #:ref?    ref?
			 #:cfkn    cfkn
			 #:cfkn1   cfkn1
			 #:cfkn2   cfkn2
			 #:k1      k1
			 #:k2      k2)
      #f)

    (let ((s1 (v-ref b1))
	  (s2 (v-ref b2)))
      (cond
       ((and (supervector? s1) (supervector? s2))
        (run s1 s2))
       
       ((supervector? s2)
	(run (make-supervector b1) s2))
       	
       ((supervector? s1)
	(let* ((n     (+ (dist i1 j1) 1))
	       (n1    (- (sz-ref b1) i1))
	       (n2    (sz-ref b2))
	       (k1    (quotient n2 2))
	       (k2    (- n2 k1))
	       (level (get-level (bits-ref b2)))
	       (n2h   (max k1 k2)))

	  (cond
	   ((= n n1 n2)
	    (if (or ref? (> (get-bit (bits-ref b1) i-ro) 0))
		(v-set! b2 s1)
		(v-set! b2 (supervector-copy s1)))
	    #f)
	   
	   ((= n n2)
	    (let ((bb1 (vector-ref (w+ref s1) 0)))
	      (if (> n (- (sz-ref bb1) i1))
		  (if (or ref? (> (get-bit (bits-ref b1) i-ro) 0))
		      (begin
			(v-set! b2 s1)
			(j-set! b2 i1))		
		      (v-set! b2 (supervector-copy s1 i1 n)))

		  (let ((s1 (make-supervector bb1 #:ref? #t))
			(s2 (make-supervector b2  #:ref? #t)))
		    (supervector-copy! s1 i1 s2 i2 n))))
	    
	    #f)

	   ((and (> level 0) (> n2h n))
	    (split-bin b2)
	    (f-split b1 i1 j1 d1 b2 i2 j2 d2))

	   ((and (> level 0) (<= n2h n))
	    (split-bin b2 #:binsize n)
	    (f-split b1 i1 j1 d1 b2 i2 j2 d2))

	   (else
	    (run s1  (make-supervector b2)))))))))
    
  (define (f-bin b1 i1 j1 d1 b2 i2 j2 d2)
    (define bits1 (bits-ref b1))
    (define bits2 (bits-ref b2))
    (define m     (get-m bits2))
    (define m1    (get-m bits1))
    (define v1    (v-ref b1))
    (define v2    (v-ref b2))
    (define n2    (+ 1 (dist i2 j2)))
    (define N2    (sz-ref b2))
    
    (define (f)
      (define bits1 (bits-ref b1))
      (define bits2 (bits-ref b2))
      (define v1    (v-ref b1))
      (define v2    (v-ref b2))

      (call-with-values (lambda () (vv-ref v1 i1 j1 d1))
	(lambda (v1 i1 j1 d1)
	  (call-with-values (lambda () (vv-ref v2 i2 j2 d2))
	    (lambda (v2 i2 j2 d2)
	      (if (fix-cow b2 i2 j2)
		  #t
		  (let lp ((bits2 bits2))
		    (cond     
		     ;; a memcopy is really efficient
		     ((and (= m m1)
			   (bits-ok-for-copy bits1 bits2)
			   (= k1 0) (= k2 0))
		      (bytevector-copy*! v1 (min i1 j1) v2 (min i2 j2)
					 (* m n2))
		      #f)
     
		     ;; Junk, we need to convert between different sizes -- slow
		     ((bits-ok-for-transfer bits1 bits2)
		      (if cfkn
			  (cfkn (not (= n2 N2)) b1 i1 j1 d1 b2 i2 j2 d2)
			  (standard-loop-2 (not (= n2 N2))
			       (bits2 i2 j2 d2)
			       (bits1 i1 j1 d1)
			       (get-setter)
			       (get-reffer)
			    (lambda (set k2 ref k1)
			      (set v2 k2 (ref v1 k1)))))
	    
		      (when (= n2 N2)
			(clear-constant-data b2))
		    
		      #f)
		     
		     ((> (get-bit bits2 i-expand) 0)
		      (expand-to-bits b2 i2 bits2 bits1 #:j j2)
		      (set! v2 (v-ref b2))
		      (lp (bits-ref b2)))
		      		   
		     (else
		      (error
		       "cannot copy typed data or unmatched sized data"
		       ))))))))))
    
    (if (and (= n2 N2) (= k1 0) (= k2 0) (> (get-bit bits1 i-ro) 0))
	(begin
	  (v-set! b2 b1)
	  (j-set! b2 i1)
	  (clear-constant-data b2)
	  #f)
	(f)))
  
    
  (define (f-slow s1 i1 j1 d1 s2 i2 j2 d2)
    (error "Bug should not enter here"))

  (supervector-chunk-tr
   f-empty11 f-empty12 f-empty2 f-bin f-slow s1 i1 s2 i2 n
   #:swap? swap? #:ref? ref? #:f-split f-split))
  

(define* (supervector-copy s
			   #:optional (i 0) (n (- (supervector-length s) i))
			   #:key (ref? #f) (ro? #f))
   (let ((s2 (make-supervector n
			       #:binsize (sbz-ref s)
			       #:bits (sbits-ref s))))
     (supervector-copy! s i s2 0 n #:ref? ref?)
     s2))


;; cludge to avoid cyclic code.
(set! core-supervector-copy! supervector-copy!)


(define (-supervector-make-ro! s)
  (define (f b)
    (let ((v (v-ref b)))
      (when (or (supervector? v) (bin? v))
	(when (= (get-bit (if (bin? v) (bits-ref v) (sbits-ref v)) i-ro) 0)
	  (let* ((n  (sz-ref b))
		 (s1 (make-supervector n #:binsize n))
		 (s2 (make-supervector n #:binsize n)))
	    (v-set! s1 b)
	    (supervector-copy! s1 0 s2 0 n)
	    (v-set! b (v-ref (vector-ref (w+ref s2) 0)))
	    (j-set! b 0)))))

    (bits-set! b (set-bit (bits-ref b) i-ro)))
	    
  (sbits-set! s (set-bit (sbits-ref s) i-ro))
  
  (let ((n- (n-ref s))
	(n+ (n+ref s))
	(w- (w-ref s))
	(w+ (w+ref s)))

    (let lp ((i 0))
      (when (< i n-)
	(f (vector-ref w- i))))
    
    (let lp ((i 0))
      (when (< i n+)
	(f (vector-ref w+ i))))))

(set! supervector-make-ro! -supervector-make-ro!)

;; TEST COPY
(define (test-supervector-copy)
  (define n  1000)
  (define x1 (make-supervector (make-bytevector (* 4 n) -1 ) #:m 1 #:signed? 1))
  (define x2 (make-supervector n #:m 1 #:scm?  1))
  
  (supervector-copy! x1 0 x2 0 n)
  (pk (supervector-copy x2 0 10))
  
  #t)
