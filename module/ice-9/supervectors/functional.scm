;;HACK
(define-syntax-rule (define-module- name args ...)
  (cond-expand
   (guile-3
    (define-module name
      #:declarative? #f
      args ...))
   (guile
    (define-module name args ...))))


(define-module- (ice-9 supervectors functional)
  #:use-module (ice-9 supervectors core)
  #:use-module (ice-9 supervectors make)
  #:use-module (ice-9 supervectors expand)
  #:use-module (ice-9 supervectors macros)
  #:use-module (ice-9 supervectors typeinfo)
  #:use-module (ice-9 supervectors basic)
  #:use-module (ice-9 supervectors iterator)
  #:use-module (rnrs bytevectors)
  #:use-module (ice-9 match)
  #:replace (negate)
  #:export
  (   
   ;; funcitonal map reduce chinked versions
   supervector-chunk-tr
   supervector-chunk-stream
   supervector-chunk-fold
   supervector-chunk-fold2

   ;; funcitonal map reduce on points in supervector
   supervector-fold
   supervector-stream
   supervector-fold2
   supervector-tr
   supervector-unary
   supervector-binary
   supervector-for-each
   supervector-for-each2
   
   supervector-op
   supervector-op2
   supervector-binary
   mk-supervector-binary
      
   ;; supervector-binary return controls
   replace
   add
   scale
   nothing
   ;;negate moved to replace
   invert

   superbits-binary
   
   test-supervector-functional

   supervector-lognot!
   supervector-not!
   ))


(define-inlinable (dist x y) (if (> x y) (- x y) (- y x)))

(define-inlinable (vv-ref v i j d)
  (if (bin? v)
      (let* ((vv (v-ref v))
	     (d2 (get-dir (bits-ref v)))
	     (ii (if (> d2 0) (+ (j-ref v) i) (- (j-ref v) i)))
	     (jj (if (> d2 0) (+ (j-ref v) j) (- (j-ref v) j)))
	     (dd (* d d2)))
	(values vv ii jj dd))
      (values v i j d)))

(define (supervector-tr-fkn f-empty11 f-empty12 f-empty2 f-bin f-slow f-split)
  (define self
    (lambda (minsize swap? ref?)
      (lambda (b1 i11 i21 d1 b2 i12 i22 d2 seed)
	(let ((n1 (+ 1 (dist i11 i21)))
	      (n2 (+ 1 (dist i12 i22)))
	      (N1 (sz-ref b1))
	      (N2 (sz-ref b2)))
	  
	  (cond
	   ((not (= n1 n2))
	    (error "Bug in tr-fkn"))

	   ;; If any of the bins are a supervector we have a border case
	   ;; where minsize can't be satisfied, use fallback slow code
	   ((or (supervector? b1) (supervector? b2))
	    (when (bin? b1)
	      (set! b1 (make-supervector b1 #:ref? #t)))

	    (when (bin? b2)
	      (set! b2 (make-supervector b2 #:ref? #t)))
	    
	    (f-slow b1 i11 i21 b2 i12 i22 seed))

	   ;; Fast path: two bins
	   (else
	    (let ((v1  (v-ref b1))
		  (v2  (v-ref b2)))
	      (let lp ((v1 v1) (v2 v2))
		(cond
		 ((= n1 N1 N2)
		  (cond		     
		   (swap?
		    (let ((temp (b-copy b1)))
		      (b-copy! b2 b1)
		      (b-copy! temp b2)
		      (values #f seed)))
		 
		   (ref?
		    (b-copy! b2 b1)
		    (values #f seed))
		 
		   ((and (not v1) (not v2))
		    (f-empty2 b1 i11 i21 d1 b2 i12 i22 d2 seed))

		   ((and f-split (or (supervector? v1) (supervector? v2)))
		    (f-split b1 i11 i21 d1 b2 i12 i22 d2 seed))
		   
		   ((and (supervector? v1) (supervector? v2))
		    (call-with-values
			(lambda ()			  
			  (supervector-tr-0 self seed
					    v1
					    (+ (if (< d1 0) 1 0) i11)
					    (+ (if (> d1 0) 1 0) i21)
					    v2
					    (+ (if (< d2 0) 1 0) i12)
					    (+ (if (> d2 0) 1 0) i22)
					    #:minsize minsize
					    #:swap?   swap?
					    #:ref?    ref?))
		      (values
		       #f
		       (case-lambda
			(() #f)
			((x) x)))))
		    
		   ((supervector? v1)
		    (lp v1 (make-supervector b2)))
		  
		   ((and (supervector? v2))
		    (lp (make-supervector b1) v2))
	     
		  
		   ((not v2)
		    (f-empty12 b1 i11 i21 d1 b2 i12 i22 d2 seed))

		   ((not v1)
		    (f-empty11 b1 i11 i21 d1 b2 i12 i22 d2 seed))

		   (else
		    (f-bin b1 i11 i21 d1 b2 i12 i22 d2 seed))))
		 
		 ((and f-split (or (supervector? v1) (supervector? v2)))
		  (f-split b1 i11 i21 d1 b2 i12 i22 d2 seed))

		 ((and (supervector? v1) (supervector? v2))
		  (call-with-values
		      (lambda ()
			(supervector-tr-0 self seed
					   v1
					   (+ (if (< d1 0) 1 0) i11)
					   (+ (if (> d1 0) 1 0) i21)
					   v2
					   (+ (if (< d2 0) 1 0) i12)
					   (+ (if (> d2 0) 1 0) i22)

					  #:minsize minsize
					  #:swap?   swap?
					  #:ref?    ref?))
		    (values
		     #f
		     (case-lambda
		      (() #f)
		      ((x) x)))))
	       
		 ((supervector? v1)
		  (lp v1 (make-supervector b2)))
	   
		 ((and (supervector? v2))
		  (lp (make-supervector b1) v2))
		
		 ((and (not v1) (not v2))
		  (f-empty2 b1 i11 i21 d1 b2 i12 i22 d2 seed))

		 ((not v2)
		  (f-empty12 b1 i11 i21 d1 b2 i12 i22 d2 seed))

		 ((not v1)
		  (f-empty11 b1 i11 i21 d1 b2 i12 i22 d2 seed))
	     
		 (else
		  (f-bin b1 i11 i21 d1 b2 i12 i22 d2 seed)))))))))))
  
  self)


(define* (supervector-tr-0 tr seed s1 i1 j1 s2 i2 j2
			   #:key
			   (minsize 1) (swap? #f) (ref? #f)
			   (divide? #f))
  
  (let* ((N1   (supervector-length s1))
	 (N2   (supervector-length s2))
	 (dir1 (if (> i1 j1) -1 1))
	 (dir2 (if (> i2 j2) -1 1))
	 (n1   (+ 1 (dist i1 j1)))
	 (n2   (+ 1 (dist i2 j2)))
	 (f    (tr minsize swap? ref?)))
    (when (not (= n1 n2))
      (error "index missmatch in supervector-tr"))

    (let lp ((ch1  (supervector-chunk-init s1 i1 j1 #:dir dir1))	     
	     (ch2  (supervector-chunk-init s2 i2 j2 #:dir dir2))
	     (seed seed))
      
      (aif (b2 i2 i12 i22 d2 ch2) (supervector-chunk-next
				   ch2 #:minsize minsize)
	   (aif (b1 i1 i11 i21 d1 ch1) (supervector-chunk-next
					ch1
					#:d       (+ 1 (dist i22 i12))
					#:minsize minsize)		
		(begin
		  (let* ((n1     (dist i11 i21))
			 (n2     (dist i12 i22))
			 (bits1  (bits-ref b1))
			 (bits2  (bits-ref b2))
			 (m1     (get-m bits1))
			 (m2     (get-m bits2))
			 (nn1    (* n1 m1))
			 (nn2    (* n2 m2))
			 (level  (get-level bits2)))

		    (when (> (get-bit bits2 i-ro) 0)
		      (error "can't write to read only supervector memory"))
		    
		    (if (and divide?
			     (> level 0)
			     (> nn2 nn1)
			     (= 0 (min i12 i22))
			     (= (sz-ref b2) (+ 1 (max i12 i22))))

			;; We will subdived s2 to try match s1
			(let* ((sz1    (sz-ref b1))
			       (bits2  (set-level bits2 (- level 1)))
			       (bs2    (bs-ref b2))
			       (NN     2)
			       (bs3    (quotient n2 2))
			       (sb     (make-supervector b2))
			       (s      (make-supervector n2 (d-ref b2)
							 #:bits    bits2
							 #:binsize bs3)))
			  (core-supervector-copy! sb 0 s 0 n2)
			  (v-set! b2 s)
			  (lp (cons    i1     ch1)
			      (cons    i2     ch2)
			      seed))
			
			(let* ((n   (min n1 n2))
			       (dh1 (cons (+ i1 (* d1 (+ n 1))) ch1))
			       (dh2 (cons (+ i2 (* d2 (+ n 1))) ch2))
			       (i21 (if (> i11 i21) (- i11 n) (+ i11 n)))
			       (i22 (if (> i12 i22) (- i12 n) (+ i12 n))))

			  (aif (p seed2) (f b1 i11 i21 d1 b2 i12 i22 d2 seed)
			       ;; we refdefined the chunk, retry
			       (lp (cons   i1     ch1)
				   (cons   i2     ch2)
				   seed)

			       (lp dh1 dh2 seed2))))))
		  
		;; If chunk 2 is not within chunk 1's size
		seed)
      
	   ;; Endpoint no more parts to iterate
	   seed))))

(define* (supervector-chunk-stream
	  f-empty11  f-empty12 f-empty2 f-bin f-slow seed s1 i1 s2 i2 n
	  #:key
	  (dir1    1  )
	  (dir2    1  )
	  (f-split #f )
	  (minsize 1  )
	  (divide? #f )
	  (swap?   #f )
	  (ref?    #f ))

  (when (> n 0)

    (supervector-tr-0 (supervector-tr-fkn
		       f-empty11
		       f-empty12
		       f-empty2
		       f-bin
		       f-slow
		       (if f-split f-split #f))
		      
		      seed

		      s1
		      (if (< dir1 0) (+ i1 n) i1)
		      (if (> dir1 0) (+ i1 n) i1)

		      s2
		      (if (< dir2 0) (+ i2 n) i2)
		      (if (> dir2 0) (+ i2 n) i2)

		      #:minsize minsize
		      #:divide? divide?
		      #:swap?   swap?
		      #:ref?    ref?)))

(define* (supervector-chunk-tr
	  f-empty11  f-empty12 f-empty2 f-bin f-slow s1 i1 s2 i2 n
	  #:key
	  (f-split #f )
	  (minsize 1  )
	  (divide? #f )
	  (swap?   #f )
	  (dir1    1  )
	  (dir2    1  )
	  (ref?    #f ))

  (when (> n 0)
    (supervector-tr-0 (supervector-tr-fkn
		       (DD f-empty11)
		       (DD f-empty12)
		       (DD f-empty2)
		       (DD f-bin)
		       (DD f-slow)
		       (if f-split (DD f-split) #f))
		      
		      #f

		      s1
		      (if (< dir1 0) (+ i1 n) i1)
		      (if (> dir1 0) (+ i1 n) i1)

		      s2
		      (if (< dir2 0) (+ i2 n) i2)
		      (if (> dir2 0) (+ i2 n) i2)

		      #:minsize minsize
		      #:divide? divide?
		      #:swap?   swap?
		      #:ref?    ref?)))

(define (E f) (lambda x (values #f (apply f x))))

(define* (supervector-chunk-fold2
	  f-empty11  f-empty12 f-empty2 f-bin f-slow seed s1 i1 s2 i2 n
	  #:key
	  (f-split #f )
	  (minsize 1  )
	  (divide? #f )
	  (swap?   #f )
	  (ref?    #f )
	  (dir1    1  )
	  (dir2    1  ))
		   
  
  (when (> n 0)
    (supervector-tr-0 (supervector-tr-fkn
		       (E f-empty11)
		       (E f-empty12)
		       (E f-empty2)
		       (E f-bin)
		       (E f-slow)
		       (if f-split (E f-split) #f))

		      seed
		      
		      s1
		      (if (< dir1 0) (+ i1 n) i1)
		      (if (> dir1 0) (+ i1 n) i1)

		      s2
		      (if (< dir2 0) (+ i2 n) i2)
		      (if (> dir2 0) (+ i2 n) i2)

		      #:minsize minsize
		      #:divide? divide?
		      #:swap?   swap?
		      #:ref?    ref?)))


(define* (supervector-tr
	  f s1 i1 s2 i2 n
	  #:key
	  (dir     1 )
	  (ctrl    #f)
	  (ref?    #f)
	  (divide? #f)
	  (minsize 1))
  
  (define f-empty12
    (lambda (b1 i1 j1 d1 b2 i2 j2 d2)
      (if (populate b2 i2 (+ (dist i2 j2) 1))
	  #t
	  (f-bin b1 i1 j1 d1 b2 i2 j2 d2))))
      
  (define f-empty11
    (lambda (b1 i1 j1 d1 b2 i2 j2 d2)
      (let ((x    (f (d-ref b1)))
	    (bits (bits-ref b2))
	    (n2   (+ 1 (dist i1 j1)))
	    (N2   (sz-ref b2)))

	(let ((v2 (v-ref b2)))
	  (if (= n2 N2)
	      (begin
		(v-set! b2 #f)
		(clear-constant-data b2)
		#f)

	      (begin
		(if (fix-cow b2 i2 j2)
		    #t
		    (standard-loop-1 #t bits i2 j2 d2 #f (get-setter #:b b2)
		      (lambda (set k)
			(set v2 k (f x)))))
		#f))))))
    		      
  (define f-empty2 
    (lambda (b1 i1 j1 d1 b2 i2 j2 d2)
      (let ((n2  (+ (dist i2 j2) 1))
	    (sz2 (sz-ref b2)))
	(if (= n2 sz2)
	    (begin
	      (d-set! b2 (f (d-ref b1)))
	      #f)
	    (if (populate b2 i2 n2)
		#t
		(f-empty11 b1 i1 j1 d1 b2 i2 j2 d2))))))
  
  (define (f-bin b1 i1 j1 d1 b2 i2 j2 d2)
    (let* ((n2   (+ 1 (dist i1 j1)))
	   (N2   (sz-ref b2)))
      
      (define (g)
	(if (fix-cow b2 i2 j2)
	    #t
	    (let ((v2 (v-ref b2))
		  (v1 (v-ref b1))
		  (bits1 (bits-ref b1))
		  (bits2 (bits-ref b2)))
	    
	      (standard-loop-2 (not (= n2 N2))
		  (bits2 i2 j2 d2)
		  (bits1 i1 j1 d1)
		  (get-setter #:b b2)
		  (get-reffer #:b b1)
	        (lambda (set k2 ref k1)
		  (set v2 k2 (f (ref v1 k1)))))
	      
	      (when (= n2 N2) (clear-constant-data b2))
	      
	      #f)))

      (if (and ctrl (= n2 N2))
	  (aif (it val) (ctrl b1)	      
	       (cond
		((eq? it nothing)
		 #t)
		
		((eq? it negate)
		 (if (can-negate b2)
		     (begin
		       (negate-bin b2)
		       #f)
		     (g)))

		((eq? it invert)
		 (if (can-invert b2)
		    (begin
		      (invert-bin b2)
		      #f)
		    (g)))

		((eq? it add)
		 (if (can-add b2)
		    (begin
		      (add-bin b2 val)
		      #f)
		    (g)))

		((eq? it scale)
		 (if (can-scale b2)
		     (begin
		       (scale-bin b2 val)
		       #f)
		     (g)))

		(else
		 (g)))
	       (g))
	  (g))))
		  
  (define (f-slow s1 i1 j1 d1 s2 i2 j2 d2)
    (error "BUG"))
  
  (supervector-chunk-tr
   f-empty11 f-empty12 f-empty2 f-bin f-slow
   s1 i1 s2 i2 n #:minsize 1 #:divide? divide? #:dir1 dir #:dir2 dir))

(define* (supervector-stream
	  f seed s1 i1 s2 i2 n
	  #:key
	  (dir     1)
	  (ctrl    #f)
	  (ref?    #f)
	  (divide? #f)
	  (minsize 1))
  
  (define f-empty12
    (lambda (b1 i1 j1 d1 b2 i2 j2 d2 seed)
      (if (populate b2 i2 (+ (dist i2 j2) 1))
	  (values #t seed)
	  (f-bin b1 i1 j1 d1 b2 i2 j2 d2 seed))))
      
  (define f-empty11
    (lambda (b1 i1 j1 d1 b2 i2 j2 d2 seed)
      (let ((x    (f (d-ref b1)))
	    (bits (bits-ref b2))
	    (n2   (+ 1 (dist i1 j1)))
	    (N2   (sz-ref b2)))

	(let ((v2 (v-ref b2)))
	  (if (= n2 N2)
	      (begin
		(v-set! b2 #f)
		(call-with-values (lambda () (f (d-ref b1) seed))
		  (lambda (x seed)
		    (d-set! b2 x)
		    (clear-constant-data b2)
		    (values #f seed))))

	      (begin
		(if (fix-cow b2 i2 j2)
		    (values #t seed)
		    (begin
		      (standard-loop-1 #t bits i2 j2 d2 #f (get-setter #:b b2)
			(lambda (set k)
			  (call-with-values (lambda () (f x seed))
			    (lambda (x seed2)
			      (set v2 k x)
			      (set! seed seed2)))))
		      (values #f seed)))))))))
    
  (define f-empty2 
    (lambda (b1 i1 j1 d1 b2 i2 j2 d2 seed)
      (let ((n2  (+ (dist i2 j2) 1))
	    (sz2 (sz-ref b2)))
	(if (= n2 sz2)
	    (call-with-values (lambda () (f (d-ref b1) seed))
	      (lambda (x seed)
		(d-set! b2 x)
		(values #f seed)))
	    
	    (if (populate b2 i2 n2)
		(values #t seed)
		(f-empty11 b1 i1 j1 d1 b2 i2 j2 d2 seed))))))
  
  (define (f-bin b1 i1 j1 d1 b2 i2 j2 d2 seed)
    (let* ((n2   (+ 1 (dist i1 j1)))
	   (N2   (sz-ref b2)))
      
      (define (g)
	(if (fix-cow b2 i2 j2)
	    (values #t seed)
	    (let ((v2 (v-ref b2))
		  (v1 (v-ref b1))
		  (bits1 (bits-ref b1))
		  (bits2 (bits-ref b2)))
	    
	      (standard-loop-2 (not (= n2 N2))
		  (bits2 i2 j2 d2)
		  (bits1 i1 j1 d1)
		  (get-setter #:b b2)
		  (get-reffer #:b b1)
		(lambda (set k2 ref k1)
		  (call-with-values (lambda () (f (ref v1 k1) seed))
		    (lambda (x seed2)
		      (set v2 k2 x)
		      (set! seed seed2)))))
	      
	      (when (= n2 N2) (clear-constant-data b2))
	      
	      (values #f seed))))

      (if (and ctrl (= n2 N2))
	  (aif (it val) (ctrl b1)	      
	       (cond
		((eq? it nothing)
		 #t)
		
		((eq? it negate)
		 (if (can-negate b2)
		     (begin
		       (negate-bin b2)
		       #f)
		     (g)))

		((eq? it invert)
		 (if (can-invert b2)
		    (begin
		      (invert-bin b2)
		      #f)
		    (g)))

		((eq? it add)
		 (if (can-add b2)
		    (begin
		      (add-bin b2 val)
		      #f)
		    (g)))

		((eq? it scale)
		 (if (can-scale b2)
		     (begin
		       (scale-bin b2 val)
		       #f)
		     (g)))

		(else
		 (g)))
	       (g))
	  (g))))
		  
  (define (f-slow s1 i1 j1 d1 s2 i2 j2 d2 seed)
    (error "BUG"))
  
  (supervector-chunk-stream
   f-empty11 f-empty12 f-empty2 f-bin f-slow seed
   s1 i1 s2 i2 n #:minsize 1 #:divide? divide? #:dir1 dir #:dir2 dir))



(define* (supervector-unary op s i n #:key (ctrl #f))
  (supervector-tr op s i s i n #:ctrl ctrl))
	 
(define replace (list 0))
(define add     (list 1))
(define scale   (list 2))
(define nothing (list 3))
(define negate  (list 4))
(define invert  (list 5))

(define-syntax-rule (mk-supervector-binary supervector-binary l ...)
(define* (supervector-binary
	  op op1 op2 op12 s1 i1 s2 i2 n
	  #:key
	  (k1      0 )
	  (k2      0 )
	  (cfkn    #f)
	  (cfkn1   #f)
	  (cfkn2   #f)
	  (divide? #f)
	  (ref?    #f)
	  (flat?   #f))
  
  (define f-empty12
    (lambda (b1 i1 j1 d1 b2 i2 j2 d2)
      (define (f)
	(if (populate b2 i2 (+ (dist i2 j2) 1))
	    #t
	    (f-bin b1 i1 j1 d1 b2 i2 j2 d2)))

      (define (handle-ref b1 b2 bits i-x)
	(define (fin j b1)
	  (clear-constant-data b2)
	  (v-set!    b2 b1)
	  (j-set!    b2 j)
	  (if (read-only? b1)
	      (bits-set! b2 (set-bit bits (logior i-x i-cow)))
	      (bits-set! b2 (set-bit bits (logior i-x)))))
	
	(let lp ((j i1) (b1 b1))
	  (if (read-only? b1)
	      (let ((v (v-ref b1)))
		(if (bin? v)
		    ;; TODO fix this for change of direction
		    (lp (+ (j-ref v) j) v)
		    (fin j b1)))
	      (fin j b1))))

      (let ((bits (bits-ref b2)))	
	(aif (p val) (if (and (= k1 0) (= k2 0))
			 (op2 (d-ref b2))
			 (values #f #f))
			      
	     (cond
	      ((eq? p nothing)
	       (if (or (read-only? b1) ref?)
		   (handle-ref b1 b2 bits 0)
		   (f)))
	      	     
	      ((eq? p replace)	       
	       (d-set! b2 val)
	       #f)
	      
	      ((eq? p invert)
	       (if (or (read-only? b1) ref?)
		   (handle-ref b1 b2 bits i-invert)
		   (f)))

	      ((eq? p negate)
	       (if (or (read-only? b1) ref?)
		   (handle-ref b1 b2 bits i-negate)
		   (f)))

	      ; TODO
	      ((eq? p add)
	       (if (or (read-only? b1) ref?)
		   (begin
		     (clear-constant-data b2)
		     (v-set! b2 b1)
		     (j-set! b2 i1)
		     (d-set! b2 (+ val (d-ref b1))))		     
		   (f)))

	      ;; TODO
	      ((eq? p scale)
	       (if (or (read-only? b1) ref?)
		   (begin
		     (clear-constant-data b2)
		     (v-set!  b2 b1)
		     (j-set!  b2 i1)
		     (d-set!  b2 (* val (d-ref b1)))
		     (sc-set! b2 (* val (sc-ref b1))))
		   (f)))
		     
	      (else
	       (f)))
	     
	     (f)))))

    (define f-empty11
    (lambda (b1 i1 j1 d1 b2 i2 j2 d2)
      (let ((x    (d-ref b1))
	    (bits (bits-ref b2))
	    (n2   (+ 1 (dist i1 j1)))
	    (N2   (sz-ref b2)))
	
	(define (g)
	  (let ((v2 (v-ref b2)))
	    (if (fix-cow b2 i2 j2)
		#t
		(if cfkn1
		    (begin
		      (cfkn1 #t x b2 i2 j2 d2)
		      #f)
		
		    (let ((sc2 (sc-ref b2))
			  (d2  (d-ref  b2)))
		      (cond
		       ((and (= d2 0) (= sc2 1))
			(standard-loop-1* (not (= n2 N2))
			    bits i2 j2 d2 #f (get-ref-setter #:b b2 l ...)
			  (lambda (ref set k)
			    (set v2 k (op (ref v2 k) x)))))

		       ((= n2 N2)
			(standard-loop-1* (not (= n2 N2))
			    bits i2 j2 d2 #f (get-ref-setter #:b b2 l ...)
			  (lambda (ref set k)
			    (set v2 k (op (+ d2 (* sc2 (ref v2 k))) x)))))
		       (else
			(let ((sc3 (/ 1 sc2))
			      (d3  (- (/ d2 sc2))))
			  (standard-loop-1* (not (= n2 N2))
			      bits i2 j2 d2 #f (get-ref-setter #:b b2 l ...)
			    (lambda (ref set k)
			      (set v2 k (+ d3
					   (* sc3
					      (op (+ d2
						     (* sc2
							(ref v2 k))) x)))))))))
		       

		      (when (= n2 N2)
			(clear-constant-data b2))
		  
		      #f)))))

	(aif (p val) (if (and (= k1 0) (= k2 0))
			 (op1 (d-ref b1))
			 (values #f #f))
	     (if (= n2 N2)
		 (cond
		  ((eq? p nothing)
		   #f)
	     
		  ((eq? p replace)	      
		   (v-set! b2 #f)
		   (d-set! b2 val)
		   #f)
	     
		  ((eq? p negate)
		   (if (can-negate b2)
		       (begin (negate b2) #f)
		       (g)))
	     
		  ((eq? p invert)
		   (if (can-invert b2)
		       (begin (invert-bin b2) #f)
		       (g)))
	     
		  ((eq? p add)
		   (if (can-add b2)
		       (begin (add b2 val) #f)
		       (g)))

		  ((eq? p scale)
		   (if (can-scale b2)
		       (begin (scale b2 val) #f)
		       (g))))
		 (g))
	       
	     (g)))))
	           
    		      
  (define f-empty2 
    (lambda (b1 i1 j1 d1 b2 i2 j2 d2)
      (let ((n2  (+ (dist i2 j2) 1))
	    (sz2 (sz-ref b2)))
	(if (= n2 sz2)
	    (if cfkn2
		(begin
		  (d-set! b2 (cfkn2 (d-ref b1) (d-ref b2)))
		  #f)
		(begin
		  (d-set! b2 (op12 (d-ref b1) (d-ref b2)))
		  #f))
	    (if (populate b2 i2 n2)
		#t
		(f-empty11 b1 i1 j1 d1 b2 i2 j2 d2))))))
  
  (define (f-bin b1 i1 j1 d1 b2 i2 j2 d2)
    (let* ((n2   (+ 1 (dist i1 j1)))
	   (N2   (sz-ref b2)))
      
      (if (fix-cow b2 i2 j2)
	  #t
	  (if cfkn
	      (begin
		(cfkn (not (= n2 N2)) b1 i1 j1 d1 b2 i2 j2 d2)
		(when (= n2 N2)
		  (clear-constant-data b2))
		#f)

	      (let ((v2   (v-ref b2))
		    (v1   (v-ref b1))
		    (sc1  (sc-ref b1))		    
		    (sc2  (sc-ref b2))		    
		    (dd1   (d-ref b1))		    
		    (dd2   (d-ref b2))		    
		    (bits1 (bits-ref b1))
		    (bits2 (bits-ref b2)))

		(cond
		 ((and (= sc1 1) (= sc2 1)
		       (= d1  0) (= d2  0))
		  (standard-loop-3 (not (= n2 N2))
		       (bits2 i1 j1 d1)
		       (bits1 i2 j2 d2)
		       (get-reffer #:b b1 l ...)
		       (get-ref-setter #:b b2 l ...)		       
		    (lambda (ref1 k1 ref2 set2 k2)
		      (set2 v2 k2 (op (ref1 v1 k1) (ref2 v2 k2))))))

		 ((and flat? (= sc1 sc2))
		  (d-set! s2 (op dd1 dd2))
		  (standard-loop-3 (not (= n2 N2))
		    (bits2 i1 j1 d1)
		    (bits1 i2 j2 d2)
		    (get-reffer #:b b1 l ...)
		    (get-ref-setter #:b b2 l ...)
		    (lambda (ref1 k1 ref2 set2 k2)			 
		      (set2 k2 (op (ref1 v1 k1) (ref2 v2 k2))))))
		 		  
		 ((= n2 N2)
		  (standard-loop-3 (not (= n2 N2))
		       (bits2 i1 j1 d1)
		       (bits1 i2 j2 d2)
		       (get-reffer #:b b1 l ...)
		       (get-ref-setter #:b b2 l ...)
		       (lambda (ref1 k1 ref2 set2 k2)
			 (set2 v2 k2 (op (+ dd1 (* sc1 (ref1 v1 k1)))
					 (+ dd2 (* sc2 (ref2 v2 k2))))))))
		 (else
		  (let ((sc3 (/ 1 sc2))
			(dd3 (- (/ d2 sc2))))
		  (standard-loop-3 (not (= n2 N2))
		       (bits2 i1 j1 d1)
		       (bits1 i2 j2 d2)
		       (get-reffer #:b b1 l ...)
		       (get-ref-setter #:b b2 l ...)
		    (lambda (ref1 k1 ref2 set2 k2)
		      (set2 v2 k2
			    (+ dd3
			       (* sc3
				  (op (+ dd1 (* sc1 (ref1 v1 k1)))
				      (+ dd3 (* sc2 (ref2 v2 k2))))))))))))
		    
		(when (= n2 N2)
		  (clear-constant-data b2))

		#f)))))
  
  (define (f-slow s1 i1 j1 d1 s2 i2 j2 d2)
    (error "BUG"))
  
  (supervector-chunk-tr
   f-empty11 f-empty12 f-empty2 f-bin f-slow
   s1 i1 s2 i2 n #:minsize 1 #:divide? divide?)))

(mk-supervector-binary supervector-binary)

(define (supervector-fold-fkn f-empty f-bin f-slow)
  (define self
    (lambda (minsize)
      (lambda (b i j d seed)
	(if (supervector? b)
	    (f-slow b i j d seed)
	    (let ((v (v-ref b)))
	      (if v
		  (if (supervector? v)
		      (supervector-fold-0 self seed v i (+ j 1)
					  #:minsize minsize)
		      (f-bin b i j d seed))
		  (f-empty b i j d seed)))))))
  self)

(define* (supervector-fold-0 f seed s i j #:key (minsize 1))
  (let ((dir (if (> i j) -1 1))
	(f   (f minsize)))
    (let lp ((chunk (supervector-chunk-init s i j #:dir dir))
	     (seed  seed))
      (aif (b i i1 i2 d ch) (supervector-chunk-next chunk #:minsize minsize)
	   (let ((i (+ i 1 (dist i1 i2))))
	     (lp (cons i ch) (f b i1 i2 d seed)))
	   seed))))


(define* (supervector-chunk-fold
	  f-empty f-bin f-slow seed s
	  #:optional
	  (i       0)
	  (len     #f)
	  #:key
	  (dir     1)
	  (minsize 1))

  (set! len (if len len (- (supervector-length s) i)))

  (if (> dir 0)
      (supervector-fold-0 (supervector-fold-fkn
			   f-empty
			   f-bin
			   f-slow)
			  seed s
			  i
			  (+ i len)
			  #:minsize minsize)
      
      (supervector-fold-0 (supervector-fold-fkn
			   f-empty
			   f-bin
			   f-slow)
			  seed s
			  (+ i len)
			  i
			  #:minsize minsize)))
  
(define* (supervector-fold
	  f seed s
	  #:optional
	  (i   0)
	  (len #f)
	  #:key
	  (dir     1)
	  (fast   #f)
	  (minsize 1))
  
  (define (f-empty bin i j d seed)
    (let ((x (d-ref bin)))
      (if fast
	  (fast x (sz-ref bin) seed)	
	  (let lp ((k i) (seed seed))
	    (let ((seed (f x seed)))
	      (if (not (= k j))
		  (lp (+ k d) seed)
		  seed))))))
  
  (define (f-bin bin i j d seed)
    (let ((v    (v-ref bin))
	  (seed seed))
      (standard-loop-1-f #t (bits-ref bin) i j d #f
			 (get-reffer #:b bin)
	(lambda (ref k)
	  (set! seed (f (ref v k) seed))))
      seed))
  
  	  
  (define (f-slow s i j d seed)
    (error "BUG"))

  (supervector-chunk-fold f-empty f-bin f-slow seed s i len
			  #:minsize minsize #:dir dir))

(define* (supervector-fold2
	  f seed s1 s2
	  #:optional
	  (i1 0)
	  (i2 0)
	  (n #f)
	  #:key
	  (dir1 1)
	  (dir2 1)
	  (divide? #f)
	  (minsize 1))
  
  (define f-empty12
    (lambda (b1 i1 j1 d1 b2 i2 j2 d2 seed)
      (let ((x    (d-ref b2))
	    (bits (bits-ref b1))
	    (v1   (v-ref b1))
	    (seed seed))

	(standard-loop-1 #t bits i1 j1 d1 #f (get-reffer #:b b1)
	  (lambda (ref k)
	    (set! seed (f (ref v1 k) seed))))

	seed)))
      
  (define f-empty11
    (lambda (b1 i1 j1 d1 b2 i2 j2 d2 seed)
      (let ((x    (d-ref b1))
	    (bits (bits-ref b2))
	    (v2   (v-ref b2))
	    (seed seed))

	(standard-loop-1 #t bits i2 j2 d2 #f (get-reffer #:b b2)
	  (lambda (ref k)
	    (set! seed (f x (ref v2 k) seed)))))

      seed))
    		      
  (define f-empty2 
    (lambda (b1 i1 j1 d1 b2 i2 j2 d2 seed)
      (let ((x1  (d-ref b1))
	    (x2  (d-ref b2))
	    (n2  (+ (dist i2 j2) 1)))
	(let lp ((k 0) (seed seed))
	  (if (< k n2)
	      (lp (+ k 1) (f x1 x2 seed))
	      seed)))))
  
  (define (f-bin b1 i1 j1 d1 b2 i2 j2 d2 seed)
    (let* ((n2    (+ 1 (dist i1 j1)))
	   (N2    (sz-ref b2))
	   (bits1 (bits-ref b1))
	   (bits2 (bits-ref b2))
	   (v1    (v-ref b1))
	   (v2    (v-ref b2)))
      
      (standard-loop-2 #t
		       (bits2 i2 j2 d2)
		       (bits1 i1 j1 d1)
		       (get-reffer)
		       (get-reffer)
	(lambda (ref1 k1 ref2 k2)
	  (set! seed (f (ref1 v1 k1) (ref2 v2 k2) seed))))

      seed))
    
  (define (f-slow s1 i1 j1 d1 s2 i2 j2 d2)
    (error "BUG"))

  (when (not n)
    (set! n (min (- (supervector-length s1) i1)
		 (- (supervector-length s2) i2))))
  
  (supervector-chunk-fold2
   f-empty11 f-empty12 f-empty2 f-bin f-slow seed
   s1 i1 s2 i2 n #:minsize 1 #:divide? divide? #:dir1 dir1 #:dir2 dir2))


(define* (supervector-for-each f s #:optional
			       (i   0)
			       (n   (- (supervector-length s) i)))
  (supervector-fold (lambda (x s) (f x) s) #t s i n))

(define* (supervector-for-each2 f s1 s2 #:optional (i1 0) (i2 0) (n #f))
  (when (not n)
    (set! n (min (- (supervector-length s1) i1)
		 (- (supervector-length s2) i2))))
  
  (supervector-fold2 (lambda (x y s) (f x y) s) #t s1 i1 s2 i2 n))

(define* (supervector-op2
	  op op1 op2 seed s1 i1 s2 i2 n
	  #:key
	  (divide? #f)
	  (minsize 1))
  
  (define f-empty12
    (lambda (b1 i1 j1 d1 b2 i2 j2 d2 seed)
      (let ((x    (d-ref b2))
	    (bits (bits-ref b1))
	    (v1   (v-ref b1))
	    (seed seed))
	(aif (p val) (op1 x (+ 1 (dist i1 j1) seed))
	     val
	     (begin
	       (standard-loop-1 #t bits i1 j1 d1 #f (get-reffer)
	         (lambda (ref k)
		   (set! seed (op (ref v1 k) seed))))
	       seed)))))
      
  (define f-empty11 
    (lambda (b1 i1 j1 d1 b2 i2 j2 d2 seed)
      (f-empty12 b2 i2 j2 d2 b1 i1 j1 d1 seed)))
    		      
  (define f-empty2 
    (lambda (b1 i1 j1 d1 b2 i2 j2 d2 seed)
      (let ((x1  (d-ref b1))
	    (x2  (d-ref b2))
	    (n2  (+ (dist i2 j2) 1)))
	(op2 x1 x2 n2 seed))))
  
  (define (f-bin b1 i1 j1 d1 b2 i2 j2 d2 seed)
    (let* ((n2    (+ 1 (dist i1 j1)))
	   (N2    (sz-ref b2))
	   (bits1 (bits-ref b1))
	   (bits2 (bits-ref b2))
	   (v1    (v-ref b1))
	   (v2    (v-ref b2)))
      
      (standard-loop-2 #t
		       (bits2 i2 j2 d2)
		       (bits1 i1 j1 d1)
		       (get-reffer)
		       (get-reffer)
	(lambda (ref1 k1 ref2 k2)
	  (set! seed (op (ref1 v1 k1) (ref2 v2 k2) seed))))

      seed))

  (define (f-slow x)
    (error "BUG!"))
  
  (supervector-chunk-fold2
   f-empty11 f-empty12 f-empty2 f-bin f-slow seed
   s1 i1 s2 i2 n #:minsize 1 #:divide? divide?))


(define-syntax-rule (mk-notter supervector-lognot! i-invert lognot can-invert)
  (define* (supervector-lognot! s #:optional (i 0) (n #f))
    (define binsize (sbz-ref s))
    
    (define (f ii jj w n+ d end)
      (let lp ((ii ii) (jj jj))
	(if (and (< jj n) (not (= ii end)))
	    (let* ((b  (vector-ref w ii))
		   (nb (sz-ref b))
		   (v  (v-ref  b)))

	      (if (< (- n jj) nb)
		  (begin
		    (populate b 0 (- n jj))
		    (let ((ss (make-supervector b #:binsize nb)))
		      (supervector-lognot! ss 0 (- n jj))
		      (lp (+ ii d) n)))

		  (begin
		   (if v
		       (let ((bits (bits-ref b)))

			 (when (not (can-invert b))
			   (populate b 0 (sz-ref b)))
			
			 (bits-set!
			  b (set-bit bits
				     (logxor i-invert
					     (get-bit bits i-invert)))))
		       
		       (d-set! b (lognot (d-ref b))))
	      
		   (lp (+ ii d) (+ jj (sz-ref b))))))
	    
	    jj)))

    (set! n (if n n (- (bytevector-length n) i)))

    (sd-set! s (lognot (sd-ref s)))
    
    (let ((n+ (n+ref s))
	  (n- (n-ref s)))
      (if (< i n-)
	  (let* ((w-   (w-ref s))
		 (ii   (quotient (- n- 1) binsize))
		 (kk   (modulo   (- n- 1) binsize))
		 (ii   (index ii -1 n-))		 
		 (end  -1)
		 (jj   (f ii 0 w- -1 n- end)))
	    (when (< jj n)
	      (let* ((n+  (n+ref s))
		     (end (+ 1 (quotient (- n+ 1) binsize))))
		(f 0 jj (w+ref s) 1 n+ end))))
	  
	  (let* ((ii   (quotient i binsize))
		 (jj   0)
		 (kk   (modulo i binsize))		 
		 (n+   (n+ref s))
		 (end  (+ 1 (quotient (- n+ 1) binsize)))
		 (w+   (w+ref s)))
	    
	    (when (> kk 0)
	      (set! ii (+ ii 1))
	      (let* ((b  (vector-ref w+ ii))
		     (nb (sz-ref b))		    
		     (m  (min (+ n kk) nb)))
		
		(set! jj m)
		
		(populate b kk (- m kk))
		
		(let ((ss (make-supervector b #:binsize nb)))
		  (supervector-lognot! ss m))))
	    
	    (f ii jj (w+ref s) 1 (n+ref s) end))))))

(mk-notter supervector-lognot! i-invert lognot can-invert)
(mk-notter supervector-not!    i-negate not    can-negate)
	  
  
  
  

(mk-supervector-binary superbits-binary
   #:is-m        8
   #:is-scm      0
   #:is-neg      0
   #:is-alg      0
   #:is-unsigned #t
   #:is-endian   (native-endianness)
   #:is-integer  #t)

(define (test-supervector-functional)
  (let ((s1 (make-supervector 32 1 #:binsize 4))
	(s2 (make-supervector 32 2 #:binsize 4))
	(s3 (make-supervector #u8(1 2 3 4 5 6 7 8 9)  #:binsize 4))
	(s4 (make-supervector #u8(1 2 1 2 1 2 1 2 1)  #:binsize 3)))
    (pk 1 (supervector-fold + 0 s1))
    (pk 2 (supervector-fold + 0 s2))
    (pk 3 (supervector-fold + 0 s3))
    (pk 4 (supervector-fold + 0 s4))

    (supervector-tr (lambda (x) (+ x 1)) s4 0 s4 0 9)
    (pk 5 s4)

    (supervector-tr (lambda (x) (+ x 1)) s2 0 s1 0 7)
    (pk 6 s1)
    
    (values)))
    
    
