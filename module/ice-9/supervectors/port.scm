;;HACK
(define-syntax-rule (define-module- name args ...)
  (cond-expand
   (guile-3
    (define-module name
      #:declarative? #f
      args ...))
   (guile
    (define-module name args ...))))


(define-module- (ice-9 supervectors port)
  #:use-module (ice-9 supervectors make)
  #:use-module (ice-9 supervectors core)
  #:use-module (ice-9 supervectors append)
  #:use-module (ice-9 supervectors strings)
  #:use-module (rnrs bytevectors)
  #:use-module (ice-9 binary-ports)
  #:use-module (fibers)
  #:export (port->superstring))

(define-syntax-rule (aif it p x y) (let ((it p)) (if it x y)))

(define (port->superstring-0 port mk-handle)
  (let* ((s      (make-superstring 0))
	 (bs     (sbz-ref s))
	 (v      (make-vector bs))
	 (n      (* 4 bs))
	 (handle (mk-handle))
	 (bv     (make-bytevector n)))

    (define (push s v j)
      (supervector-append!
       s
       (list
	(make-superstring
	 (if (= j bs)
	     v
	     (let ((vv (make-vector j)))
	       (let lp ((i 0))
		 (if (< i j)
		     (begin
		       (vector-set! vv i (vector-ref v i))
		       (lp (+ i 1)))
		     vv))))))))
    
    (let lp ((i 0) (j 0))
      (let ((x (get-bytevector-some! port bv i (- n i))))
	(cond
	 ((eof-object? x)
	  (let ((j (handle bv i v j)))
	    (push s v j))
	  s)
	 
	 ((eq? x 0)
	  (sleep 0.001)
	  (lp i j))
	 
	 (else
	  (let ((i (+ i x)))
	    (call-with-values
		(lambda () (handle bv i v j))
	      (lambda (j ii)		
		(let ((iii (- i ii)))
		  (when (> iii 0)
		    (bytevector-copy! bv ii bv 0 iii))
		  (if (= j bs)
		      (begin
			(push s v bs)
			(lp iii 0))
		      (lp iii j))))))))))))
	  

(define (mk-utf8-handle)
  (let ((val   0)
	(n     0)
	(state 1))

    (define (next-state code)
      (case state
	((1)
	 (cond
	  ((<= code #x7f)
	   code)

	  ((<= code #xdf)
	   (set! val (ash (logand code #x1f) 6))
	   (set! state 2)
	   #f)
	  
	  ((<= code #xef)
	   (set! val (ash (logand code #xf) 12))
	   (set! state 3)
	   (set! n     1)
	   #f)

	  ((<= code #xff)
	   (set! val (ash (logand code #x7) 18))
	   (set! state 4)
	   (set! n     2)
	   #f)))
			     	     
	((2)
	 (set! state 1)
	 (logior val (logand code #x3f)))

	
	((3)
	 (case n
	   ((0)
	    (set! state 1)
	    (logior val (logand code #x3f)))
	   
	   ((1)
	    (set! n   0)
	    (set! val (logior val (ash (logand code #x3f) 6)))
	    #f)))
	    
	((4)
	 (case n
	   ((0)
	    (set! state 1)
	    (logior val (logand code #x3f)))
	   
	   ((1)
	    (set! n   0)
	    (set! val (logior val (ash (logand code #x3f) 6)))
	    #f)

	   ((2)
	    (set! n   1)
	    (set! val (logior val (ash (logand code #x3f) 12)))
	    #f)))))

	 
    
    (lambda (bv i v j)
      (let ((n (vector-length v)))
	(let lp ((k 0) (j j))
	  (if (< k i)
	      (if (< j n)
		  (aif it (next-state (bytevector-u8-ref bv k))
		       (begin
			 (vector-set! v j it)
			 (lp (+ k 1) (+ j 1)))
		       (lp (+ k 1) j))
		  (values j k))
	      (values j k)))))))


(define (port->superstring/utf8 port)
  (port->superstring-0 port mk-utf8-handle))

(define* (port->superstring port #:key (encoding 'utf-8))
  (case encoding
    ((utf-8)
     (port->superstring/utf8 port))
    (else
     (error "unknown encoding"))))
