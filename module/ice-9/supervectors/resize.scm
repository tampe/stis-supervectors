;;HACK
(define-syntax-rule (define-module- name args ...)
  (cond-expand
   (guile-3
    (define-module name
      #:declarative? #f
      args ...))
   (guile
    (define-module name args ...))))


(define-module- (ice-9 supervectors resize)
  #:use-module (ice-9 supervectors core)
  #:use-module (ice-9 supervectors copy)
  #:use-module (ice-9 supervectors make)
  #:use-module (ice-9 supervectors typeinfo)
  #:export
  (
   supervector-expand-left!
   supervector-expand-right!
   supervector-shrink-left!
   supervector-shrink-right!
   ))

(define (resize-bin b default m)
  (let* ((bits    (bits-ref b))
	 (bs      (bs-ref   b))
	 (n       (sz-ref   b)))
		      
    (let lp ((k 1))
      (if (< k m)
	  (lp (* 2 k))
	  (let* ((k  (min k bs))
		 (s1 (make-supervector0 (vector) (vector b) 0 n bs bits 0))
		 (s2 (make-supervector k default #:bits bits #:binsize bs)))
	    
	    (supervector-copy! s1 0 s2 0 n)
	    
	    (let ((b2 (vector-ref (w+ref s2) 0)))
	      (v-set!    b (v-ref  b2))
	      (j-set!    b (j-ref  b2))
	      (d-set!    b (d-ref  b2))
	      (bits-set! b (bits-ref b2))
	      (sz-set!   b m)
	      (sc-set!   b (sc-ref b2))))))))
	      

(define (mk w+ref n+ref w+set! n+set!)
  (lambda (s n)
    (let* ((n+      (n+ref s))
	   (w+      (w+ref s))
	   (bits    (sbits-ref s))
	   (bs      (sbz-ref s))
	   (nn      (+ n+ n))
	   (i0      (quotient (- n+ 1) bs))
	   (i       (quotient (- nn 1) bs))
	   (j       (modulo   (- nn 1) bs))
	   (default (sd-ref s))
	   (b       (vector-ref w+ i0))
	   (bits2   (bits-ref b))
	   (bits3   (clear-constant-bits bits2)))

      (when (= (get-bit bits i-resize) 0)
	(error "can't resize"))
      
      (cond
       ((= i0 i)
	(resize-bin b default (+ j 1))
	(n+set! s nn))
       
       (else
	;; Maybe resize the w+ vector
	(let ((wn (vector-length w+)))
	  (let lp ((kwn wn))
	    (if (> (+ i 1) kwn)
		(lp (* 2 kwn))
		(when (< wn kwn)
		  (let ((ww+ (make-vector kwn)))
		    (w+set! s ww+)
		    (bytevector-copy*! w+ 0 ww+ 0 (+ i0 1))
		    (set! w+ ww+))))))

	(resize-bin b default bs)
	
	;; Fill in the rest up to the last one
	(let lp ((ii (+ i0 1)))	  
	  (when (< ii i)
	    (let ((bq  (mk-bin bs default bs bits3)))
	      (vector-set! w+ ii bq)
	      (lp (+ ii 1)))))
	
	;; The last bin
	(let ((bq  (mk-bin (+ j 1) default bs bits3)))
	  (vector-set! w+ i bq))

	(n+set! s nn))))))
  	     
(define supervector-expand-right! (mk w+ref n+ref w+set! n+set!))
(define supervector-expand-left!  (mk w-ref n-ref w-set! n-set!))

;; shrinking will not free memory, to do that use free
(define (supervector-schrink-right! s n)
  (recenter-right s n)
  (let* ((n+ (n+ref s))
	 (nn (- n+ n))
	 (bs (sbz-ref s))
	 (i0 (quotient nn bs))
	 (j0 (modulo   nn bs))
	 (w+ (w+ref s))
	 (b  (vector-ref w+ i0)))
    (sz-set! b (+ j0 1))
    (n+set! s nn)))

(define (supervector-schrink-left! s n)
  (recenter-left s n)
  (let* ((n+ (n-ref s))
	 (nn (- n+ n))
	 (bs (sbz-ref s))
	 (i0 (quotient nn bs))
	 (j0 (modulo   nn bs))
	 (w+ (w-ref s))
	 (b  (vector-ref w+ i0)))
    (sz-set! b (+ j0 1))
    (n-set! s nn)))


(define (recenter-right s n)
  (let ((n+ (n+ref s))
	(bs (sbz-ref s)))
    (when (< n+ n)
      (let ((n- (n-ref s)))
	(when (< (+ n+ n-) n)
	  (error "cant shrink more than available size"))

	(let* ((m-half (quotient n- 2))
	       (m      (max m-half (- n n+)))
	       (mm     (if (> (modulo m bs) 0)
			   (* bs (+ 1 (quotient m bs)))
			   m))
	       (mmm    (if (> mm n-)
			   m
			   mm))
	  
	       (nn-    (- n- mmm)))

	  (let ((s2 (make-supervector (+ n+ n-) #:n- n- #:bits (sbits-ref s))))
	    (supervector-copy! s 0 s2 0 (+ n+ n-) #:ref? #t)
	    (w+set! s (w+ref s2))
	    (n+set! s (n+ref s2))
	    (w-set! s (w-ref s2))
	    (n-set! s (n-ref s2))))))))

(define (recenter-left s n)
  (let ((n+ (n-ref s))
	(bs (sbz-ref s)))
    (when (< n+ n)
      (let ((n- (n+ref s)))
	(when (< (+ n+ n-) n)
	  (error "cant shrink more than available size"))

	(let* ((m-half (quotient n- 2))
	       (m      (max m-half (- n n+)))
	       (mm     (if (> (modulo m bs) 0)
			   (* bs (+ 1 (quotient m bs)))
			   m))
	       (mmm    (if (> mm n-)
			   m
			   mm))
	  
	       (nn-    (- n- mmm)))

	  (let ((s2 (make-supervector (+ n+ n-) #:n- n- #:bits (sbits-ref s))))
	    (supervector-copy! s 0 s2 0 (+ n+ n-) #:ref? #t)
	    (w+set! s (w+ref s2))
	    (n+set! s (n+ref s2))
	    (w-set! s (w-ref s2))
	    (n-set! s (n-ref s2))))))))

	
     

	
	
    
	
