;;HACK
(define-syntax-rule (define-module- name args ...)
  (cond-expand
   (guile-3
    (define-module name
      #:declarative? #f
      args ...))
   (guile
    (define-module name args ...))))


(define-module- (ice-9 supervectors basic)
  #:use-module (rnrs bytevectors)
  #:use-module (srfi srfi-9 gnu)
  #:use-module (ice-9 supervectors core)
  #:use-module (ice-9 supervectors macros)
  #:use-module (ice-9 supervectors typeinfo)
  #:use-module (ice-9 supervectors expand)
  #:use-module (ice-9 supervectors make)

  #:export (supervector-scm-ref
	    supervector-u8-ref
	    supervector-s8-ref
	    supervector-u16-ref
	    supervector-s16-ref
	    supervector-u32-ref
	    supervector-s32-ref
	    supervector-u64-ref
	    supervector-s64-ref
	    supervector-ieee-single-ref
	    supervector-ieee-double-ref

	    *do-expand*           ;;Set this is a set shall expand (slower)

	    supervector-scm-set!
	    supervector-u8-set!
	    supervector-s8-set!
	    supervector-u16-set!
	    supervector-s16-set!
	    supervector-u32-set!
	    supervector-s32-set!
	    supervector-u64-set!
	    supervector-s64-set!

	    supervector-ieee-single-set!
	    supervector-ieee-double-set! 

	    get-super-setter        ;; get a super from bits
	    get-super-reffer        ;; ...
	    get-super-setter-f      ;; compiles faster
	    get-super-reffer-f      ;; compiles faster
	    
	    test-supervector-basic))

(define b-ref  bytevector-u8-ref)
(define b-set! bytevector-u8-set!)

(define bytevector-scm-set! vector-set!)
(define bytevector-scm-ref  vector-ref)

(eval-when (eval compile load)
  (define (symbol-concat . l)
    (let* ((l (map symbol->string l))
	   (s (apply string-append l)))
      (string->symbol s))))

(define-syntax-rule (mk-r m f r)
  (define f (mk-r0 m r)))

(define (mk-r0 m ref)
  (if (= m 1)
      ref
      (lambda (s i l)
	(let ((v (make-bytevector m)))
	  (let lp ((j 0) (i i))
	    (if (< j m)
		(begin
		  (b-set! v j (supervector-u8-ref s i))
		  (lp (+ j 1) (+ i 1)))
		(apply ref v 0 l)))))))

(define-syntax mk-rr
  (lambda (x)
    (syntax-case x ()
      ((u m uu)
       (let ((u8 (syntax->datum #'uu)))
	 (with-syntax ((fall (datum->syntax
			      #'u (symbol-concat 'fallback- u8 '-ref)))
		       
		       (byte (datum->syntax
			      #'u (symbol-concat 'bytevector- u8 '-ref))))
	   #'(mk-r m fall byte)))))))

(define-syntax-rule (mk mk-rr a ...)
  (begin
    (mk-rr a ... 1 u8)
    (mk-rr a ... 1 s8)
    (mk-rr a ... 2 u16)
    (mk-rr a ... 2 s16)
    (mk-rr a ... 4 u32)
    (mk-rr a ... 4 s32)
    (mk-rr a ... 4 ieee-single)
    (mk-rr a ... 8 u64)
    (mk-rr a ... 8 s64)
    (mk-rr a ... 8 ieee-double)
    (mk-rr a ... 1 scm)))

(mk mk-rr)

(define-syntax-rule (mk-s m f s)
  (define f (mk-s0 m s)))

(define (mk-s0 m set)
  (if (= m 1)
      set
      (lambda (s i l)
	(let ((v (make-bytevector m)))
	  (apply set v 0 l)
	  (let lp ((j 0) (i i))
	    (if (< j m)
		(begin
		  (supervector-u8-set! s i (b-ref v j))
		  (lp (+ j 1) (+ i 1)))
		(values)))))))

(define-syntax mk-ss
  (lambda (x)
    (syntax-case x ()
      ((u m uu)
       (let ((u8 (syntax->datum #'uu)))
	 (with-syntax ((fall (datum->syntax
			      #'u (symbol-concat 'fallback- u8 '-set!)))
		       
		       (byte (datum->syntax
			      #'u (symbol-concat 'bytevector- u8 '-set!))))
	    #'(mk-s m fall  byte)))))))
		      
(mk mk-ss)
	   
(define-syntax-rule (mk-x s m r b f)
  (define r (mk-x0 s m b f)))

(define-inlinable (lognot- m x)
  (logand (- (ash 1 (* 8 m)) 1) (lognot x)))

(define (mk-x0 set m b-u8 fallback)
  (lambda (self mm vec i n binsize l0)
    (define len   (* mm binsize))
    (define i1    (modulo   i len))
    (define i2    (quotient i len))
    (define bin   (vector-ref vec i2))
    (define bits  (bits-ref bin))
    (define bv    (v-ref bin))
    (define inv   (get-bit bits i-invert))
    (define dir   (get-dir bits))

    (define l (if (and set (> inv 0)) (cons (lognot- m (car l0)) (cdr l0)) l0))
    ;; Expansions should be detected with a catch and the *do-expand* should
    ;; be used to indicate to expand the area with another write
    ;; we do not want to always test values acording to spec
    (let lp ((bin bin) (bv (v-ref bin)) (ii i1))
      (cond
       ((bin? bv)
	(let* ((jj   (j-ref bv))
	       (dir2 (get-dir bv))
	       (ii   (if (> dir2 0) (+ jj ii) (- jj ii))))
	  (lp bv (v-ref bv) ii)))

       ((not bv)
	(if set
	    (begin
	      (populate bin ii)
	      (lp bin (v-ref bin) ii))
	      
	    (d-ref  bin)))

       ((supervector? bv)
	(apply self bv ii l))

       (bv
	(when set
	  (let ((bits (bits-ref bin)))
	    (when (> (get-bit bits i-ro) 0)
	      (error "try to set a read only supervecor"))
	    

	    (fix-cow bin ii)
	    
	    (let ((bits (bits-ref bin)))
	      (when (> (get-bit bits i-expand) 0)
		(let* ((scm1  (get-bit bits  i-scm))
		       (scm2  (call-with-values
				  (lambda () (get-type-from-val (car l)))
				(lambda (m sg fl scm)
				  scm))))
		
		  (when (and (> scm2 0) (= scm1 0))
		    (let ((bits2 (logior i-scm i-dir)))
		      (expand-to-bits bin ii bits bits2)
		      (lp bin (v-ref bin) ii))))))))
	    

	(let* ((bits (bits-ref bin))
	       (bv   (v-ref bin))
	       (dir  (if (> (get-bit bits i-dir) 0) 1 -1)))
	  
	  (let ((i1 (index ii dir len)))
	    (if (or set (= inv 0))
		(apply b-u8 bv i1 l)
		
		(lognot- m
		 (apply b-u8 bv i1 l))))))))))

    
    ;; Expand to 
      
     
(define-syntax mk-xx
  (lambda (x)
    (syntax-case x ()
      ((u #f m v)
       (let ((u8 (syntax->datum #'v)))
	 (with-syntax ((ref  (datum->syntax
			      #'u (symbol-append 'ref- u8)))
		       (byte (datum->syntax
			      #'u (symbol-append 'bytevector- u8 '-ref)))
		       (fall (datum->syntax
			      #'u (symbol-append 'fallback- u8 '-ref))))
	      #'(mk-x #f m ref byte fall))))

      ((u #t m v)
       (let ((u8 (syntax->datum #'v)))
	 (with-syntax ((ref  (datum->syntax
			      #'u (symbol-append 'set- u8)))
		       (byte (datum->syntax
			      #'u (symbol-append 'bytevector- u8 '-set!)))
		       (fall (datum->syntax
			      #'u (symbol-append 'fallback- u8 '-set!))))
	   #'(mk-x #t m ref byte fall)))))))

(mk mk-xx #f)
(mk mk-xx #t)

(define-syntax-rule (mk-y m h r b f l ...)
  (define (h s i l ...)
    (mk-y0 h s i m r b f (list l ...))))
    

(define (mk-y0 self s i m ref bytevector-ref fallback-ref l)
  (define binsize (sbz-ref s))
  (define bits    (sbits-ref s))
  (define mm      (get-m bits))
  (define n-      (n-ref s))
  (define n+      (n+ref s))
  (cond
   ((< i 0)
    (error "index underflow"))

   ((> (+ i m) (* mm (+ n- n+)))
    (error "index overflow"))
   
   ;; Boundary ==> slow fallback
   ((not (= (quotient i binsize) (quotient (- (+ i m) 1) binsize)))
    (fallback-ref s i l))

   (else
    (if (< i n-)
	(ref self mm (w-ref s) (neg i n-) n- binsize l)
	(ref self mm (w+ref s) (pos i n-) n+ binsize l)))))

(define-syntax mk-ys
  (lambda (x)
    (syntax-case x ()
      ((u m uu l ...)
       (let ((u8 (syntax->datum #'uu)))
	 (with-syntax ((ref (datum->syntax
			     #'u (symbol-concat 'set- u8)))
		       (sup (datum->syntax
			     #'u (symbol-concat
				  'supervector- u8 '-set!)))
		       
		       (fall (datum->syntax
			      #'u (symbol-concat
				   'fallback- u8 '-set!)))
		       
		       (byte (datum->syntax
			      #'u (symbol-concat
				   'bytevector- u8 '-set!)))

		       ((l ...) (cons*
				 (datum->syntax #'u 'val)
				 (if (= (syntax->datum #'m) 1)
				     '()
				     (list (datum->syntax #'u 'e))))))

	    #'(mk-y m sup ref byte fall l ...)))))))

(define-syntax mk-yr
  (lambda (x)
    (syntax-case x ()
      ((u m uu l ...)
       (let ((u8 (syntax->datum #'uu)))
	 (with-syntax ((ref (datum->syntax
			     #'u (symbol-concat 'ref- u8)))
		       (sup (datum->syntax
			     #'u (symbol-concat
				  'supervector- u8 '-ref)))
		       
		       (fall (datum->syntax
			      #'u (symbol-concat
				   'fallback- u8 '-ref)))
		       
		       (byte (datum->syntax
			      #'u (symbol-concat
				   'bytevector- u8 '-ref)))
		       
		       ((l ...) (if (= (syntax->datum #'m) 1)
				    '()
				    (list (datum->syntax #'u 'e)))))
	 #'(mk-y m sup ref byte fall l ...)))))))

(mk mk-yr)
(mk mk-ys)

(mk-get-setter get-super-setter #t 1 AS
	       ((1 supervector-s8-set!)
		(2 supervector-s16-set!)
		(4 supervector-s32-set!)
		(8 supervector-s64-set!))
	       
	       ((1 supervector-u8-set!)
		(2 supervector-u16-set!)
		(4 supervector-u32-set!)
		(8 supervector-u64-set!))

	       ((4 supervector-ieee-single-set!)
		(8 supervector-ieee-double-set!))

	       ((1 supervector-scm-set!)))

(mk-get-setter get-super-reffer #t #f AR
	       ((1 supervector-s8-ref)
		(2 supervector-s16-ref)
		(4 supervector-s32-ref)
		(8 supervector-s64-ref))
	       
	       ((1 supervector-u8-ref)
		(2 supervector-u16-ref)
		(4 supervector-u32-ref)
		(8 supervector-u64-ref))
	       
	       ((4 supervector-ieee-single-ref)
		(8 supervector-ieee-double-ref))
	       
	       ((1 supervector-scm-ref)))

(mk-get-setter get-super-ref-setter #t 2 ARS
	       ((1 supervector-s8-ref  supervector-s8-set! )
		(2 supervector-s16-ref supervector-s16-set!)
		(4 supervector-s32-ref supervector-s32-set!)
		(8 supervector-s64-ref supervector-s64-set!))
	       
	       ((1 supervector-u8-ref  supervector-u8-set! )
		(2 supervector-u16-ref supervector-u16-set!)
		(4 supervector-u32-ref supervector-u32-set!)
		(8 supervector-u64-ref supervector-u64-set!))
	       
	       ((4 supervector-ieee-single-ref supervector-ieee-single-set!)
		(8 supervector-ieee-double-ref supervector-ieee-double-set!))
	       
	       ((1 supervector-scm-ref supervector-scm-set!)))

	   
(define (test-supervector-basic)
  (let ((s10 (make-supervector 5 11000 #:m 2    #:binsize 4))
	(s11 (make-supervector 10 1 #:expand? #t #:binsize 4 #:level 1)))    

    (pk s10)
    (pk 'ref (supervector-u16-ref s10 4 'little))
    
    (supervector-u16-set! s10 4 2000 'little)
    (pk 'ref (supervector-u16-ref s10 4 'little))
    (pk s10)
    
    (supervector-u16-set! s11 4 3000 'little)
    (pk 'ref (supervector-u16-ref s11 4 'little))
    (pk 'ref s11)
    (supervector-scm-set! s11 4 'a)
    (pk s11)
    (values)))
