;;HACK
(define-syntax-rule (define-module- name args ...)
  (cond-expand
   (guile-3
    (define-module name
      #:declarative? #f
      args ...))
   (guile
    (define-module name args ...))))


(define-module- (ice-9 supervectors make)
  #:use-module (ice-9 supervectors core)
  #:use-module (ice-9 supervectors typeinfo)
  #:use-module (rnrs bytevectors)
  #:export (mk-bin
	    make-supervector
	    
	    test-supervector-make))

(define* (mk-bin x #:optional (d 0) (bs #f) (bits 0) #:key (ref? #f))
  (let* ((m   (get-m bits))
	 (dir (get-bit bits i-dir)))	      
    (cond
     ((supervector? x)
      (let ((sz (supervector-length x)))
	(mk-bin0 x  #f sz sz bits (if (> dir 0) 0 (- sz m)) 1)))
	     
     ((or (bytevector? x) (vector? x))      
      (let ((sz (/ (bytevector-length* x) m)))
	(mk-bin0 x d
		 sz
		 bs
		 bits
		 (if (> dir 0) 0 (- sz m))
		 1)))

     ((bin? x)
      (let* ((v     (v-ref x))
	     (bits1 (bits-ref x))
	     (bits2 (set-bit (clear-bit bits1 (logior i-ro
						      i-algebra
						      i-dir
						      i-invert
						      i-negate))
			     i-dir)))

	(cond
	 ((not v)
	  (mk-bin0 #f (d-ref x) (sz-ref x)  (bs-ref x) bits2 0 1))

	 (else
	  (if (> 0 (get-bit bits1 i-ro))
	      (if (bin? (v-ref v))
		  (let ((bits2 (clear-bit bits1 i-ro)))
		    (mk-bin0 (v-ref v) (d-ref v) (sz-ref x) (bs-ref v) bits2
			     (j-ref v) (sc-ref v)))
		  (mk-bin0 v 0 (sz-ref x) (bs-ref x) bits2 0 1))
	      (mk-bin0 v 0 (sz-ref x) (bs-ref x) bits2 0 1))))))
     
     ((and (number? x) (integer? x) (>= x 0))
      (mk-bin0 #f d x bs bits (if (> dir 0) 0 (- x m)) 1)))))

(define (get-sizes n binsize)
  (let* ((i  (modulo n binsize))
	 (nn (if (= n 0) 0 (if (= i 0) binsize i)))
	 (N  (if (= n 0) 1 (+ (if (> i 0) 1 0) (quotient n binsize)))))
    (cons* i nn N)))

(define* (make-supervector n #:optional (x 0)
			   #:key
			   (binsize     #f)
			   (n-          0)
			   (d           0)
			   (dir         1)
			   (m           1)
			   (signed?     #f)
			   (float?      #f)
			   (endian      (native-endianness))
			   (ro?         #f)
			   (ref?        #f)
			   (scm?        #f)
			   (expand?     #f)
			   (resizable?  #f)
			   (typed?      #f)
			   (superbit?   #f)
			   (superstr?   #f)
			   (bitcorrect   0)
			   (level        0)
			   (bits        #f)
			   (y           #f))

  (define (make-superstring v bits)
    (define level0 (get-level bits))
    
    (define (make-superbin s i nn level)
      (let* ((m    (/ s 8))
	     (n    (* nn m))
	     (bits (set-level (set-m bits m) level))
	     (bv   (make-bytevector n)))
	
	(cond
	 ((= m 1)
	  (let lp ((j 0) (i i))
	    (when (< j n)
	      (bytevector-u8-set! bv j (vector-ref v i))
	      (lp (+ j 1) (+ i 1)))))
	 
	 ((= m 2)
	  (let lp ((j 0) (i i))
	    (when (< j n)
	      (bytevector-u16-set! bv j (vector-ref v i) (native-endianness))
	      (lp (+ j 2) (+ i 1)))))

	 ((= m 4)
	  (let lp ((j 0) (i i))
	    (when (< j n)
	      (bytevector-u32-set! bv j (vector-ref v i) (native-endianness))
	      (lp (+ j 4) (+ i 1))))))
	 
	(mk-bin bv 0 binsize bits #:ref? #t)))
	
    (define (make-superbins i s1 n1 s2 n2 level)
      (define-syntax-rule (call code)
	(call-with-values (lambda () code)
	  (lambda (x y) y)))
      
      (let ((b1 (call (run i n1 (- level 1))))
	    (b2 (call (run (+ i n1) n2 (- level 1)))))

	;; Combine if the size is equal in both limbs
	(if (and (bytevector? (v-ref b1))
		 (bytevector? (v-ref b2))
		 (= (get-m (bits-ref b1)) (get-m (bits-ref b2))))
	    
	    (call (run i (+ n1 n2) level))
	    
	    (let ((bits (set-level bits level)))	    	
	      (mk-bin (make-supervector (cons b1 b2) #:bits bits #:ref? #t)
		      0 (+ n1 n2)
		      bits)))))
	
    (define (check i N s)
      (let lp ((j i) (k 0) (size s))
	(if (< k N)
	    (if (< (vector-ref n j) (ash 1 size))
		(lp (+ j 1) (+ k 1) size)
		(lp j k (* 2 size)))
	    size)))
    
    (define (run i bs level)
      (let ((N (vector-length v)))
	(if (< i N)
	    (let* ((nn (min (- N i) bs))
		   (s  (check i nn 8)))
	      (values (+ i nn)
		      (if (> s 8)
			  (let* ((k1 (quotient nn 2))
				 (k2 (- nn k1))
				 (n1 (max k1 k2))
				 (n2 (min k1 k2)))
			    (if (or (= level 0)
				    (< n1 64)
				    (< n1 (ash binsize (- level0))))
				(make-superbin s i nn level)
				(let ((s1 (check i n1 8))
				      (s2 (check (+ i n1) n2 8)))
				  (if (= s1 s2)
				      (make-superbin s i nn level)
				      (make-superbins i s1 n1 s2 n2 level)))))
			  (make-superbin 8 i nn level)))))))

    (set! bits (set-bit bits i-expand))
    
    (let* ((nn    (vector-length v))
	   (l     (let lp ((i 0))
		    (if (< i nn)
			(call-with-values (lambda () (run i binsize level0))
			  (lambda (i b)
			    (cons b (lp i))))
			'()))))
      (make-supervector0 (vector) (list->vector l) 0 nn binsize bits 0)))
  
  (set! binsize (if binsize binsize *binsize*))
  
  (cond
   ((bin? n)
    (let* ((binsize (bs-ref   n))
	   (bits    (bits-ref n))
	   (v+      (vector   (if ref? n (b-copy n))))
	   (n       (sz-ref   n))
	   (v-      (vector)  ))
      (make-supervector0 v- v+ 0 n binsize bits x)))

   ((pair? n)
    (let* ((b1      (car n))
	   (b2      (cdr n))
	   (binsize (sz-ref b1))
	   (v+      (vector (if ref? b1 (b-copy b1))
			    (if ref? b2 (b-copy b2))))
	   (v-      (vector)  ))
      (bs-set! b1 binsize)
      (bs-set! b2 binsize)
      (make-supervector0 v- v+ 0 (+ binsize (sz-ref b2)) binsize bits 0)))
   
   ((supervector? n)
    (let ((w-      (w-ref n))
	  (n-      (n-ref n))
	  (w+      (w+ref n))
	  (n+      (n+ref n))
	  (binsize (if binsize binsize (sbz-ref n)))
	  (bits    (clear-bit (sbits-ref n) i-ro))
	  (x       (sd-ref n)))
	      
      (define (w-copy w)
	(let* ((n  (vector-length w))
	       (ww (make-vector n)))
	  (let lp ((i 0))
	    (if (< i n)
		(begin
		  (vector-set!
		   ww i
		   (let ((b (vector-ref w i)))
		     (if (bin? b)
			 (mk-bin b #:ref? ref?)
			 b)))
		  (lp (+ i 1)))
		ww))))
      
      (let ((out (make-supervector0
		  (w-copy w-) (w-copy w+) n- n+ binsize bits x)))
	
	(when ro?
	  (supervector-make-ro! out))

	out)))

   ((or (bytevector?  n) (vector?  n))
    (let* ((bv    n)
	   (nb    (bytevector-length* bv))
	   (scm?  (and (not superstr?) (or scm? (vector? n)) 1))
	   (bits  (make-bits #:bits       bits
			     #:m          (if (and (not superstr?) scm?) 1 m)
			     #:dir        dir
			     #:typed?     typed? 
			     #:scm?       scm?
			     #:float?     float?
			     #:signed?    signed?
			     #:endian     endian
			     #:scm?       scm?
			     #:ro?        ro?
			     #:superbit?  superbit?
			     #:superstr?  superstr?
			     #:expand?    expand?
			     #:bitcorrect bitcorrect
			     #:resizable? resizable?
			     #:level      level)))
      (if superstr?
	  (make-superstring n bits)
	  (let* ((M     (* m binsize))
		 (i.n.N (get-sizes (quotient nb m) binsize))
		 (i     (car i.n.N))
		 (nn    (cadr i.n.N))
		 (N     (cddr i.n.N))
		 (v+    (make-vector N))
		 (v-    (vector (mk-bin 0 x binsize bits))))
      
	    (when (not (= (modulo nb m) 0))
	      (error
	       "try to make a supervector from a bytvector with wrong word size"
	       ))

	    (let lp ((i 0) (k 0))
	      (when (< k N)
		(let* ((K (if (< k (- N 1)) M (- nb i)))
		       (bv2 (make-bytevector* K #:bits bits)))
		  (bytevector-copy*! bv i bv2 0 K)
		  (vector-set! v+ k (mk-bin bv2 d binsize bits))
		  (lp (+ i K) (+ k 1)))))

	    (make-supervector0 v- v+ 0 (/ nb m) binsize bits x)))))

   ((string? n)
    #;TODO
    (values))
   
   ((and (number? n) (integer? n) (>= n 0))
    (let* ((n+    (- n n-))
	   (nn-   (* m n-))
	   (nn+   (* m n+))
	   (bits  (if bits
		      bits
		      (make-bits #:m          (if scm? 1 m)
				 #:dir        dir
				 #:typed?     typed? 
				 #:scm?       scm?
				 #:float?     float?
				 #:signed?    signed?
				 #:endian     endian
				 #:scm?       scm?
				 #:superbit?  superbit?
				 #:superstr?  superstr?
				 #:ro?        ro?
				 #:expand?    expand?
				 #:bitcorrect bitcorrect
				 #:resizable? resizable?
				 #:level      level)))
	   
	   (bits  (aif it (expand-due-to-val? bits x)
		       (if expand?
			   it
			   (error (format
				   #f "value is not acording to spec ~a ~a"
				   bits x)))
		       bits))

	   (i.n.N (get-sizes n- binsize))
	   (i1    (car  i.n.N))
	   (nn1   (cadr i.n.N))
	   (N1    (cddr i.n.N))
	   (v-    (let ((v (make-vector N1)))
		    (for-each
		     (lambda (i)
		       (vector-set! v i (mk-bin binsize x binsize bits)))
		     (iota N1))
		    v))

	   (i.n.N (get-sizes n+ binsize))
	   (i2    (car i.n.N))
	   (nn2   (cadr i.n.N))
	   (N2    (cddr i.n.N))
	   (v+    (let ((v (make-vector N2)))
		    (for-each
		     (lambda (i)
		       (vector-set! v i (mk-bin binsize x binsize bits)))
		     (iota N2))
		    v))
	   (v-    (vector (mk-bin 0 x binsize bits))))

      (sz-set! (vector-ref v- (- N1 1)) nn1)
      (sz-set! (vector-ref v+ (- N2 1)) nn2)      
      (make-supervector0 v- v+ 0 n binsize bits x)))

   (else
    (pk n)
    (error "Not a valid make-superstring object"))))

;; ================================ TEST TOOL ================================
	      
(define (test-supervector-make)
  (let ((s10 (make-supervector 10 1          #:binsize 16))
	(s15 (make-supervector 15 1          #:binsize 16))
	(s16 (make-supervector 16 1          #:binsize 16))
	(s17 (make-supervector 17 1          #:binsize 16))
	(s18 (make-supervector (vector 1 'a) #:binsize 16 #:scm? #t))
	(s** (make-supervector 17 -1000      #:binsize 16 #:expand? #t))
	(sbv (make-supervector #u64(1 2 3 4) #:binsize 4 #:m 2 #:expand? #t)))
  
    (pk 10 s10)
    (pk 15 s15)
    (pk 16 s16)
    (pk 17 s17)
    (pk 18 s18)
    (pk 00 s**)
    (pk 11 sbv)
    
    (values)))
