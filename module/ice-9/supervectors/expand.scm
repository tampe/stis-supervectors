;;HACK
(define-syntax-rule (define-module- name args ...)
  (cond-expand
   (guile-3
    (define-module name
      #:declarative? #f
      args ...))
   (guile
    (define-module name args ...))))


(define-module- (ice-9 supervectors expand)
  #:use-module (ice-9 supervectors typeinfo)
  #:use-module (ice-9 supervectors core)
  #:use-module (ice-9 supervectors macros)
  #:use-module (ice-9 supervectors make)
  #:use-module (rnrs bytevectors)
  #:export (fix-cow
	    split-bin
	    expand-to-bits
	    populate))

(define-inlinable (dist x y) (if (> x y) (- x y) (- y x)))

(define* (split-bin b #:key (binsize #f))
  (let* ((bits (bits-ref b))
	 (lev  (get-level bits))
	 (bits (set-level bits (- lev 1)))
	 (n    (sz-ref b)))
    (if (>= n 16)
	(let* ((n1   (if binsize binsize (quotient n 2)))
	       (n2   (- n n1))
	       (sz   (max n1 n2))
	       (sb   (make-supervector b))	 
	       (s
		(make-supervector n #:binsize sz #:bits bits)))

	  (core-supervector-copy! sb 0 s 0 n)
	  (v-set! b s))
	(values))))
#|
Contains routines that creates bytevecors
|#
(define* (fix-cow b i #:optional (nn #f))
  (define nnn  (if nn nn (sz-ref b)))
  (define bits (bits-ref b))
  (define dir  (get-bit bits i-dir))
  (define j    (if (> dir 0) (- (+ i nnn) 1) (- nnn i 1)))
  
  (define (f)
    (let* ((i0   (j-ref  b))
	   (m    (get-m bits))
	   (n    (* m (sz-ref b)))
	   (j0   (if (> dir 0) (- (+ i0 n) 1) (- i0 n 1)))
	   (i1   (min i0 j0))
	   (v    (v-ref b))
	   (bitq (if (bin? v) (bits-ref b) 0))
	   (inv0 (get-bit bitq i-invert))
	   (neg0 (get-bit bitq i-negate))
	   (d0   (if (bin? v) (d-ref  v) 0))
	   (sc0  (if (bin? v) (sc-ref v) 1))
	   (bv   (make-bytevector* n #:bits bits)))
   
      (call-with-values (lambda () (vv-ref (v-ref b) i1 0 1))
	(lambda (v i1 _ dd)
	  (bytevector-copy*! v i1 bv 0 n)
	  (let* ((bits (clear-bit bits i-cow)))
	    (bits-set! b  bits)	
	    (v-set!    b  bv)
	    (j-set!    b  (if (> dir 0) 0 (- n 1)))
	    #f)))))
	  
  (if b
      (let ((bits  (bits-ref b))
	    (level (get-level bits)))
	(if (> (get-bit bits i-cow) 0)
	    (if (> level 0)
		(let* ((i0 (min i j))
		       (i1 (max i j))
		       (n  (+ 1 (dist i j)))
		       (sz (sz-ref b))
		       (n1 (quotient sz 2))
		       (n2 (- sz n1))
		       (N  (max n1 n2)))
		  (if (and (>= sz 16) (or (< i1 N) (>= i0 N)))
		      (begin
			(split-bin b)
			#t)
		      (f)))
		(f))
	    #f))
      #f))

(define* (expand-to-bits b i bits1 bits2 #:key
			 (divide? #f)
			 (j (- (sz-ref b) i 1)))
  (fix-cow b i)
  
  (let ()
    (define v1    (v-ref b))
    (define level (get-level bits1))

    
    (cond
     ((not v1)
      (values))
     
     ((and i divide? (> level 0))
      (let* ((level*    (- level 1))
	     (binsize   (sz-ref b))
	     (binsize1  (quotient binsize 2))
	     (binsize2  (- binsize binsize1))
	     (binsize3  (max binsize1 binsize2))
	     (binsize4  (min binsize1 binsize2))
	     (bits      (set-level bits2 level*))
	     (s         (make-supervector 
			 binsize
			 (d-ref b)
			 #:binsize binsize3
			 #:bits    bits))
	     (binx      (if (< i binsize3)
			    (get-bin1 s)
			    (get-bin2 s)))
	     (i         (if (< i binsize3)
			    i
			    (- i binsize3))))
	 
	(v-set! b s)
	(expand-to-bits binx i bits1 bits2)))

     (else
      (let* ((m1  (get-m bits1))
	     (m2  (get-m bits2))
	     (n1  (bytevector-length* v1))
	     (n   (/ n1 m1))
	     (n2  (* n m2)))
	(let ((v2 (make-bytevector* n2 #:bits bits2)))
	  (let ((s1 (make-supervector v1 #:bits bits1))
		(s2 (make-supervector v2 #:bits bits2)))
	    (core-supervector-copy! s1 0 s2 0 n)
	    (v-set!  b v2)
	    (d-set!  b 0 )
	    (j-set!  b 0 )
	    (sc-set! b 1)
	    (let* ((bits1 (clear-bit bits1 i-invert))
		   (bits1 (clear-bit bits1 i-dir))
		   (bits1 (set-type  bits1 bits2))
		   (bits1 (set-m bits1 m2)))
	      (bits-set! b bits1)))))))))

	 	  

;; when initiating a bin the default value is stored and
;; the vector is not created, in stead we just record the
;; default value, when we set a value in the bin we must
;; make sure to create the vector and the default value is
;; used inside the bin
(define* (populate bin i #:optional (nn #f))
  (when (not (v-ref bin))
    (let* ((bits  (bits-ref bin))
	   (m     (get-m  bits))
	   (y     (d-ref bin))
	   (s     (sz-ref bin))
	   (n     (* m s))
	   (level (get-level bits)))

      (define (f)
	(let ((v (if (= m 1)
		     (make-bytevector* n y #:bits bits)
		     (make-bytevector  n 0))))
	  (when (> m 1)
	    (iter-data bits m s		       
	      (lambda (i j d)
		(standard-loop-1 #f bits i j d #f (get-setter)
		  (lambda (set k)
		    (set v k y))))))

	  (clear-constant-data bin)
	  (j-set!  bin 0) ;; Todo improve this to allow different
	  (d-set!  bin 0)
	  (sc-set! bin 1)
	  (v-set!  bin v)

	  #f))

      (if (and i (> s 8) (> level 0))
	  (let* ((dir (get-bit bits i-dir))
		 (j   (if (> dir 0) (- (+ i n) 1) (- n i 1)))
		 (i1  (min i j))
		 (i2  (max i j))
		 (sz (sz-ref bin))
		 (n1 (quotient sz 2))
		 (n2 (- sz n1))
		 (N  (max n1 n2)))
	    (if (and (> sz 16) (or (< j N) (>= i N)))
		(begin
		  (split-bin bin)
		  #t)
		(f)))
	  (f)))))
      	  

