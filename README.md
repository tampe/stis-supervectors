# stis-supervectors

This is alpha software

A supervector that can be a big bytevectors, vector, string, bitvector, and what not. With batteries included.

The reationall is that when we consider very large vector objects we can many ti
mes optimize for featurefullness in stead of beein lean. The reason is that the actual operatins are expensive and we can cram in features due to this that is not costing you anything significantly extra. The featurefullness comes from chomping the vector op in smaller parts.


##Prerequisites
wingos: fibers is used at one spot.

##How to make it
(You may need to tweak the configuration files)

autoreconf
./configure
make
make install

## Example
> (use-modules (ice-9 supervectors))
> (define s (make-supervector 1000 10)  ;; Make a superfector of size 1000
> (supervector-set! s 10 12)            ;; set element 10 to 12
> (pk (supervector-fold + 0 s))         ;; sum all elements in the supervector
> (pk (supervector-ref s 5))            ;; get the value of element 5
